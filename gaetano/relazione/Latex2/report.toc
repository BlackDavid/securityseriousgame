\contentsline {section}{\numberline {1}Abstract : Why so serious?}{4}{section.1}
\contentsline {section}{\numberline {2}Learning vs entertainment}{6}{section.2}
\contentsline {section}{\numberline {3}Cognitive aspects}{9}{section.3}
\contentsline {section}{\numberline {4}Knowledge models and representation}{10}{section.4}
\contentsline {subsection}{\numberline {4.1}Knowledge Bayesian Tracing}{10}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Knowledge Bayesian Tracing Parameters}{11}{subsubsection.4.1.1}
\contentsline {section}{\numberline {5}Gaetano's Journey}{13}{section.5}
\contentsline {subsection}{\numberline {5.1}Security serious game on Firewall}{13}{subsection.5.1}
\contentsline {subsubsection}{\numberline {5.1.1}Clash of Lans: a firewall serious game}{13}{subsubsection.5.1.1}
\contentsline {subsubsection}{\numberline {5.1.2}Pros and Cons on the serious firewall game}{15}{subsubsection.5.1.2}
\contentsline {subsection}{\numberline {5.2}Security serious game on Criptography}{15}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}A serious game based on reinforcement learning}{16}{subsubsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.2}BigBro's world}{16}{subsubsection.5.2.2}
\contentsline {subsubsection}{\numberline {5.2.3}BigBro a serious game open and designed for scalability}{17}{subsubsection.5.2.3}
\contentsline {subsubsection}{\numberline {5.2.4}BigBro gameplays}{17}{subsubsection.5.2.4}
\contentsline {subsection}{\numberline {5.3}Teaching computer security with BigBro game}{18}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Topics covered by the Serious game}{23}{subsection.5.4}
\contentsline {subsubsection}{\numberline {5.4.1}Pros and cons on the cryptography Serious game}{23}{subsubsection.5.4.1}
\contentsline {section}{\numberline {6}Recaps}{24}{section.6}
\contentsline {subsection}{\numberline {6.1}Recap on Firewall}{24}{subsection.6.1}
\contentsline {subsubsection}{\numberline {6.1.1}Topology}{25}{subsubsection.6.1.1}
\contentsline {subsubsection}{\numberline {6.1.2}IDS}{25}{subsubsection.6.1.2}
\contentsline {subsubsection}{\numberline {6.1.3}IPS}{25}{subsubsection.6.1.3}
\contentsline {subsection}{\numberline {6.2}Recap on Symmetric Cryptography}{26}{subsection.6.2}
\contentsline {subsubsection}{\numberline {6.2.1}ECB}{26}{subsubsection.6.2.1}
\contentsline {subsubsection}{\numberline {6.2.2}CBC}{27}{subsubsection.6.2.2}
\contentsline {subsubsection}{\numberline {6.2.3}CTS}{27}{subsubsection.6.2.3}
\contentsline {subsubsection}{\numberline {6.2.4}CFB}{28}{subsubsection.6.2.4}
\contentsline {subsubsection}{\numberline {6.2.5}OFB}{28}{subsubsection.6.2.5}
\contentsline {subsubsection}{\numberline {6.2.6}CTR}{28}{subsubsection.6.2.6}
\contentsline {subsubsection}{\numberline {6.2.7}3DES}{28}{subsubsection.6.2.7}
\contentsline {subsubsection}{\numberline {6.2.8}IDEA}{28}{subsubsection.6.2.8}
\contentsline {subsubsection}{\numberline {6.2.9}RC2 -RC4 -RC5}{29}{subsubsection.6.2.9}
\contentsline {subsection}{\numberline {6.3}AES}{29}{subsection.6.3}
\contentsline {section}{\numberline {7}Game's manual}{30}{section.7}
\contentsline {subsection}{\numberline {7.1}Getting Started}{30}{subsection.7.1}
\contentsline {subsubsection}{\numberline {7.1.1}Windows Users}{31}{subsubsection.7.1.1}
\contentsline {subsubsection}{\numberline {7.1.2}Android User}{32}{subsubsection.7.1.2}
\contentsline {section}{\numberline {8}Game main components}{32}{section.8}
\contentsline {subsection}{\numberline {8.1}Learn Tab}{33}{subsection.8.1}
\contentsline {subsubsection}{\numberline {8.1.1}Quiz Game Play}{34}{subsubsection.8.1.1}
\contentsline {subsubsection}{\numberline {8.1.2}Text Game Play}{34}{subsubsection.8.1.2}
\contentsline {subsubsection}{\numberline {8.1.3}Block Game Play}{35}{subsubsection.8.1.3}
\contentsline {subsection}{\numberline {8.2}Insight Tab and Me Tab}{36}{subsection.8.2}
\contentsline {section}{\numberline {9}Programmer's Manual}{41}{section.9}
\contentsline {subsection}{\numberline {9.1}Implementation of the security-serious Game based on KBT}{41}{subsection.9.1}
\contentsline {subsubsection}{\numberline {9.1.1}Gettin Started with the BigBro code}{41}{subsubsection.9.1.1}
\contentsline {subsubsection}{\numberline {9.1.2}Menu Scene and its scripts}{41}{subsubsection.9.1.2}
\contentsline {subsubsection}{\numberline {9.1.3}Levelsel Scene and stageManager.cs}{44}{subsubsection.9.1.3}
\contentsline {subsubsection}{\numberline {9.1.4}Question Scene and QuestionManager.cs}{44}{subsubsection.9.1.4}
\contentsline {subsection}{\numberline {9.2}The Game Manager class : GameManager.cs}{45}{subsection.9.2}
\contentsline {subsection}{\numberline {9.3}Levels and question definition in XML files}{48}{subsection.9.3}
\contentsline {subsubsection}{\numberline {9.3.1}Level's File}{48}{subsubsection.9.3.1}
\contentsline {subsubsection}{\numberline {9.3.2}Levels Tag}{48}{subsubsection.9.3.2}
\contentsline {subsubsection}{\numberline {9.3.3}Tips and Jokes Tag}{50}{subsubsection.9.3.3}
\contentsline {subsubsection}{\numberline {9.3.4}Questions Tag}{50}{subsubsection.9.3.4}
\contentsline {subsubsection}{\numberline {9.3.5}Quiz Game Play Format}{51}{subsubsection.9.3.5}
\contentsline {subsubsection}{\numberline {9.3.6}Text Game Play Format}{51}{subsubsection.9.3.6}
\contentsline {subsubsection}{\numberline {9.3.7}Block Game Play Format}{52}{subsubsection.9.3.7}
\contentsline {subsection}{\numberline {9.4}KNOWLEDGE BAYESIAN COMPONENT}{52}{subsection.9.4}
\contentsline {subsubsection}{\numberline {9.4.1}Attributes and Constructor of KBT Class}{52}{subsubsection.9.4.1}
\contentsline {subsubsection}{\numberline {9.4.2}Example}{53}{subsubsection.9.4.2}
\contentsline {subsubsection}{\numberline {9.4.3}Query Methods}{53}{subsubsection.9.4.3}
\contentsline {subsubsection}{\numberline {9.4.4}Command Methods}{54}{subsubsection.9.4.4}
\contentsline {subsubsection}{\numberline {9.4.5}Model Degeneracy}{55}{subsubsection.9.4.5}
