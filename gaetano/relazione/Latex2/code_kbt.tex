\subsection{KNOWLEDGE BAYESIAN COMPONENT}
\label{codek}
The code presented is extracted from a class that model a BKT of a  Knowledge component. It is composed by two different sets of methods command and queries according to Bertrand Mayer's CQS principle. 

The first set (query set \ref{queryset})  returns values related to the probability for a student to know a specific topic in a particular time concerning the past performances.
The second set (command set \ref {commandset}) works after the system has collected a certain amount of performances, and it can update the four parameters of BKT making them fit the collected data. These methods took as input a list of performances.

\subsubsection{Attributes and Constructor of KBT Class}

The first couple of integers (lid, sid) are the key to determine the specific level of the video game (i.e. the Knowledge Component) that is characterised by a set of tests. Each test has a list of past performances modelled as KcRecords \ref{lst:kcrec}. 
The following four binary variables are the four parameters that characterised a BKT. The KnownParameter (Ln) represents the probability for a student to know the particular KC. The integer PosTransition is used to represent the Knowledge array. For each performance (observation) that can be 0 or 1 (right or wrong answer), there is a latent variable representing the knowledge state that can be either 0 or 1 (learned or not learned). Since BKT model allows transitions from not learned to learned only, the knowledge array is a sequence of zeros followed by a list of ones. Both lists can be empty. In this case, the array is encoded as the transition position. 

\begin{lstlisting}[label={lst:kc}]

enum ApproachType {Baseline, Bounded}; 

public class KnowledgeComponent{
    private int lid,sid;  
    private double TransitionParameter;
    private double SlipParameter;         
    private double GuessParameter;         
    private double AlreadyKnownParameter;         
    private double KnownParameter;         
    private int PosTransition; 
    private const int ENOUGH_TESTS = 1000;         
    private const double COGNITIVE_MASTERY = 0.95;         
    private ApproachType type; 
} 

\end{lstlisting}

\begin{lstlisting}[label={lst:kcrec}]
public class KcRecord {
    public string timestamp;         
    public int grade;     
    public  bool value; 
}     
\end{lstlisting}

\subsubsection{Example}

\begin{lstlisting}
    int PosTransition = 5
    KnowledgeArray = [0,0,0,0,0,1,1,1,1,1,1..]  
\end{lstlisting}

 The constant \verb|ENOUGH_TESTS|\ref{lst:kc} represents the minimum amount of collected performances to run the methods that estimate the four KBT parameters that fit the data. This model needs a huge number of tests because it is affected by identifiability \cite{beckchang} i.e. more combination of the three KBT parameter can predict the same observations especially where there are not enough data. The constant \verb|COGNITIVE_MASTERY| \ref{lst:kc} is the minimum value of KnownParameter\ref{lst:kc} that a student should reach before to consider the specific topic learned. The system will recommend tests on this KC until the KnownParameter meets this requirement. ApproachType\ref{lst:kc} is an enumeration variable that can be or Baseline or Bounded. The baseline approach allows each BKT parameters to be between 0 and 1. The second approach is the Bounded guess and slip method called Bounded that limits the guess parameter between 0 and 0.3 and the slip parameter between 0 and 0.1.    

\begin{lstlisting}
public KnowledgeComponent(int lid, int sid, 
 double AlreadyKnownParameter, ApproachType type) 
\end{lstlisting}

The constructor receives as input the KC (the first couple of integers is the couple for a specific Knowledge component), the AlreadyKnownParameter and the ApproachType. 
The AlreadyKnownParamater can be correctly set as the average of the first performance of a certain amount of students on that KC. Otherwise, a safe approach is to set it to a small value.    
The other parameters are set with fixed values to make the transition not easy. This means that in the most of the case the skill is considered more difficult than it is.  Here in the table \ref{tab:kbtkbt} an example of how the parameters evolve to reach the mastery (Ln > 0.95) after seven right answers in a row. 
\begin{table}
\begin{center}
\begin{tabular}{|p{3cm}|p{3cm}|p{3cm}|p{3cm}|p{3cm}|}
\hline
Attempt & Action & Ln-1 & Ln-1|Actual & Ln\\ [0.5ex] 
\hline
1&1&0.01&0.02941176&0.039118\\
2&1&0.039118&0.10883797&0.11775\\
3&1&0.11775&0.28591583&0.293057\\
4&1&0.293057&0.55429205&0.558749\\
5&1&0.558749&0.79161689&0.793701\\
6&1&0.793701&0.92026777&0.921065\\
7&1&0.921065&0.97222685&0.972505\\
\hline
\end{tabular}
\end{center}
\caption{\newline Evolution in three time steps of the knowledge component}\label{tab:kbtkbt}}
\end{table}


\subsubsection{Query Methods}
\label{queryset}
\begin{itemize}

\item{
\begin{lstlisting}
public double GetKnownParameter()  
\end{lstlisting}
This method returns the probability for a student to know the specific KC. It is the Ln KBT parameter.
}
\item{
\begin{lstlisting}
public bool StudentHasReachedMastery() 
\end{lstlisting}

According Cognitive Mastery Learning \cite{anderson} a student should practise each skill until the \(P(L_{n})\) has reached a certain threshold (mastery). Continuing to practise after mastery has reached does not lead to better performances \cite{koedinger}.  In the same paper Corbett indicate 0.95 as a fair choice.       
}
\item{
\begin{lstlisting}
public double ProbabilityStudentWillGetNextPerformanceRight() 
\end{lstlisting}
This method can be used to predict the probability for a student to get the right answer on the next attempt. It used this formula:
\[P(Corr_{n}) = P(L_{n}) * (1-P(S)) + P(~Ln) * P(G) \]
}
\end{itemize}

\subsubsection{Command Methods}
\label{commandset}
\begin{itemize}
\item{
\begin{lstlisting}
public void FitThisParametersUpdate()
\end{lstlisting}
This function is called to update the state of the KBT if the system has collected more than ENOUGH\_TESTS performances. In this case it saves all the parameters and then it invokes the following methods. If the newer parameter does not satisfy the approach constraints it will restore the previous state.     
}
\item{
In this stage we have collected  performances and we use an heuristic for determining the moment when the transition from not learned to learned happened. The proposed method chooses the best hypothesis i.e. the knowledge sequence that best matches the past performances. This methodology is known as EP (i.e. empirical probability)   
If the past performances are a sequence like  " 0 1 0 1 1 1 1 1 ", the knowledge sequence that has a transition from not learned to learned in the time t = 0 (already known) fits it for the 75\% of the cases : 

\begin{center}
\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|}
\hline
Past Performances:&0&1&0&1&1&1&1&1&\\
\hline
Knowledge sequence (t=0):&1&1&1&1&1&1&1&1&\\
&F&T&F&T&T&T&T&T&75\%\\ 
\hline
Knowledge sequence (t=1):&0&1&1&1&1&1&1&1&\\   
&T&T&F&T&T&T&T&T&87\%\\ 
\hline
Knowledge sequence (t=3) :&0&0&0&1&1&1&1&1&\\    
&T&F&T&T&T&T&T&T&87\%\\                          
\hline
\end{tabular}
\end{center}
}
\end{itemize}

  
In this case, the second hypothesis fits the data better than the first one. Notice that two hypotheses equally matches the past performances. Therefore if they have the highest probability value, we can consider taking the transition position as the average of them.   

\begin{lstlisting}
private void UpdateTransitionParameter(int transition, int count)
\end{lstlisting}

This function calculates the new Transition Parameter concerning the list of the known sequence calculated in the previous method (K array ) and it calculates the probability of changing state from not learned to learned :

\[
    P(T) =  \frac{\sum_{i\neq0}(1-K_{i})*K_{i}}{\sum_{i\neq0}(1-K_{i-1})}
\]

Where K is the knowledge prediction sequence in the previous method.    

\begin{lstlisting}
void UpdateGuessParameter(List<GameManager.KcRecord> records, 
int transition)
\end{lstlisting}
 
This function calculates the new guess parameter for the list of past performances and the 'knowledge' sequence, and it updates the new guess parameter in the current KBT.

\[
    P(G) =  \frac{\sum_{i\neq0}(1-K_{i})*C_{i}}{\sum_{i\neq0}(1-K_{i})}
\]

\begin{lstlisting}
void UpdateSlipParameter(List<GameManager.KcRecord> records, 
int transition)
\end{lstlisting}
 
This function takes as input the past performances and the predicted knowledge sequence, and it calculates the new slip parameter in the current KBT.

\[
    P(S) =  \frac{\sum_{i\neq0}(1-C_{i})*K_{i}}{\sum_{i\neq0}(K_{1})}
\]

\begin{lstlisting}
void UpdateKnownParameter(bool LastPerformance )
\end{lstlisting}

This method takes as input the last performance and it calculates the probability to know the skill at the moment it is invoked. 

\[P(L_{n} \vert Action_{n} )= P(L_{(n-1)} \vert Action_{n} )+(1-P(L_{(n-1)}|Action_{n}))* P(T)
\]

\subsubsection{Model Degeneracy}

A BKT model can theoretically degenerate when its guess and slip parameter is greater than 0.5 because it breaks the link between tests and knowledge. A model such that can also be empirically degenerate if violated the relationship between knowledge prediction and actions.  In this implementation, we tested the theoretical degeneracy as well as the empirical ones.

\begin{lstlisting}
private bool TestFirstEmpiricalModelDegeneration(int N)
private bool TestSecondEmpiricalModelDegeneration(int M)
\end{lstlisting}

The first empirical degeneracy test checks if after  N correct actions the probability of know the skill is higher than before. 
\[
L_{n+N} > L_{n} 
\]
if the last N actions are correct.

The second empirical degeneracy test checks if after M right consequently actions the student reaches the Mastery.

N, M are arbitrary and in this case, we use N=3 and M=10 as suggested by Baker and Corbett in their paper \cite{corbett}.
