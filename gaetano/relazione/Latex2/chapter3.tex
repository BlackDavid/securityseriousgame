\section{Knowledge models and representation}

The goal of a Serious game is definitely to merge the learning and playing activity. 

It means we should assure that through the proposed game we rightly make people learn. 
Old questions arise in this context: how to transmit knowledge? How to measure it? What do we mean saying knowledge?     
  
Since we are not yet able to look into the brain in this section, we will explore mathematical models to define and measure knowledge indirectly.   

This process is often called 'Latent Knowledge Estimation' because the measure is not direct.    
Knowledge, the measure we want to estimate, appears in different ways. It can be a set of beliefs, judgments, skills, experiences and finally connections, the way how these items are connected each other.

The field that studies the different way to encode knowledge is the knowledge representation (often referred as KR in papers). It is a field of interest of both artificial intelligence and epistemology. Its goal is to find a formal thus machine-readable, representation for knowledge.

The easiest way to represent and model knowledge is to break it into small atomic pieces called knowledge components (KC).

Formally a knowledge component is the description of a mental structure that can be combined with other to solve a complex task.
 
What we would be able to do is to find and measure knowledge component at a specific time. 

We model the learner's brain like a black box and look their performance over the time because surely there is a link between performance and knowledge but we have to admit that this is not a solid link.

These kind of model are essential in all ITS systems (Intelligent Tutoring Systems) because it can inform tutors and all people involved in the education about students performances on a specific task.

\subsection{Knowledge Bayesian Tracing}
\label{kbt}

During the 60s, some researchers started to explore the learning activity models and the first proposal for measuring the learning activity came by Richard Chatman Atkinson. Sir Atkinson proposed a bayesian model that is known as BKT : Bayesian Knowledge Tracing.
Due to its simplicity, BKT is one of the most popular ways to model a student in tutoring systems.

The BKT model, under some constraints, gives a measure of one knowledge element in a specific time considering the performances related to that piece of knowledge, in the model known as knowledge component.

For every KC the relative BKT model has an item. This item is composed of two possible current states (learn or not), tests, the array of the tests' performances. Each BKT item is characterised also by a set of parameters that regulate the state transitions and the probability to get the right answer in the current state.  

After enough performances of the KC item tests, a BKT compute the probability \(PL_{n}\)  that determines the state that indicates how likely the KC is known. Then concerning this state that can be learned or not learned, it can predict if the student will get the right answer PCORR. 
In BKT, performances, as well as knowledge states, are encoded with only two values. It means that each performance is evaluating with a binary score: 0 or 1, in other words only right and wrong, are allowed. It doesn't take into account how wrong an answer can be.

One limit of BKT is that every item must have only one KC. It means that the test should deal with only one KC and it should not involve other KCs.

For those who are familiar with, this is a particular case of Hidden Markov model where the hidden states are 'learned', and 'not-learned' and the answers are the observations.

The four parameters that characterised the BKT models the probability to change the state and consequently the probability to get the right answer. 

\subsubsection{Knowledge Bayesian Tracing Parameters}

A KBT is characterized by four parameters called Learned at time 0 (\(L_{0}\)), Transition State (T), Guess (G) , Slip (S). \myfig{\ref{fig:kbt}}
\begin{itemize}

\item    \(P(L_{0})\) represents the probability that the skill (KC) is known before the first opportunity to test it during a performance. It is the initial state. 
\item    \(P(T)\) represents the probability of learning a skill at each opportunity to test it. The model allows the state change from not learned to learned after each performance ( conversely it is not authorised from learned to learned) 
\item    \(P(G)\) is the probability for a student to guess when he is in not learned state. 
\item    \(P(S)\) is the probability for a student to make a mistake when he is in the learn state. 
\end{itemize}

\begin{figure}[h]
% If the picture uses fonts of the correct size (10 ... 12 pt)
% then can be included without scaling
\centerline{\includegraphics[scale=0.4]{kbt.png}}
% otherwise see the example in the following (commented out) line
% to scale it relatively to the page width
%   \centerline{\includegraphics[width=0.9\textwidth]{handshake.pdf}}
\caption{The KBT Bayesian network representation}
\label{fig:kbt}
\end{figure}   


We calculate the probability for a student to get the right answer when he is in the learned state as \(1-P(S)\). Conversely, the probability for a student to get the wrong answer when he is in the not learned state is \(1-P(G)\).  
 
Therefore the probability to get the right answer is the probability to do not slip the answer when the student is in the learned state times the probability to guess it when he is in the not learned state \((Ln)\).\newline
Please notice that \(P(\sim Ln) = 1-L(n)\)


\[
    P(Corr_{n})= P(L_{n} )*(1-P(S))+ P(\sim L_{n} )*P(G)
\]

Reasoning on the previous result and by applying the Bayes theorem, we can obtain the probability for a student to know it before the nth test and did it correctly in the nth  attempt as 

\[
    P(L_{(n-1)} \vert Corr_{n} )=  \frac{P(L_{(n-1)} \cap Corr_{n} )}{(P(Corr_{n}))}
\]

\[
    P(L_{(n-1)} \vert Corr_{n}) = \frac{(P(L_{(n-1)})*(1-P(S))}{(P(L_{(n-1)})*(1-P(S)+P(\sim L_{(n-1)})*(1-P(G)))}
\]

On the other hand, the probability for a student knew it before the nth test and did it incorrectly is

\[
    P(L_{(n-1)} \vert \sim Corr_{n})=  \frac{(P(L_{(n-1)})*(P(S))}{(P(L_{(n-1)})*(P(S)+P(~L_{(n-1)})*(P(G)))}
\]


Finally, we obtain the probability for a student to know the knowledge component (skill)  after the action, either right or wrong  is


\[P(L_{n} \vert Action_{n} )= P(L_{(n-1)} \vert Action_{n} )+(1-P(L_{(n-1)}|Action_{n}))* P(T)
\]


This model works on three strong assumptions:
\begin{enumerate}
    \item The knowledge component concerning the learner can be known or not known. 
    \item The knowledge component can be learned at each opportunity he has to apply the skill.
    \item The Knowledge component once was known cannot be forgotten.
\end{enumerate}

Of course, all these assumptions are wrong in the real world, but under some conditions, like time spaced practice where you have to do the same problem in a short time, it is not unreasonable \cite{ryan_1}.

Please notice that the first assumption doesn't lead to the conclusion that once we know something we will always get the right answer, there is still some possibility that the learner makes a mistake \(P(S)\). On the other hand, a learner that does not know the answer can guess it correctly \(P(G)\).
In other words, the array that stores the performances for each time is not necessarily the same of the Knowledge state array because performances are indirect observations of the latent variable, in this case, the knowledge.   

