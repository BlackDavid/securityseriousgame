\subsection{Recap on Symmetric Cryptography}
\label{recap_crypto}
Symmetric algorithms use only one key for both the two counterparts, the encryption of the plaintext and the decryption of ciphertext. 
In symmetric cryptography, there are two classes of algorithms:

\begin{itemize}
    \item Block algorithm: data to be encrypted are split into block of the same size and they are processed once at time
    \item Stream algorithm: data are processed per bit or byte, and they are mainly used on data stream
    \label{symmetric}
\end{itemize}

In symmetric cryptography two properties should be assured:

\begin{itemize}
    \item Diffusion: a modification in the plaintext should bring a comprehensive adjustment in the cypher-text
    \item    Confusion: a change in the plaintext should alter the cypher-text such that would be not clear what was changed in the plaintext.
\end{itemize}

Those properties can make an encrypted message safer, but at the same times in a transmission context, each error propagates potentially into all cyphertext making the decryption operation not possible at all. 

Moreover, we want those algorithms resistant against two potential vulnerabilities: 

\begin{itemize}
    \item Swapping: encryption is dependent on the position of the block. People can retrieve information based on some blocks position.  
    \item    Known-plaintext: same blocks in different data are encrypted in the same way. 
\end{itemize}

Malicious people could swap blocks and change the original data or could use a database containing the most common blocks (e.g. header of the common type of files) encrypted by a large set of keys and then compare the block of the cypher text to those to find the original keys. 

The symmetric methods that split the data into fixed size blocks could require filling the last block because can happen that the plaintext is not an integer multiple of the block size. For this reason, it is mandatory to specify which padding techniques are used to fill the last block to decrypt the ciphertext. 

\subsubsection{ECB}
\label{ecb}
The Electronic Code Book splits the plaintext into blocks of fixed size. Each block is encrypted applying the encryption algorithm independently with no respect to the previous ones. The ciphertext is the sorted concatenation of each encrypted block. 

\[
    C_{i} = enc(Key,P_{i})
\]

Decryption is made by splitting the ciphertext with the same fixed size of the encryption and applying the decryption algorithm 

\[
    P_{i} = enc-1 (Key, C_{i})
\]

This algorithm can be executed in parallel and errors in transmission affect only the belonging blocks since the independence of each block from the others,
However, this method suffers from the swapping and known-plaintext vulnerabilities. 

\subsubsection{CBC}
\label{cbc}
The Cypher Block Chaining splits the plaintext into blocks of fixed size starting from index 0. Each block is (\(XOR_{ed}\)) with the block resulting of the previous encryption. The first one is XORed with a particular known random block called initialization vector (IV). Each encrypted block depends on all previous plaintext (against the swapping).  

\[
C_{i} = enc(Key, P_{i} XOR C_{i-1})  if   i>0
\]
\[            
C_{0} = enc(Key, P_{i} XOR IV)    if  i=0
\]

Considering that the reverse operation of the XOR is the XOR itself, decryption is 

\[ 
P_{i} = enc\textsuperscript{-1}(Key, C_{i}) XOR C_{i-1} if i>0
\]
\[
P_{i} = enc\textsuperscript{-1}(Key, C_{i}) XOR IV    if i=0
\]
        
The IV is a block of unpredictable and random bytes; it should be used a new one for each transmission.

This method prevents the swapping and known-plaintext vulnerabilities, however, a transmission error in the (\(n_{th}\)) block will determine an error in the following next blocks 
\[ 
(C_{i} i > n) 
\]

Moreover, since there is a high dependency among the blocks parallelization is not possible.


\subsubsection{CTS}

Cypher text stealing extends the previous techniques (ECB \ref{ecb} or CBC \ref{cbc}) solving the padding problem with the ciphertext that is the same size as plaintext.
Plaintext is divided into n blocks of the same size, and the last one could not correctly fit the block size.  
The first n-2 blocks are encrypted per one of ECB and CBC technique. The n-1 th block is split into two parts called head and tail so that the tail has the size needed by the last block to perfectly fit the block size. 
The last block concatenated with the tail of the penultimate block has the block size and can be encrypted as well as the others.
The n-th block of the cypher text obtained encrypting the last bytes of the plaintext plus the tail is concatenated with the n-2 cypher blocks and finally is concatenated the head to the previous cypher-blocks.

\subsubsection{CFB}

Cypher Feedback encrypt one group of bits per time, and the resulting cypher text is used to feed the next step of encryption. The plaintext is split in the group of N bits ( N depends on the encryption algorithm chosen, and it is smaller than the size of the block). Each i-th block of the plaintext is stored in a left shift register, and it is encrypted using a particular key Ki.  We took the N leftmost bits of the left shift register and we XORed with the plaintext block (Pbi) obtaining the group Cbi that is ready to be sent. Cbi is also stored in the right most of the left shift register. This technique needs an initialization vector to fill the left shift register for the first group encryption.  Please notice that a transmission error will affect the decryption until the error is in the register. 


\subsubsection{OFB}

The output feedback mode is like the CFB, but it feeds the left shift register, not with the result of the XOR operation between the plaintext and the N bit of the encrypted block. It feeds the register with the previous block encrypted before the XOR operation.


\subsubsection{CTR} 

The counter mode does not use a shift register that is feed from the previous encrypted block but uses a register that stores the combination of a counter with a nonce. The combination can be a Boolean operation, concatenation,  sum or a combination of them. An error in transmission will affect a decryption error only for that group. It can be executed in parallel.

\subsubsection{3DES}

3DES is a symmetric stream algorithm \ref{symmetric} therefore it does not require to split data into blocks. Each bit or byte of the stream is matched with a given key obtaining the ciphertext data stream.
The encryption and decryption process requires using the same key. This technique is based on an algorithm called DES (Data Encryption Standard) that uses a 56 bit long effective key the actual key is 64 bit and the remaining 8 bit are used for parity) and hardware techniques (XOR, Bit shifts) to obtain the ciphertext. DES was proved to be vulnerable by Ron Rivest and the newer version called 3DES consists in the application of the DES algorithm three times, potentially using three different keys with an effective key 168 bit long. Notice that there is still the possibility to use only two keys reducing the effective key from 168 bit to 112 bit ( ${Key_{1} = Key_{3}}$ ).
The algorithm is usually implemented in the EDE mode (Encrypt, Decrypt and Encrypt again)

\begin{enumerate}
        \item 
        ${
        C_{1} = enc (Key_{1}, P) 
        }$
        \item
        ${
        C_{2} = dec (Key_{2}, C_{1})
        }$
        \item
        ${
        C = enc (Key_{3}, C_{2})
        }$
\end{enumerate}


\subsubsection{IDEA}

IDEA is a stream symmetric algorithm \ref{symmetric} that requires a 128-bit long key, blocks of 64 bit and hardware operations like ${ XOR, mod16, mod2^{16+1} }$  
The required operations are hardware primitives (${mod2^{16+1}}$ is a hardware primitive in some CISC processors) making IDEA a good fit for 16-bit CPUs. Its implementation requires a license owned by Ascom-Tech AG. It requires small loyalties for business use, and it is free for non-commercial use (e.g. it is used by PGP). 
  
\subsubsection{RC2 -RC4 -RC5}

Ron Rivest developed RC2,RC4 and RC5 ( RC stands for Ron Rivest Code) and Rivest's company RSA owns them but they are not patented. RC2 was published as a contestant of the AES competition that would have to choose one of the competitors as the standard encryption algorithm. RC2 is a block algorithm whereas RC4 is a stream algorithm.  Both of them are implemented at a software level and the latter can be up to 10 times faster of DES.     
RC5 is a block algorithm developed targeting the WAP (Wireless Application Protocol). The algorithm was designed to work with variable key and block size. It is optimised to work with the block size as long as twice a word in the running CPU. It uses hardware operation (bit shifts, bit rotations, modular additions)

\subsection{AES}
\label{aes}
DES vulnerability found by Ron Rivest pushed the United States government to find another standard for encryption. Among the contestants, Rijndael algorithm by Vincent Rijmen and Joan Daemen was chosen. It can use a key of 256-bit with blocks of 128-bit size. It does not require much more additional memory, and it performs well on hardware and software. Those are the reason why it was chosen among the others and probably because it was developed by non-American specialists and the USA wanted to appear neutrally in its choice. Even if it is a standard and it was published in 2001, it is still not fully adopted because it takes a long time to be sure that it is not vulnerable.


