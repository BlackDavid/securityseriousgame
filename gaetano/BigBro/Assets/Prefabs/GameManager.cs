﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.IO;
using System.Xml.Serialization;
using System.Text;

public class GameManager : MonoBehaviour
{
   
    public class Stage
    {
        public string desc;
        public int stage_number;
        public int count;
        KBTManager.KnowledgeComponent KBT = 
            new KBTManager.KnowledgeComponent(0.001,0,KBTManager.ApproachType.Bounded);
        public List<Question> questions = new List<Question>();

        public void UpdateKBT(bool lastPerformance)
        {
            this.KBT.UpdateKnownParameter(lastPerformance);
        }
        public double getKnownParameter()
        {
            return this.KBT.GetKnownParameter();
        }

        public int getAttemptsNumber()
        {
            return this.KBT.GetAttempts();
        }

        public bool StageMastery()
        {
            return this.KBT.StudentHasReachedMastery();
        }

        public void setKBT(double knownpara,int attempts)
        {
            this.KBT = new KBTManager.KnowledgeComponent(knownpara, attempts, KBTManager.ApproachType.Bounded);
        }


    }

    public class Level
    {
        public int lid;
        public string title;
        public string description;
        public string icon;
        public List<Stage> stages = new List<Stage>();
        
    }

    public class Question
    {
        public int qid;
        public int gameplay;
        public string text;
        public string joke;
        public string tip; 
        public List<Answer> answer = new List<Answer>();

    }

    public class KcRecord
    {
        public string timestamp;
        public int position;
        public int grade;    
        public  bool value;
    }


    public class Answer
    {
        public int value;
        public string text;

    }

    public static void Shuffle<T>(IList<T> list)
    {
        RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();
        int n = list.Count;
        while (n > 1)
        {
            byte[] box = new byte[1];
            do provider.GetBytes(box);
            while (!(box[0] < n * (Byte.MaxValue / n)));
            int k = (box[0] % n);
            n--;
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

 

    public string player_name;
    private string player_surname = "";
    public int player_daycounter;
    public int nbadges = 2;
    public List<String> badges = new List<string>();
    public System.DateTime lastactivity;
    private string level_id;
    private string scene_id;
    private Question current_question;
    private List<Level> levels_coll = new List<Level>();
    public TextAsset player_xml;
    public TextAsset levels_xml;
    public Dictionary<int,List<KcRecord>> KcAnswers;    
    public UnityEngine.UI.Text debug;
    public string FileNamePlayer = "Player.aaa";
    public string FileNameAnswer = "Answer.xml";
    private System.Object PlayerFileLock = new System.Object();
    private System.Object AnswerFileLock = new System.Object();
   


    public static GameManager instance;

    void Awake()
    {
        //implemento il singleton del game manager
        if (instance == null)
            instance = this;
        else if (instance != this)
            DestroyObject(gameObject);
        LoadInfo();
        //Evito che il game object venga distrutto al cambio scena
        DontDestroyOnLoad(gameObject);

    }

    public void LoadInfo()
    {
        try
        {
           loadLevelInfo();
           LoadQuestionInfo();
           LoadTip_JokeInfo();
            LoadPlayerInfo();
        }
        catch
        {
            Debug.Log("parsing erroe");
        }
        }


     public void changeScene(string scene,string lid, string sid)
    {
        scene_id = sid;
        level_id = lid;
        SceneManager.LoadScene(scene);

    }   

    public void setLevelid(string lid)
    {
        this.level_id = lid;
    }

    public string getLevelid()
    {
        return this.level_id;
    }

    public string getSceneid()
    {
        return this.scene_id;
    }

    public void setStageid(string sid)
    {
        this.scene_id = sid;
    }

    public List<Stage> loadStages()
    {
        foreach (Level l in levels_coll)
        {       
            if (l.lid == Int32.Parse(level_id))
                return l.stages;
        }
        return null;
    }

    public List<Level> loadLevels()
    {
        return levels_coll;
    }

    public Stack<Question> loadQuestions(int lid, int sid)
    {
    
        List<Question> list = new List<Question>();
        foreach (Level l in levels_coll)
        {
            if (l.lid == lid)
                foreach (Stage s in l.stages)
                    if (s.stage_number == sid)
                    {
                        list = new List<Question>(s.questions);
                        Shuffle<Question>(list);
                        return new Stack<Question>(list);
                    }
        }
        return null;
    }

    public string getName()
    {
        return instance.player_name+" "+instance.player_surname; 
    }

    public string getDayCount()
    {
        return "on less than 7 day streak ";// + //instance.player_daycounter.ToString() + " day streak";
    }

    public void LoadPlayerInfo()
    {
       
        lock (PlayerFileLock)
        {
            XmlDocument doc = loadDocument("Player");
            XmlNodeList player = doc.DocumentElement.SelectNodes("/BigBro/Player");
            
            foreach (XmlNode n in player)
            {
                foreach (XmlNode nn in n)
                {
                    switch (nn.Name)
                    {
                        case "name":
                            player_name = nn.InnerText;
                            //nn.InnerText = "Gae";
                            //doc.Save("Assets/custom/Player.xml");         
                            break;
                        case "surname":
                            player_surname = nn.InnerText;
                            break;
                        case "lastactivityn":
                            if ((DateTime.Parse(nn.InnerText) - DateTime.Now).TotalSeconds < 0)
                            {
                                lastactivity = DateTime.Now;
                            }
                            else
                            {
                                lastactivity = DateTime.Parse(nn.InnerText);
                            }
                            break;
                        case "daycounter":
                            player_daycounter =  Int32.Parse(nn.InnerText);
                            break;
                        case "answers":
                        /*foreach (XmlNode answer in nn)
                        {
                            int quid = Int32.Parse(answer.Attributes["qid"].InnerText);
                            List<KcRecord> lista = new List<KcRecord>();
                            foreach (XmlNode record in answer)
                            {
                                KcRecord kc = new KcRecord();
                                kc.timestamp = record.Attributes["ts"].InnerText;
                                if (record.Attributes["value"].InnerText.Equals("true")) kc.value = true;
                                else kc.value = false;
                                if (KcAnswers == null) KcAnswers = new Dictionary<int, List<KcRecord>>();
                                if (!KcAnswers.ContainsKey(quid))
                                {
                                    lista = new List<KcRecord>();
                                    lista.Add(kc);
                                    KcAnswers.Add(quid, lista);
                                }
                                else
                                {
                                    lista = KcAnswers[quid];
                                    lista.Add(kc);
                                    KcAnswers[quid] = lista;
                                }
                            }
                        }
                        break;
                        */
                        case "KnowledgeTracing":
                            foreach (XmlNode node in nn)
                            {

                                foreach (Level l in levels_coll)
                                    foreach (Stage s in l.stages)

                                        if (l.lid.ToString().Equals(node.Attributes["lid"].Value))
                                            if (s.stage_number.ToString().Equals(node.Attributes["sid"].Value))
                                            {
                                                s.setKBT(Double.Parse(node.Attributes["knownpar"].InnerText),
                                                    Int32.Parse(node.Attributes["attempts"].InnerText));
                                            }
                            }
                            break;
                        case "achievements":
                            nbadges = Int32.Parse(nn.Attributes["n"].InnerText);
                            foreach (XmlNode nnode in nn)
                            {
                                if(nnode.Name == "achievement")
                                {
                                    foreach (XmlNode nnnode in nnode)
                                        if (nnnode.Name == "Title")
                                            badges.Add(nnnode.InnerText);
                                    Debug.Log("BADGE"+nnode.InnerText);
                                }
                            }
                            break;
                        default:
                            //error
                            break;
                    }
                }
            }
        }
    }

    public void insertAnswer(int quid,KcRecord value)
    {
        List<KcRecord> lista = new List<KcRecord>();
        if(!KcAnswers.ContainsKey(quid))
        {
            lista.Add(value);
            KcAnswers[quid] = lista;
        }
        else
        {
            lista = KcAnswers[quid];
            lista.Add(value);
            KcAnswers[quid] = lista;
        }

    }

    public XmlDocument loadDocument(string namefile)
    {

        XmlDocument doc = new XmlDocument();
        try
        {
            doc.Load(Application.persistentDataPath + "/" + namefile+".txt");
        }
        //catch (FileNotFoundException nf)
        catch(Exception ec)
        {
            Debug.Log("playerrrrrrrSSS   erroe"+ Application.persistentDataPath + "/" + "Player.txt");
            TextAsset pl = (TextAsset)Resources.Load("Player", typeof(TextAsset));
            StringReader xml = new StringReader(pl.text);
            doc.Load(xml);
            doc.Save(Application.persistentDataPath + "/" + namefile);
            
        }

        return doc;

    }

    public void addAnswerRecordtoPlayerInfo2(KcRecord kc, int quid)
    {
        lock (PlayerFileLock)
        {
            XmlDocument doc = loadDocument(FileNamePlayer);

            XmlNodeList player = doc.DocumentElement.SelectNodes("/BigBro/Player");
            foreach (XmlNode n in player)
            {

                foreach (XmlNode nn in n)
                {
                    XmlNode record = doc.CreateNode(XmlNodeType.Element, "record", null);
                    record.Attributes.Append(doc.CreateAttribute("ts"));
                    record.Attributes.Append(doc.CreateAttribute("value"));
                    record.Attributes["ts"].InnerText = kc.timestamp.ToString();
                    record.Attributes["value"].InnerText = kc.value.ToString();
                    

                    switch (nn.Name)
                    {
                     //TODO: No need to group by quid ..    
                        case "answers":
                            foreach (XmlNode answer in nn)
                            {
                                if (answer.Attributes.GetNamedItem("qid").Value.Equals(quid.ToString()))
                                {
                                    answer.AppendChild(record);
                                    doc.Save(Application.persistentDataPath + "/Player.txt");
                                    return;
                                }
                            }
                            XmlNode newanswer = doc.CreateNode(XmlNodeType.Element, "answer", null);
                            newanswer.Attributes.Append(doc.CreateAttribute("qid"));
                            newanswer.Attributes["qid"].InnerText = quid.ToString();
                            newanswer.AppendChild(record);
                            nn.AppendChild(newanswer);
                            doc.Save(Application.persistentDataPath + "/Player.txt");
                            break;
                    }
                }
            }
        }
    }

    public void addAnswerRecordtoPlayerInfo(KcRecord kc, int quid)
    {
        lock (AnswerFileLock)
        {
            
            XmlDocument doc = new XmlDocument();
            doc.Load(Application.persistentDataPath + "/Answer.xml");
            XmlNodeList ans = doc.DocumentElement.SelectNodes("/answers");
            XmlNode record = doc.CreateNode(XmlNodeType.Element, "record", null);
            record.Attributes.Append(doc.CreateAttribute("qid"));
            record.Attributes.Append(doc.CreateAttribute("ts"));
            record.Attributes.Append(doc.CreateAttribute("value"));
            record.Attributes["ts"].InnerText = kc.timestamp.ToString();
            record.Attributes["qid"].InnerText = quid.ToString();
            record.Attributes["value"].InnerText = kc.value.ToString();
            ans[0].AppendChild(record);   
            doc.Save(Application.persistentDataPath + "/Answer.xml");

        }

    }

    public void savePlayertInfo()
    {
        //Application.persistentDataPath
       lock (PlayerFileLock) { 
       XmlDocument doc = loadDocument(FileNamePlayer);

        XmlNodeList player = doc.DocumentElement.SelectNodes("/BigBro/Player");
        foreach (XmlNode n in player)
        {
            foreach (XmlNode nn in n)
            {
                switch (nn.Name)
                {
                    case "name":
                        nn.InnerText = player_name;
                        //doc.Save("Assets/custom/Player.xml");
                        break;
                    case "surname":
                        nn.InnerText = player_surname;
                        break;
                    case "lastactivityn":
                            nn.InnerText = lastactivity.ToString();
                        break;

                   case "daycounter":
                            /*if (( lastactivity - DateTime.Now).TotalDays > 1 )
                            {
                                player_daycounter += 1;
                                nn.InnerText = player_daycounter.ToString();
                            }
                            nn.InnerText = player_daycounter.ToString();*/
                            break;
  
                        case "KnowledgeTracing":
                            nn.InnerText = "";
                            foreach (Level l in levels_coll)
                                foreach(Stage s in l.stages)
                            {
                                XmlNode scene = doc.CreateNode(XmlNodeType.Element, "KBT", null);
                                scene.Attributes.Append(doc.CreateAttribute("sid"));
                                    scene.Attributes.Append(doc.CreateAttribute("lid"));
                                    scene.Attributes.Append(doc.CreateAttribute("knownpar"));
                                    scene.Attributes.Append(doc.CreateAttribute("attempts"));
                                    scene.Attributes["sid"].InnerText = s.stage_number.ToString();
                                    scene.Attributes["lid"].InnerText = l.lid.ToString();
                                    scene.Attributes["knownpar"].InnerText = s.getKnownParameter().ToString();
                                    scene.Attributes["attempts"].InnerText = s.getAttemptsNumber().ToString();
                                    nn.AppendChild(scene);
                            }
                            break;
                        case "achievements":
                            nn.Attributes["n"].InnerText = nbadges.ToString();
                            break;
                        default:
                        //error
                        break;
                }
            }
        }
       
        Debug.Log("path is " + Application.persistentDataPath);
        doc.Save(Application.persistentDataPath + "/Player.txt");
            }
    }

    public void loadLevelInfo()
    {
        XmlDocument doc = new XmlDocument();
        //doc = new XmlDocument();
        StringReader xml = new StringReader(levels_xml.text);
        doc.Load(xml);
        XmlNodeList levels = doc.DocumentElement.SelectNodes("/BigBro/levels/level");
        foreach (XmlNode n in levels)
        {
            Level l = new Level();
            string id = n.Attributes["lid"].InnerText;
            l.lid = Int32.Parse(id);

            foreach (XmlNode nn in n)
            {
                switch (nn.Name)
                {
                    case "title":
                        l.title = nn.InnerText;
                        break;
                    case "description":
                        l.description = nn.InnerText;
                        break;
                    case "icon":
                        l.icon = nn.InnerText;
                        break;
                    case "stages":
                        foreach (XmlNode s in nn)
                        {
                            string counts = nn.Attributes["count"].InnerText;

                            switch (s.Name)
                            {
                                case "stage":
                                    Stage stage = new Stage();
                                    stage.stage_number = Int32.Parse(s.Attributes["sid"].InnerText);
                                    stage.desc = s.InnerText;
                                    stage.count = Int32.Parse(counts);
                                    l.stages.Add(stage);
                                    break;
                            }
                        }
                        break;
                    default:
                        //error
                        break;
                }
            }
            levels_coll.Add(l);
        }
    }

    public void LoadQuestionInfo()
    {
        XmlDocument doc = new XmlDocument();
        //doc = new XmlDocument();
        StringReader xml = new StringReader(levels_xml.text);
        doc.Load(xml);
        XmlNodeList player = doc.DocumentElement.SelectNodes("/BigBro/questions/question");

        foreach (XmlNode n in player)
        {
            Stack<string[]> stack = new Stack<string[]>();
            Question q = new Question();
            q.tip = "-1";
            q.joke = "-1";
            try
            {
                q.qid = Int32.Parse(n.Attributes["qid"].InnerText);
                q.gameplay = Int32.Parse(n.Attributes["gameplay_id"].InnerText);
                q.tip = n.Attributes["tid"].InnerText;
                q.joke = n.Attributes["jid"].InnerText;
                
            }
            catch
            {
               // Debug.Log("errore nel parsing" + q.qid + " " +q.tip+" "+ q.joke);
            }
            foreach (XmlNode nn in n)
            {
                switch (nn.Name)
                {
                    case "q":
                        q.text = nn.InnerText;
                        break;
                    case "a":
                        Answer a = new Answer();
                        a.value = Int32.Parse(nn.Attributes["grade"].InnerText);
                        a.text = nn.InnerText;
                        q.answer.Add(a);
                        break;
                    case "levels":
                        string[] lev = new string[2];
                        lev[0] = nn.Attributes["lid"].InnerText;
                        lev[1] = nn.Attributes["sid"].InnerText;
                        stack.Push(lev);
                        break;
                    default:
                        //error
                        break;
                }
            }
            foreach (string[] lev in stack)
            {
                foreach (Level l in levels_coll)
                    if (Int32.Parse(lev[0]) == l.lid)
                        foreach (Stage s in l.stages)
                            if (Int32.Parse(lev[1]) == s.stage_number)
                            {                               
                                s.questions.Add(q);
                            }
            }

        }
    }

    public void updateKBT(bool lastperformance, int lid, int sid)
    {
        Double KnowParameter = 0.01;
        foreach (Level l in levels_coll)
        {
            if (l.lid == lid)
                foreach (Stage s in l.stages)
                    if (s.stage_number == sid)
                    {
                        s.UpdateKBT(lastperformance);
                        KnowParameter = s.getKnownParameter();
                    }
        }


        lock (PlayerFileLock)
        {
            XmlDocument doc = loadDocument(FileNamePlayer);
            XmlNodeList player = doc.DocumentElement.SelectNodes("/BigBro/Player");
            //SAVE KBT IN XML

        }
    }
    public void LoadTip_JokeInfo()
    {
        XmlDocument doc = new XmlDocument();
        //doc = new XmlDocument();
        StringReader xml = new StringReader(levels_xml.text);
        doc.Load(xml);
        XmlNodeList tips = doc.DocumentElement.SelectNodes("/BigBro/tips/tip");
        XmlNodeList jokes = doc.DocumentElement.SelectNodes("/BigBro/jokes/joke");

        foreach (XmlNode n in tips)
        {
            try
            {
                string desc = n.InnerText;
                string id = n.Attributes["tid"].InnerText;
                foreach (Level l in levels_coll)
                {

                    foreach (Stage s in l.stages)
                        foreach (Question q in s.questions)
                        {
                            if (q.tip.Equals(id))
                                q.tip = desc;
                        }
                }
            }
            catch
            {
                continue;
            }
          

        }

        foreach (XmlNode n in jokes)
        {
            try
            {
                string desc = n.InnerText;
                string id = n.Attributes["jid"].InnerText;
                foreach (Level l in levels_coll)
                {
                    foreach (Stage s in l.stages)
                        foreach (Question q in s.questions)
                            if (q.joke.Equals(id))
                                q.joke = desc;
                }
            }
            catch
            {
                continue;
            }
        }

    }

    // Use this for initialization
    void Start()
    {
        //KcAnswers = new Dictionary<int, List<KcRecord>>();

    }

    // Update is called once per frame
    void Update()
    {

    }

}
