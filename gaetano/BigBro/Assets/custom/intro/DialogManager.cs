﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DialogManager : MonoBehaviour {

    public UnityEngine.UI.Text txt;
    public UnityEngine.UI.Text continue_txt;
    public UnityEngine.UI.Text skip;
    public int Number;

    int counter;
    CanvasGroup cg;
    CanvasGroup skip_cg;
    float quantity;

    // Use this for initialization
    void Start () {
        Number = 0;
        cg = txt.GetComponent<CanvasGroup>();
        skip_cg = skip.GetComponent<CanvasGroup>();
        skip_cg.alpha = 1f;
        quantity = 0.005f;
    }
	
    void Update()
    {

        if (skip_cg.alpha >= 1f || skip_cg.alpha <= 0f) quantity *= -1;
        skip_cg.alpha += quantity;

        if (cg.alpha < 1f) cg.alpha += 0.05f;
        if (cg.alpha >= 1f) continue_txt.GetComponent<CanvasGroup>().alpha += 0.5f;
        else continue_txt.GetComponent<CanvasGroup>().alpha = 0;
    }

	
   
	public void nextDialog() {
        cg.alpha = .0f;
        Number++;   
        switch (Number)
        {
            case 1:
                txt.text = "Hi, i'm BIG-BRO2 a Artificial Intelligence";
                break;
            case 2:
                txt.text = "I speak neolanguage, if you speak English just remeber to forget some useless world.";
                break;
            case 3:
                txt.text = "Welcome to this internship at the PARTY";
                break;
            case 4:
                txt.text = "let me remind you where we are : ";
                break;         
            case 5:
                txt.text = "This is the capital of Oceania, the greatest country in the world.";
                break;
            case 6:
                txt.text = "We are the bestcountry thanks to our centenarian leader: The Big Brother. ";            
                break;
            case 7:
                txt.text = "Even if no one have seen him, and many of foreigners consider him artificial ,";
                break;
            case 8:
                txt.text = " we love him, because we are programmed to do it.";
                break;
            case 9:
                txt.text = "He is also the Head of out four Ministry :";
                break;
            case 10:
                txt.text = "Minitrue, here we make propaganda and we write history, to make us bigger than in the past even when we are not.";
                break;
            case 11:
                txt.text = "The Minipax :  here we decide which, far-from-home war, we want to begin.";
                break;
            case 12:
                txt.text = "The MiniPlenty :  here we decide how much people should reduce its food, ";
                break;
            case 13:
                txt.text = "don't worry we have some agreement with Politecnico and you will not starve,";
                break;
            case 14:
                txt.text = "you could die for many other reasons here.";
                break;
            case 15:
                txt.text = "The ministery of love, you don't want to know what in which way we love people.";
                break;
            case 16:
                txt.text = "We love those people by respecting Geneva Agreements.";
                break;
            case 17:
                txt.text = "We believe four principles ";
                break;
            case 18:
                txt.text = "WAR IS PEACE";
                break;
            case 19:
                txt.text = "FREEDOM IS SLAVERY,";
                break;
            case 20:
                txt.text = "IGNORANCE IS STRENGTH";
                break;
            case 21:
                txt.text = "and COMPUTER SECURITY IS EASY";
                break;
            case 22:
                txt.text = "This lead us to your role here at the PARTY. ";
                break;
            case 23:
                txt.text = "Our leader, can learn almost everything quickly,due to his Machine, sorry Human-learning techniques";
                break;
            case 24:
                txt.text = "If you become good we will train a model to replace you and send you back to your country.";
                break;
            case 25:
                txt.text = "If you don't we will send you back to your country, if you are luckly.";
                break;
            case 26:
                changeScene("custom/menu/menu");
                break;
            default:
                break;
        }
    }


    public void changeScene(string scene)
    {
        Application.LoadLevel(scene);

    }
}
