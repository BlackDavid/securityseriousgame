﻿using UnityEngine;
using System.Collections;

public class Bigmovement : MonoBehaviour {
    int counter = 1;
    Vector3 velocity;
    Transform tr;
    public UnityEngine.UI.Text debug;
	// Use this for initialization
	void Start () {
        tr = GetComponent<Transform>();
        velocity = new Vector3(0, 0);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        counter++;
        if (counter % 100 == 0)
        {
            if(velocity.y > 0) velocity = new Vector3(Random.Range(0,0), Random.Range(-65,-65));
            else velocity = new Vector3(Random.Range(0, 0), Random.Range(65, 65));

        }
        debug.text = "x =" + tr.position.x + " y=" + tr.position.y +"vx="+velocity.x+"vy"+velocity.y;
       // if (tr.position.x > 500 && velocity.x > 0) velocity.x *= -1;
       // if (tr.position.x < 300 && velocity.x < 0) velocity.x *= -1;
       // if (tr.position.y > 700 && velocity.y > 0) velocity.y *= -1;
       // if (tr.position.y < 400 && velocity.y < 0) velocity.y *= -1;
        Vector3 a = velocity * Time.deltaTime;
        tr.position += a ;
    }
}
