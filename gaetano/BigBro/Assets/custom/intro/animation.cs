﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class animation : MonoBehaviour {

    public GameObject bbro;
    public GameObject panel;
    public UnityEngine.UI.Text txt;
    float sprite_alpha;
    int counter;

	// Use this for initialization
	void Start () {
        sprite_alpha = 0f;
        bbro.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0f);
        panel.GetComponent<CanvasGroup>().alpha = 0;   
        txt.GetComponent<CanvasGroup>().alpha = 0;
        counter = 0;
    }
	
	// Update is called once per frame
	void Update () {
     counter++;
	 if(counter > 100  && counter < 300 )
        {
            GetComponent<CanvasGroup>().alpha += 0.1f;
        }
     if (counter > 300 && counter < 400)
        {
            GetComponent<CanvasGroup>().alpha -= 0.05f;
        }
     if(counter > 400 && counter < 500)
        {
            sprite_alpha += 0.1f;
            bbro.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, sprite_alpha);
            panel.GetComponent<CanvasGroup>().alpha += 0.1f;

        }


    }
}
