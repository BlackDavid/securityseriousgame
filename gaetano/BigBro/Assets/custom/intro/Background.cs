﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Background : MonoBehaviour {

    public float speed = 0.05f;
    
    RectTransform rt;
	// Use this for initialization
	void Start () {
      
    }
	
	// Update is called once per frame
	void Update () {

        Vector2 offset = new Vector2(Time.time * speed, 0); 
        GetComponent<Renderer>().material.mainTextureOffset = offset;
    
	}
}
