﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class MeManager : MonoBehaviour {


    public UnityEngine.UI.Text name;
    public UnityEngine.UI.Text count;
    public GameObject b;
    public GameObject gm;
    Panelsc p;
    public GameObject panel;
    public GameObject text;
    public GameObject desc;
    public GameObject icon;
    public GameObject color;
    public GameObject back;
    private string state;
    CanvasGroup gamcv;
    bool dissolve = false;
    public int UnitEvent { get; private set; }


    void Awake()
    {
        if (GameManager.instance == null) Instantiate(gm);

    }
    void Start()
    {
        p = new Panelsc(panel, color, panel, icon, desc);


  

        addBadge();
        try
        {
            //GameManager gmanager = gm.GetComponent<GameManager>();
            name.text = GameManager.instance.getName();
            count.text = GameManager.instance.getDayCount();
            
        }
        catch
        {
            name.text = "Errore";
        }


    }

    void addBadge()
    {
        
        var levels = GameManager.instance.loadLevels();
        bool a = true;
        foreach (var s in levels[0].stages)
        { a = a && s.StageMastery();
        }
        if (a) {
            b.SetActive(true);
            GameManager.instance.nbadges += 1;
            GameManager.instance.savePlayertInfo();
        }

    }


    // Update is called once per frame
    void Update()
    {

    }
}
