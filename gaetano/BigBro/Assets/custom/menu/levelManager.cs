﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class levelManager : MonoBehaviour {


    class Card
    {
       public int id;
       public string title;
       public string desc;
       public string path_img;
       

        public Card(string t, string d, string p, int id)
        {
            
            this.title = t;
            this.desc = d;
            this.path_img = p;
            this.id = id;

        }
  
        public bool validate()
        {
            //you should add the scene name too!!!
            return id != -1 && title != null && desc != null && path_img != null;
        }


    }

    public GameObject card;
    public GameObject[] cards;
    Card[] cardmodel;
    public UnityEngine.UI.Text  debug;
    public GameObject listview;
    public GameObject gm = null;


    void Awake()
    {
        if (GameManager.instance == null) Instantiate(gm);

    }


    // Use this for initialization
    void Start () {

        card.SetActive(false); //prototype hiding
        List<GameManager.Level> levels=GameManager.instance.loadLevels();


        foreach(GameManager.Level l in levels)
        {
            AddCard(new Card(l.title, l.description, l.icon, l.lid));

        } 
        

        }


void AddCard(Card c)
    {
        GameObject card2 = Instantiate(card);
        card2.name = c.title;
        UnityEngine.UI.Text title = card2.transform.FindChild("Title").GetComponent<UnityEngine.UI.Text>();
        title.text = c.title;
        UnityEngine.UI.Text des = card2.transform.FindChild("Description").GetComponent<UnityEngine.UI.Text>();
        des.text = c.desc;
        UnityEngine.UI.RawImage img = card2.transform.FindChild("RawImage").GetComponent<UnityEngine.UI.RawImage>();
        img.texture = (Texture2D)Resources.Load(c.path_img, typeof(Texture2D));
        card2.transform.parent = listview.transform;
        card2.transform.localScale = new Vector3(1, 1, 1);

        EventTrigger trigger = card2.transform.FindChild("Panel").transform.FindChild("Button").GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback = new EventTrigger.TriggerEvent();
        UnityEngine.Events.UnityAction<BaseEventData> call = new UnityEngine.Events.UnityAction<BaseEventData>((UnityEngine.EventSystems.BaseEventData baseEvent) => {
            GameManager.instance.changeScene("custom/select/level_sel",""+c.id,"");
        });
        entry.callback.AddListener(call);
        trigger.triggers.Add(entry);

        card2.SetActive(true);
    }

    public void changeScene(string scene)
    {

        SceneManager.LoadScene(scene);

    }


    // Update is called once per frame
    void Update () {
	
	}
}
