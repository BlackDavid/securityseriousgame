﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KBTManager : MonoBehaviour {

    public enum ApproachType {Baseline, Bounded};



    public class KnowledgeComponent
    {
        private int lid, sid;
        private int attempts;
        private double TransitionParameter;
        private double SlipParameter;
        private double GuessParameter;
        private double AlreadyKnownParameter;
        private double KnownParameter;
        private int PosTransition;
        private const int ENOUGH_TESTS = 100;
        private const double COGNITIVE_MASTERY = 0.95;
        private const int N_FIRSTEMPIRICALTEST = 3;
        private const int M_SECONDEMPIRICALTEST = 10;

        private ApproachType type;

        public KnowledgeComponent(double AlreadyKnownParameter, int attempts, ApproachType type)
        {
            this.type = type;
            this.attempts = attempts;
            this.AlreadyKnownParameter = AlreadyKnownParameter; // We need students data to estimate this one
            this.KnownParameter = AlreadyKnownParameter;
            this.TransitionParameter = 0.01;
            this.SlipParameter = 0.1;
            this.GuessParameter = 0.3;
            PosTransition = 10;
        }

        public void FitThisParametersUpdate(List<GameManager.KcRecord> list)
        {

            if (list.Count < ENOUGH_TESTS) return; //At least 1000 tests

            /* list.Sort(delegate (KcRecord c1, KcRecord c2)
             { return c1.timestamp.CompareTo(c2.timestamp); });
             */

            double before_Transition = this.TransitionParameter;
            double before_Guess = this.GuessParameter;
            double before_Slip = this.SlipParameter;
            int before_PosTransition = this.PosTransition;

            EstimateEmpiricalProbabilityPosition(list);
            UpdateTransitionParameter(this.PosTransition, list.Count);
            UpdateGuessParameter(list, this.PosTransition);
            UpdateSlipParameter(list, this.PosTransition);


            if (TestFirstEmpiricalModelDegeneration(N_FIRSTEMPIRICALTEST) ||
                TestSecondEmpiricalModelDegeneration(M_SECONDEMPIRICALTEST))
            {
                this.TransitionParameter = before_Transition;
                this.GuessParameter = before_Guess;
                this.SlipParameter = before_Slip;
                this.PosTransition = before_PosTransition;
            }

            return;

        }

        private void EstimateEmpiricalProbabilityPosition(List<GameManager.KcRecord> records)
        {
            double score = 0;
            double max_score = -1;
            int BestTransitionPos = -1;
            for (int TransitionPos = 0; TransitionPos < records.Count; TransitionPos++)
            {
                score = 0;
                for (int i = 0; i < records.Count; i++)
                {

                    if (i <= TransitionPos) //Unkwown state
                        score += (records[i].value ? 0 : 1);
                    else //Knwon state
                        score += (records[i].value ? 1 : 0);
                }

                if (score > max_score)
                {
                    max_score = score;
                    BestTransitionPos = TransitionPos;
                }
            }

            //edge case 1 : Not knwon yet 
            score = 0;
            for (int i = 0; i < records.Count; i++)
                score += (records[i].value ? 0 : 1);

            if (score > max_score)
            {
                max_score = score;
                BestTransitionPos = records.Count + 1;
            }

            //edge case 1 : Already Known
            score = 0;
            for (int i = 0; i < records.Count; i++)
                score += (records[i].value ? 1 : 0);

            if (score > max_score)
            {
                max_score = score;
                BestTransitionPos = -1;
            }


            this.PosTransition = BestTransitionPos;
        }

        private void UpdateTransitionParameter(int transition, int count)
        {
            //The Knowledge vector is something like 0000011111111
            //because since you change state from unknwown to knwown
            // you can never come back. For this reason I only store 
            //the transition position from 0->1 (unknwon to known)
            double num = 0, den = 0;
            for (int i = 1; i < count; i++)
            {
                num += (1 - ((i - 1) <= transition ? 0 : 1)) * i <= transition ? 0 : 1;
                den += 1 - ((i - 1) <= transition ? 0 : 1);

            }

            // if (type == ApproachType.Bounded) this.TransitionParameter = num / den > 0.1 ? 0.1 : num / den;
            this.TransitionParameter = num / den;
        }

        private void UpdateGuessParameter(List<GameManager.KcRecord> records, int transition)
        {

            double num = 0, den = 0;
            for (int i = 0; i < records.Count; i++)
            {
                num += (records[i].value ? 1 : 0) * (1 - (transition <= i ? 1 : 0)); // sum [Ci * (1-Ki)]
                den += (1 - (transition <= i ? 1 : 0)); // sum[1-Ki]
            }

            //if (type == ApproachType.Bounded) this.GuessParameter = num / den > 0.3 ? 0.3 : num / den;
            this.GuessParameter = num / den;
        }

        private void UpdateSlipParameter(List<GameManager.KcRecord> records, int transition)
        {
            double num = 0, den = 0;
            for (int i = 0; i < records.Count; i++)
            {
                num += (1 - (records[i].value ? 1 : 0)) * (transition <= i ? 1 : 0); //sum[(1-Ci)*Ki]
                den += transition <= i ? 1 : 0; //sum[Ki]
            }
            //if (type == ApproachType.Bounded) this.SlipParameter = num / den > 0.1 ? 0.1 : num / den;
            this.SlipParameter = num / den;
        }

        public int GetAttempts()
        {

            return this.attempts;
        }


        public int GetTransitionPosition()
        {
            return this.PosTransition;
        }

        public double GetTransitionParameter()
        {
            return this.TransitionParameter;
        }

        public double GetSlipParameter()
        {
            return this.SlipParameter;
        }

        public double GetGuessParameter()
        {
            return this.GuessParameter;
        }

        public void UpdateKnownParameter(bool LastPerformance)
        {
            this.attempts++; 

            double probabilityStudentknewBeforeAction;

            if (LastPerformance == true) //means correct
            {
                //probability the student knew it if his action is correct
                probabilityStudentknewBeforeAction = (KnownParameter * (1 - this.SlipParameter)) /
                    ((KnownParameter * (1 - this.SlipParameter)) + ((1 - KnownParameter) * this.GuessParameter));
            }
            else // means incorrect
            {
                //probability the student knew it if his action is wrong
                probabilityStudentknewBeforeAction = (KnownParameter * (this.SlipParameter)) /
                   ((KnownParameter * this.SlipParameter) + ((1 - KnownParameter) * (1 - this.GuessParameter)));

            }

            this.KnownParameter = probabilityStudentknewBeforeAction +
                (1 - probabilityStudentknewBeforeAction) * this.TransitionParameter;

        }

        private bool TestFirstEmpiricalModelDegeneration(int N)
        {
            //Carnegie Mellon University -  Empirical Degeneracy of KBT Model

            /*if a student’s first three actions in the tutor are all correct,
            but the model’s estimated probability that the student knows the skill is lower
            than before these three actions, we say that the model failed*/

            bool ModelIsDegenerated = false;
            double KnowledgebeforeThreeCorrectActions = this.KnownParameter;

            for (int i = 0; i < N; i++)
                UpdateKnownParameter(true);

            if (KnownParameter < KnowledgebeforeThreeCorrectActions)
                ModelIsDegenerated = true;

            this.KnownParameter = KnowledgebeforeThreeCorrectActions;

            return ModelIsDegenerated;

        }

        private bool TestSecondEmpiricalModelDegeneration(int M)
        {
            //Carnegie Mellon University -  Empirical Degeneracy of KBT Model

            /*If a student gets a skill correct ten times in a row without reaching skill
            mastery, we say that the model failed the second test of empirical degeneracy*/

            bool ModelIsDegenerated = false;
            double KnowledgebeforeTenCorrectActions = this.KnownParameter;

            for (int i = 0; i < M; i++)
                UpdateKnownParameter(true);

            if (KnownParameter < COGNITIVE_MASTERY)
                ModelIsDegenerated = true;

            this.KnownParameter = KnowledgebeforeTenCorrectActions;

            return ModelIsDegenerated;

        }

        public double GetKnownParameter()
        {
            return this.KnownParameter;
        }

        public bool StudentHasReachedMastery()
        {
            return this.KnownParameter >= COGNITIVE_MASTERY;
        }

        public double ProbabilityStudentWillGetNextPerformanceRight()
        {
            // P(Corr) = P(Ln) * (1 - P(s)) + P(^ Ln) * P(G)
            return (this.KnownParameter * (1 - this.SlipParameter)) +
                 (1 - this.KnownParameter) * (this.GuessParameter);

        }


    }


    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
