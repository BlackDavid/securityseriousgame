﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using System;

public class InsightManaager : MonoBehaviour {

    public GameObject gm;
    public GameObject listview;
    public GameObject card;
    public GameObject masteryitem;

    class CardInsight
    {
        public GameManager.Level level;

        public CardInsight(GameManager.Level l)
        {
            this.level = l;
        }
    }

    void Awake()
    {
        if (GameManager.instance == null) Instantiate(gm);
    }

    // Use this for initialization
    void Start()
    {

        card.SetActive(false);
        List<GameManager.Level> levels = GameManager.instance.loadLevels();

        

        foreach (GameManager.Level l in levels)
        {
            AddCard(new CardInsight(l));

        }


    }


    void AddCard(CardInsight c)
    {
        GameObject card2 = Instantiate(card);
        card2.name = c.level.title;
        UnityEngine.UI.Text title = card2.transform.FindChild("LevelName").GetComponent<UnityEngine.UI.Text>();
        UnityEngine.UI.Text scenetext = card2.transform.FindChild("ScenePanel").
            FindChild("SceneStatus").GetComponent<UnityEngine.UI.Text>();
        title.text = c.level.title;
        scenetext.text = "\n";
        UnityEngine.Transform masterypanel = card2.transform.FindChild("MasteryPanel");
        foreach ( GameManager.Stage s in c.level.stages)
        {
            scenetext.text += "SCENE ID = " + s.stage_number + " #QUESTIONS " + s.questions.Count 
                + " #ATTEMPTS " +s.getAttemptsNumber()+ " P(Known)= "+ String.Format("{0:0.000}", s.getKnownParameter())+"\n";
            GameObject masterycard = Instantiate(masteryitem);
            masterycard.SetActive(true);
            if (s.StageMastery()) masterycard.GetComponent<UnityEngine.UI.Image>().color = Color.green;
            else masterycard.GetComponent<UnityEngine.UI.Image>().color = Color.gray;
            masterycard.transform.parent = masterypanel;
        }
        
        /*UnityEngine.UI.Text des = card2.transform.FindChild("Description").GetComponent<UnityEngine.UI.Text>();
        des.text = c.desc;
        UnityEngine.UI.RawImage img = card2.transform.FindChild("RawImage").GetComponent<UnityEngine.UI.RawImage>();
        img.texture = (Texture2D)Resources.Load(c.path_img, typeof(Texture2D));
        */
        card2.transform.parent = listview.transform;
        card2.transform.localScale = new Vector3(1, 1, 1);
      
        card2.SetActive(true);
    }
}
