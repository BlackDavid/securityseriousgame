﻿        using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class tabcontroller : MonoBehaviour {

    // Use this for initialization
    public GameObject tab1;
    public GameObject tab2;
    public GameObject tab3;
    public GameObject tab1_l;
    public GameObject tab2_l;
    public GameObject tab3_l;


    void Start()
    {
        selectTab(1);

    }


    public void selectTab(int num)
    {
        switch (num)
        {
            case 1:
                tab1.GetComponent<CanvasGroup>().alpha = 1;
                tab2.GetComponent<CanvasGroup>().alpha = 0;
                tab3.GetComponent<CanvasGroup>().alpha = 0;

                tab1_l.GetComponent<CanvasGroup>().alpha = 1;
                tab2_l.GetComponent<CanvasGroup>().alpha = 0;                
                tab3_l.GetComponent<CanvasGroup>().alpha = 0;

                tab1_l.SetActive(true);
                tab2_l.SetActive(false);
                tab3_l.SetActive(false);

                break;

            case 2:
                tab1.GetComponent<CanvasGroup>().alpha = 0;
                tab2.GetComponent<CanvasGroup>().alpha = 1;
                tab3.GetComponent<CanvasGroup>().alpha = 0;

                tab1_l.GetComponent<CanvasGroup>().alpha = 0;
                tab2_l.GetComponent<CanvasGroup>().alpha = 1;
                tab3_l.GetComponent<CanvasGroup>().alpha = 0;

                tab1_l.SetActive(false);
                tab2_l.SetActive(true);
                tab3_l.SetActive(false);
                break;

            case 3:
                tab1.GetComponent<CanvasGroup>().alpha = 0;
                tab2.GetComponent<CanvasGroup>().alpha = 0;
                tab3.GetComponent<CanvasGroup>().alpha = 1;

                tab1_l.GetComponent<CanvasGroup>().alpha = 0;
                tab2_l.GetComponent<CanvasGroup>().alpha = 0;
                tab3_l.GetComponent<CanvasGroup>().alpha = 1;

                tab1_l.SetActive(false);
                tab2_l.SetActive(false);
                tab3_l.SetActive(true);

                break;
            default:
                break;                
        }


    }

    public void changeScene(string scene)
    {
        Application.LoadLevel(scene);

    }


}
