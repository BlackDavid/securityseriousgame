﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Threading;

public class QuestionManager : MonoBehaviour {

    class GamePlayBlocksModel
    {
        public bool setSolution = false;
        public int lastinsertposition = -1;
        private const int MaximumSolutionLength = 10;
        private Mutex m = new Mutex();
        private String[] answer = new String[MaximumSolutionLength];
        private int inserted = 0;
        private String[] correct = new String[MaximumSolutionLength];
        Stack<UnityEngine.Transform> lastinserted = new Stack<Transform>();

        public UnityEngine.Transform getLastinserted()
        {
            lock (this.m)
            {
                return this.lastinserted.Pop();
            }
        }

        public void setLastinserted(UnityEngine.Transform a)
        {
            lock (this.m)
            {
                this.lastinserted.Push(a);
            }
        }

        public int decInserted()
        {
            lock(this.m)
            {
                inserted--;
                return inserted;
            }
        }

        public int augInserted(int x)
        {
            lock (this.m)
            {
                inserted+=x;
                return inserted;
            }
        }

        public void insertAt(bool correctArray, int index, String s)
        {
            lock(m)
            {
                String[] vec = this.answer;
                if (correctArray) vec = this.correct;
                vec[index] = s;
                if (correctArray) this.correct = vec;
                else this.answer = vec;

            }

        }

        public bool checkSolution()
        {
            lock(m)
            {
                foreach (String s in correct) Debug.Log(s);
                for (int i = 0; i < this.answer.Length; i++)
                {
                    Debug.Log("check" + this.answer[i] + "--" + this.correct[i]);
                    if (this.answer[i] == null || this.correct[i] == null) continue;

                    bool found = false;
                    if (this.setSolution)
                        foreach (String s in correct)
                            if (this.answer[i] == s)
                                found = true;
                    if (found) continue;

                    if (!this.answer[i].Equals(correct[i]))
                        return false;
                }
                return true; 

            }
        }

    }
    
    public GameObject gm;
    public GameObject [] gameplay;
    private int questions_number;
    public GameObject progress;
    GameManager.Question q = new GameManager.Question();
    Stack<GameManager.Question> questions;
    GamePlayBlocksModel gameplayblock;

    Panelsc p;
    public GameObject panel;
    public GameObject text;
    public GameObject desc;
    public GameObject icon;
    public GameObject color;

    public GameObject hintbutton;
    public GameObject closebutton;
    string state = "";
    
    CanvasGroup gamcv;
    bool dissolve = false;
    public int UnitEvent { get; private set; }

    void Awake()
    {
        if (GameManager.instance == null) Instantiate(gm);
    }
   
    void Start()
    {
        p = new Panelsc(text, color, panel,icon,desc);
        
        int gameplaynum = transform.Find("scroll").transform.childCount;
        gameplay = new GameObject[gameplaynum];
        for (int i = 0; i < gameplaynum ; i++)
            gameplay[i] = transform.Find("scroll").transform.GetChild(i).gameObject;
        gamcv = gameplay[0].transform.parent.GetComponent<CanvasGroup>();
        EventTrigger trigger = panel.GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback = new EventTrigger.TriggerEvent();
        UnityEngine.Events.UnityAction<BaseEventData> call = new UnityEngine.Events.UnityAction<BaseEventData>((UnityEngine.EventSystems.BaseEventData baseEvent) => {          
            while (!p.completed) ;
            p.animate(1, "", "");
           if(!state.Equals("hint"))
                popQuestion();
            state = "";
            gamcv.interactable = true;
        });
        entry.callback.AddListener(call);
        trigger.triggers.Add(entry);


        EventTrigger trigger2 = hintbutton.GetComponent<EventTrigger>();
        EventTrigger.Entry entry2 = new EventTrigger.Entry();
        entry2.eventID = EventTriggerType.PointerClick;
        entry2.callback = new EventTrigger.TriggerEvent();
        UnityEngine.Events.UnityAction<BaseEventData> call2 = new UnityEngine.Events.UnityAction<BaseEventData>((UnityEngine.EventSystems.BaseEventData baseEvent) => {
            p.animate(2, "hint",q.tip);
            state = "hint";
            gamcv.interactable = false;
        });
        entry2.callback.AddListener(call2);
        trigger2.triggers.Add(entry2);


        EventTrigger trigger3 = closebutton.GetComponent<EventTrigger>();
        EventTrigger.Entry entry3 = new EventTrigger.Entry();
        entry3.eventID = EventTriggerType.PointerClick;
        entry3.callback = new EventTrigger.TriggerEvent();
        UnityEngine.Events.UnityAction<BaseEventData> call3 = new UnityEngine.Events.UnityAction<BaseEventData>((UnityEngine.EventSystems.BaseEventData baseEvent) => {
            GameManager.instance.savePlayertInfo();
            SceneManager.LoadScene("custom/menu/menu");
        });
        entry3.callback.AddListener(call3);
        trigger3.triggers.Add(entry3);


        int lid = Int32.Parse(GameManager.instance.getLevelid());
        int sid = Int32.Parse(GameManager.instance.getSceneid());
        questions = GameManager.instance.loadQuestions(lid,sid);
        questions_number = questions.Count;
        foreach (GameObject gp in gameplay) gp.SetActive(false);
        popQuestion();


    }

    public void popQuestion()
    {
        
        if (questions.Count != 0)
        {
            
            q = questions.Pop();
            //Debug.Log("gameplay" + q.gameplay + "-"+q.qid);
            setGameplay(q.gameplay);
            switch (q.gameplay)
            {
                case 1:
                    initializeGameplayQuiz();
                    break;
                case 2:
                    initializeGameplayWriteAnswer();
                    break;
                case 3:
                    initializeGameplayBlocks();
                    break;
            }

        }
        else
        {
            //Attention if something in between happens
            GameManager.instance.savePlayertInfo();
            SceneManager.LoadScene("custom/menu/menu");
        }
    }

    private void checkAnser(GameManager.Question q, String answer)
    {

        int lid = Int32.Parse(GameManager.instance.getLevelid());
        int sid = Int32.Parse(GameManager.instance.getSceneid());
        GameManager.KcRecord k = new GameManager.KcRecord();
        switch (q.gameplay)
        {
            case 1: 
                 k = manageGameplayQuiz(q, answer);                
                break;
            case 2:
                k = manageGameplayWriteAnswer(q, answer);
                break;
            case 3:
                k = manageGameplayBlocks(q, answer);
                break;
            default:
                return;
        }
         updateProgress();
         GameManager.instance.updateKBT(k.value, lid, sid);
            
    }

    void initializeGameplayQuiz()
    {
        List<GameManager.Answer> answers = new List<GameManager.Answer>(q.answer);
        GameManager.Shuffle<GameManager.Answer>(answers);
        UnityEngine.Transform[] buttons = new UnityEngine.Transform[4];
        gameplay[0].transform.Find("Panel").transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = q.text;
        buttons[0] = gameplay[0].transform.FindChild("ListRow").transform.FindChild("Row1").
           transform.FindChild("Button1");
        buttons[1] = gameplay[0].transform.FindChild("ListRow").transform.FindChild("Row1").
           transform.FindChild("Button2");
        buttons[2] = gameplay[0].transform.FindChild("ListRow").transform.FindChild("Row2").
           transform.FindChild("Button3");
        buttons[3] = gameplay[0].transform.FindChild("ListRow").transform.FindChild("Row2").
           transform.FindChild("Button4");

        int i = 0;
        foreach (UnityEngine.Transform button in buttons)
        {
            button.FindChild("Text").GetComponent<UnityEngine.UI.Text>().text = answers[i].text;
            i++;

            UnityEngine.UI.Button b = button.GetComponent<UnityEngine.UI.Button>();
            b.onClick.RemoveAllListeners();
            b.onClick.AddListener(() =>
            {

                gamcv.interactable = false;
                checkAnser(q, b.gameObject.transform.FindChild("Text").GetComponent<UnityEngine.UI.Text>().text);

            });

        }
    }

    void initializeGameplayBlocks()
    {      
        this.gameplayblock = new GamePlayBlocksModel();
        List<GameManager.Answer> answers = new List<GameManager.Answer>(q.answer);
        GameManager.Shuffle<GameManager.Answer>(answers);
        UnityEngine.Transform[] buttons = new UnityEngine.Transform[6];
        UnityEngine.Transform[] solutions = new UnityEngine.Transform[4];

        gameplay[2].transform.Find("Panel").transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = q.text;
        int solution_count = 0;

        for (int c = 0; c < answers.Count; c++)
        {
            //Let's load the answers in the model gameplayblock
            if (answers[c].value > 0) {
                if (gameplayblock.lastinsertposition == answers[c].value - 1 || gameplayblock.setSolution)
                {
                    gameplayblock.setSolution = true;
                    gameplayblock.lastinsertposition += 1;
                    gameplayblock.insertAt(true, gameplayblock.lastinsertposition, answers[c].text);

                }
                else
                {
                    gameplayblock.insertAt(true, answers[c].value - 1, answers[c].text);
                    gameplayblock.lastinsertposition = answers[c].value - 1;
                }
            solution_count++;
         }

            // Let's load the texts of answers in the two rows
            if (c < (answers.Count / 2))
                buttons[c] = gameplay[2].transform.FindChild("ListRow").transform.FindChild("Row1").
                    transform.GetChild(c);
            else

                buttons[c] = gameplay[2].transform.FindChild("ListRow").transform.FindChild("Row2").
                    transform.GetChild(c - (answers.Count / 2));
        }



        // resize the solutions 
        for (int c = 0; c < 4; c++)
        {
            solutions[c] = gameplay[2].transform.FindChild("Solution").
                     transform.FindChild("Row").transform.GetChild(c);
            solutions[c].transform.localScale = new Vector3(0, 0, 0);
        }

        int i = 0;
        foreach (UnityEngine.Transform button in buttons)
        {
            button.transform.localScale = new Vector3(1, 1, 1);
            button.FindChild("Text").GetComponent<UnityEngine.UI.Text>().text = answers[i].text;
            i++;
            UnityEngine.UI.Button b = button.GetComponent<UnityEngine.UI.Button>();
            b.onClick.RemoveAllListeners();
            b.onClick.AddListener(() =>
            {
                String t = b.gameObject.transform.FindChild("Text").GetComponent<UnityEngine.UI.Text>().text;
                solutions[gameplayblock.augInserted(0)].transform.localScale = new Vector3(1, 1, 1);
                solutions[gameplayblock.augInserted(0)].FindChild("Text").GetComponent<UnityEngine.UI.Text>().text = t;
                gameplayblock.setLastinserted(button);
                gameplayblock.insertAt(false, gameplayblock.augInserted(0), t);
                button.transform.localScale = new Vector3(0, 0, 0);
                if (gameplayblock.augInserted(1) >= solution_count)
                {
                    gamcv.interactable = false;
                    Debug.Log("qui");
                    checkAnser(q, t);
                }
            });
        }

        foreach (UnityEngine.Transform sol_button in solutions)
        {

            UnityEngine.UI.Button b = sol_button.GetComponent<UnityEngine.UI.Button>();
            b.onClick.RemoveAllListeners();
            b.onClick.AddListener(() =>
            {

                UnityEngine.Transform lastbutton = this.gameplayblock.getLastinserted();
                String lastbuttontext = lastbutton.transform.FindChild("Text").
                    GetComponent<UnityEngine.UI.Text>().text;
                //undo one choise putting the button from the solutions among the options
                if (lastbuttontext.Equals(b.transform.FindChild("Text").
                    GetComponent<UnityEngine.UI.Text>().text))
                {
                    int pos = this.gameplayblock.decInserted();
                    sol_button.gameObject.transform.localScale = new Vector3(0, 0, 0);
                    lastbutton.transform.localScale = new Vector3(1, 1, 1);
                }
                else this.gameplayblock.setLastinserted(lastbutton);

            });
        }
    }

    void initializeGameplayWriteAnswer()
    {
            List<GameManager.Answer> answers = new List<GameManager.Answer>(q.answer);
            UnityEngine.Transform checkbutton;
            UnityEngine.Transform textAnswer;
            gameplay[1].transform.Find("Panel").transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = q.text;
            textAnswer = gameplay[1].transform.FindChild("Text Input").transform.FindChild("InputField").
                transform.FindChild("DisplayText");
            textAnswer.GetComponent<UnityEngine.UI.Text>().text = "";
            checkbutton = gameplay[1].transform.FindChild("Text Input").transform.FindChild("check");
            UnityEngine.UI.Button b = checkbutton.GetComponent<UnityEngine.UI.Button>();
            b.onClick.RemoveAllListeners();
            b.onClick.AddListener(() =>
            {
                gamcv.interactable = false;
                checkAnser(q, textAnswer.GetComponent<UnityEngine.UI.Text>().text);
            });

     }
    
    GameManager.KcRecord manageGameplayQuiz(GameManager.Question q, String answer)
    {
        int value = 0;
        GameManager.KcRecord kc = new GameManager.KcRecord();
        kc.timestamp = DateTime.Now.ToString();
        foreach (GameManager.Answer a in q.answer)
            if (a.text.Equals(answer))
            {
                value = a.value;
            };
        if (value <= 0)
        {   //High efficient push_back
            List<GameManager.Question> temp = new List<GameManager.Question>(questions);
            temp.Add(q);
            temp.Reverse();
            questions = new Stack<GameManager.Question>(temp);
            p.animate(2, "wrong", q.tip);
            kc.value = false;
        }
        else
        {
            p.animate(2, "correct", q.tip);
            kc.value = true;
        }
        //GameManager.instance.insertAnswer(q.qid, kc);
        //GameManager.instance.savePlayertInfo();
        return kc;
        //popQuestion(); 
    }

    GameManager.KcRecord manageGameplayWriteAnswer(GameManager.Question q, String answer)
    { 

        int value = 0;
        GameManager.KcRecord kc = new GameManager.KcRecord();
        kc.timestamp = DateTime.Now.ToString();
        string correctanswers = "Correct answers:";
        foreach (GameManager.Answer a in q.answer)
        {
            if (a.value > 0) correctanswers += a.text + " ";
            if (a.text.ToLower().Equals(answer.ToLower()))
            {
                value = a.value;
            };
        }
        if (value <= 0)
        {   //High efficient push_back
            List<GameManager.Question> temp = new List<GameManager.Question>(questions);
            temp.Add(q);
            temp.Reverse();
            questions = new Stack<GameManager.Question>(temp);
            p.animate(2, "wrong", correctanswers);
            kc.value = false;
        }
        else
        {
            p.animate(2, "correct", q.tip);
            kc.value = true;
        }        
        return kc;
    }

    private GameManager.KcRecord manageGameplayBlocks(GameManager.Question q, string answer)
    {
        
        GameManager.KcRecord kc = new GameManager.KcRecord();
        kc.timestamp = DateTime.Now.ToString();
        if (!gameplayblock.checkSolution())
        {
            List<GameManager.Question> temp = new List<GameManager.Question>(questions);
            temp.Add(q);
            temp.Reverse();
            questions = new Stack<GameManager.Question>(temp);
            p.animate(2, "wrong", q.tip);
            kc.value = false;
        }
        else
        {
            p.animate(2, "correct", q.tip);
            kc.value = true;
        }
        return kc;
    }
    void updateProgress()
    {
        float fraction = 1- (float)questions.Count/questions_number;
        //Debug.Log("fracition"+fraction);
        progress.GetComponent<UnityEngine.UI.Image>().fillAmount = fraction;
        

    }

    private void setGameplay(int gam)
    {
        try
        {
            dissolve = true;          
            foreach (GameObject gp in gameplay) gp.SetActive(false);
            (gameplay[gam-1]).SetActive(true);
        }
        catch
        {
            Debug.Log("Outbound");
        }
    }

    void FixedUpdate()
    {
        p.UpdateAnimation();
        if (dissolve && gamcv.alpha > 0f) gamcv.alpha -= 0.05f;
        if (dissolve && gamcv.alpha <= 0f)
        {
            gamcv.alpha = 1f;
            dissolve = false;
        }
    }

    void Update () {
        
    }

}
