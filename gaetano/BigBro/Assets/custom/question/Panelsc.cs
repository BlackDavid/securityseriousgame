﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Panelsc { 

    public bool completed = true;
    public GameObject color;
    public GameObject text;
    public GameObject descr;
    public GameObject icon;
    public GameObject panel;
    int animation_num = 0;
    public RectTransform tr;
    public string state;
    string desc;
    float counter;



    public Panelsc(GameObject text, GameObject color, GameObject panel, GameObject icon,
        GameObject descr)
    {
        this.text = text;
        this.color = color;
        this.panel = panel;
        this.icon = icon;
        this.descr = descr;
        tr = panel.GetComponent<RectTransform>();
      
    }

    // Update is called once per frame
    public void UpdateAnimation()
    {
        //Debug.Log("la fixed è chiamata");
        switch (animation_num)
        {
            case 1:
                HidePanel();
                break;
            case 2:
                ShowPanel();
                break;
            default:
                break;

        }

    }


    public void HidePanel()
    {

        if (tr.anchoredPosition.y > -150)
        {
            Vector2 delta = new Vector2(0, -13);
            tr.anchoredPosition += delta;
        }
        else  completed = true;
    }
    

    public void animate(int anim, string state, string description)
    {
       // Debug.Log("la animate è chiamata");
        if (completed == false) return;
        this.state = state;
        this.desc = description;
        completed = false;
       // Debug.Log("la animate è chiamata2");
        animation_num = anim;
    }



    public void ShowPanel()
    {
        UnityEngine.UI.RawImage img = icon.GetComponent<UnityEngine.UI.RawImage>();
       
        switch (state)
        {
            case "correct":
                text.GetComponent<UnityEngine.UI.Text>().text = "CORRECT";
                color.GetComponent<UnityEngine.UI.Image>().color = new Color(0.29f,0.45f,0.25f);
                img.texture = (Texture2D)Resources.Load("bro1", typeof(Texture2D));
                descr.GetComponent<UnityEngine.UI.Text>().text = desc;
                //descr.SetActive(false);
                break;
            case "wrong":
                text.GetComponent<UnityEngine.UI.Text>().text = "WRONG";
                color.GetComponent<UnityEngine.UI.Image>().color = new Color(0.40f, 0.2265f, 0.2937f);
                img.texture = (Texture2D)Resources.Load("bro1angr", typeof(Texture2D));
                descr.GetComponent<UnityEngine.UI.Text>().text = desc;
                //descr.SetActive(false);
                break;
            case "hint":
                descr.SetActive(true);
                text.GetComponent<UnityEngine.UI.Text>().text = "HINT";
                img.texture = (Texture2D)Resources.Load("bro1", typeof(Texture2D));
                color.GetComponent<UnityEngine.UI.Image>().color = new Color(0.55f, 0.53f, 0.31f);
                descr.GetComponent<UnityEngine.UI.Text>().text = desc;
                break;
        }

        if (tr.anchoredPosition.y < 150)
        {
            Vector2 delta = new Vector2(0, +13);
            tr.anchoredPosition += delta;
        }
        else completed = true;
    }


}
