﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class stageManager : MonoBehaviour {


  

    public GameObject card;
    public GameObject listview;
    public GameObject gm;


    void Awake()
    {
        if (GameManager.instance == null) Instantiate(gm);

    }


    // Use this for initialization
    void Start () {
        card.SetActive(false);
        /*Stage s = new Stage();
          s.count = 4;
          s.desc="prova prova";
          s.stage_number = 3;
          AddCard(s);
          AddCard(s);
          */

        List<GameManager.Stage> stages;
        stages = GameManager.instance.loadStages();
        foreach (GameManager.Stage st in stages) AddCard(st);
        
       
        
               

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void AddCard(GameManager.Stage s)
    {
        GameObject card2 = Instantiate(card);
        card2.name = "stage"+s.stage_number;
        UnityEngine.UI.Text content = card2.transform.FindChild("content").GetComponent<UnityEngine.UI.Text>();
        content.text = s.desc;
        UnityEngine.UI.Text lesson = card2.transform.FindChild("lesson").GetComponent<UnityEngine.UI.Text>();

        EventTrigger trigger = card2.transform.FindChild("Button").GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback = new EventTrigger.TriggerEvent();
        UnityEngine.Events.UnityAction<BaseEventData> call = new UnityEngine.Events.UnityAction<BaseEventData>((UnityEngine.EventSystems.BaseEventData baseEvent) => {
            GameManager.instance.changeScene("custom/question/question", GameManager.instance.getLevelid(), s.stage_number.ToString());
        });
        entry.callback.AddListener(call);
        trigger.triggers.Add(entry);


        lesson.text = "LESSON "+s.stage_number+" OF "+s.count;
        card2.transform.parent = listview.transform;
        card2.transform.localScale = new Vector3(1, 1, 1);
        card2.SetActive(true);
    }
}
