﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Warn : MonoBehaviour {
    public UnityEngine.UI.Text txt;
    public Image icon;
    public Sprite seconda;
    public Sprite terza;
    public Sprite quarta;
    public Sprite quinta;
    int num = 0;
    CanvasGroup cg;
    public UnityEngine.UI.InputField inputf;
    CanvasGroup input;
    // Use this for initializationss
    public GameObject gm=null;

    void Awake()
    {
        if (GameManager.instance == null) Instantiate(gm);

    }


    void Start()
    {
        num = 0;
        cg = GetComponent<CanvasGroup>();
        input = inputf.GetComponent<CanvasGroup>();
        
    }
	// Update is called once per frame
	void Update () {
        if (cg.alpha < 1f) cg.alpha += 0.1f;
	}

   public void changeToText()
    {
        if (num == 0)
        {
            txt.text = "Allow the BIG BROTHER \nto access your microphone\nanywhere?";
            icon.sprite = seconda;
            cg.alpha = 0;
            
        }

        if (num == 1)
        {
            txt.text = "Allow the BIG BROTHER \nto copy and paste your\nfingerprint?";
            icon.sprite = terza;
            cg.alpha = 0;
        }

        if (num == 2)
        {
            txt.text = "Allow the BIG BROTHER \nto access your biometric data\nand read part of your DNA?";
            icon.sprite = quarta;
            cg.alpha = 0;
        }

        if (num == 3)
        {
            GameManager.instance.LoadPlayerInfo();
            if (GameManager.instance.player_name != "")
                changeScene("custom/intro/story");
            txt.text = "Hi, my name is Malebear the official toy of The Party. What is yours?";
            icon.sprite = quinta;
            input.alpha = 1;
         
        }

        if(num>3 && inputf.text.Length != 0){
            
            GameManager.instance.player_name = inputf.text;
            GameManager.instance.savePlayertInfo();
            changeScene("custom/intro/story");
        }

        num++;
    }

    void changeScene(string scene)
    {
        Application.LoadLevel(scene);
        
    }

}

