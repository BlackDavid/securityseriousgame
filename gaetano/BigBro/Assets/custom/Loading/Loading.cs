﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Loading : MonoBehaviour {

    public Image loadingbar;
    public float loadingtime;
    public UnityEngine.UI.Text fake;
    public UnityEngine.UI.Text fake2;
    public UnityEngine.UI.Text txt;
    public UnityEngine.UI.Button allw;
    public Image panel;
    CanvasGroup gc;
    CanvasGroup gc_allow;
    CanvasGroup namepanel;
    CanvasGroup inputn;
    public InputField input;



    // Use this for initialization
    void Start () {
        if (loadingbar == null) return;
        if (loadingbar != null) loadingbar.fillAmount = 0f;
        gc = panel.GetComponent<CanvasGroup>();
        gc_allow = allw.GetComponent<CanvasGroup>();
        gc_allow.alpha = 0f;
    }
	
	// Update is called once per frame
	void Update () {
        if (loadingbar == null) return;
        if(loadingbar.fillAmount <= 1)
        {
            loadingbar.fillAmount += 1.0f / loadingtime * Time.deltaTime;
        }
        while (true)
        {
            if (loadingbar.fillAmount < 0.1) { fake2.text = "|"; break; }
            if (loadingbar.fillAmount < 0.2) { fake2.text = "/"; break; }
            if (loadingbar.fillAmount < 0.3) { fake2.text = "-"; break; }
            if (loadingbar.fillAmount < 0.4) { fake2.text = "\\"; break; }
            if (loadingbar.fillAmount < 0.5) { fake2.text = "|"; break; }
            if (loadingbar.fillAmount < 0.6) { fake2.text = "/"; break; }
            if (loadingbar.fillAmount < 0.7) { fake2.text = "-"; break; }
            if (loadingbar.fillAmount < 0.8) { fake2.text = "\\"; break; }
            if (loadingbar.fillAmount < 0.9) { fake2.text = "|"; break; }
            if (loadingbar.fillAmount < 1.0) { fake2.text = "/"; break; }
            break;
        }
        if (loadingbar.fillAmount < 0.2 )
        {
            fake.text = "Loading Artificial Intelligence";
            return;
        }

        if (loadingbar.fillAmount < 0.3)
        {
            fake.text = "Loading Emotional Module";
            return;
        }

        if (loadingbar.fillAmount < 0.5)
        {
            fake.text = "Loading Tortures";
            return;
        }

        if (loadingbar.fillAmount < 0.7)
        {
            fake.text = "Loading Impossibile Questions aka Exams";
            return;
        }
        

        if (loadingbar.fillAmount < 0.9)
        {
            fake.text = "Loading : better you don't know what";
            return;
        }

        if (loadingbar.fillAmount < 0.99)
        {
            fake.text = "Almost finished";
            return;
        }

        if(loadingbar.fillAmount > 0.9)
        {
            if(gc.alpha < 1f) gc.alpha += 0.1f;
            if (gc_allow.alpha < 1f) gc_allow.alpha += 0.1f;
        }



    }
}
