<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1462779751511" ID="ID_1004274143" MODIFIED="1462788738461" TEXT="Security Serious Game">
<node CREATED="1462786808874" ID="ID_1909509251" MODIFIED="1462786836010" POSITION="right" TEXT="Serious Games : The State of the art">
<node CREATED="1462786844604" ID="ID_1670270881" MODIFIED="1462786858713" TEXT="Videogame market"/>
<node CREATED="1462791930934" ID="ID_1494951864" MODIFIED="1462791967298" TEXT="Industry simulations"/>
<node CREATED="1462786864212" ID="ID_1958224430" MODIFIED="1462786922926" TEXT="Serious games "/>
<node CREATED="1462787823641" ID="ID_343797742" MODIFIED="1462787870543" TEXT="What is a Serious game"/>
</node>
<node CREATED="1462786924122" ID="ID_1848581317" MODIFIED="1462786928912" POSITION="left" TEXT="Game design">
<node CREATED="1462786992103" ID="ID_1582932423" MODIFIED="1462787404694" TEXT="Prototyping"/>
<node CREATED="1462787405253" ID="ID_294573422" MODIFIED="1462787408075" TEXT="Testing"/>
<node CREATED="1462787006045" ID="ID_1226880526" MODIFIED="1462787544099" TEXT="Game design bricks">
<node CREATED="1462787545914" ID="ID_1893303707" MODIFIED="1462787549502" TEXT="Players"/>
<node CREATED="1462787551988" ID="ID_1888396140" MODIFIED="1462787557503" TEXT="Rules"/>
<node CREATED="1462787558250" ID="ID_1728727389" MODIFIED="1462787564868" TEXT="Resources"/>
<node CREATED="1462787566833" ID="ID_1064656239" MODIFIED="1462787612308" TEXT="Procedures"/>
</node>
</node>
<node CREATED="1462786930543" HGAP="54" ID="ID_1976865379" MODIFIED="1462791991424" POSITION="right" TEXT="Learning Aspects" VSHIFT="20">
<node CREATED="1462786971200" ID="ID_232175557" MODIFIED="1462786977842" TEXT="Training vs Entertainemtent"/>
<node CREATED="1462787949416" ID="ID_1195000556" MODIFIED="1462787962095" TEXT="Cognitive aspects"/>
<node CREATED="1462787962744" ID="ID_816467848" MODIFIED="1462788032977" TEXT="Learning Models">
<node CREATED="1462787996290" ID="ID_304175391" MODIFIED="1462787999067" TEXT="KBT"/>
<node CREATED="1462787999632" ID="ID_723798706" MODIFIED="1462788450622" TEXT="Big Data and Learning"/>
</node>
</node>
<node CREATED="1462787634169" ID="ID_1338372164" MODIFIED="1462787729721" POSITION="left" TEXT="Developing platforms">
<node CREATED="1462787733300" ID="ID_774397575" MODIFIED="1462787736309" TEXT="Unity"/>
<node CREATED="1462787739913" ID="ID_1999562690" MODIFIED="1462787753050" TEXT="Gdevelop"/>
<node CREATED="1462787754721" ID="ID_403849948" MODIFIED="1462787759904" TEXT="Pixi.js"/>
<node CREATED="1462787785984" ID="ID_381006833" MODIFIED="1462787791422" TEXT="Phaser"/>
<node CREATED="1462787763539" ID="ID_1253088346" MODIFIED="1462787774968" TEXT="Xamarin"/>
</node>
<node CREATED="1462788092373" HGAP="9" ID="ID_358618967" MODIFIED="1462788350587" POSITION="right" TEXT="Making a videogame: The Odyssey" VSHIFT="-6">
<node CREATED="1462788118861" HGAP="11" ID="ID_730758686" MODIFIED="1462788290961" TEXT="Davide&apos;s Journey" VSHIFT="38">
<node CREATED="1462789132538" ID="ID_738405342" MODIFIED="1462789137671" TEXT="RPG games">
<node CREATED="1462789139055" ID="ID_154603767" MODIFIED="1462789175872" TEXT="RPG Rewarding system "/>
<node CREATED="1462789224954" ID="ID_889441074" MODIFIED="1462789356992" TEXT="RPG balancing system"/>
</node>
<node CREATED="1462789279902" ID="ID_387976359" MODIFIED="1462789290006" TEXT="Puzzles games"/>
<node CREATED="1462789448515" ID="ID_1328360354" MODIFIED="1462789457681" TEXT="Incomplete information problem"/>
<node CREATED="1462789506699" ID="ID_1296372144" MODIFIED="1462790672676" TEXT="Chosen Gamplay">
<node CREATED="1462790297050" ID="ID_818775623" MODIFIED="1462790595726" TEXT="Puzzle">
<node CREATED="1462790745747" ID="ID_314559560" MODIFIED="1462790748672" TEXT="IDS"/>
<node CREATED="1462790750468" ID="ID_789307401" MODIFIED="1462790762849" TEXT="Cryptogtaphy"/>
<node CREATED="1462790767488" ID="ID_1087348818" MODIFIED="1462790781152" TEXT="Network Security"/>
<node CREATED="1462790793553" ID="ID_1333708393" MODIFIED="1462790805640" TEXT="Authentication Systems"/>
</node>
</node>
</node>
<node CREATED="1462788255419" HGAP="8" ID="ID_828887905" MODIFIED="1462790079885" TEXT="Gaetano&apos;s Journey" VSHIFT="37">
<node CREATED="1462789846680" ID="ID_1991352285" MODIFIED="1462790678745" TEXT="Gameplay">
<node CREATED="1462789856829" ID="ID_1884424628" MODIFIED="1462789866675" TEXT="Clash of Lans">
<node CREATED="1462789869495" ID="ID_675465631" MODIFIED="1462789873546" TEXT="Survival Game"/>
</node>
<node CREATED="1462789875772" ID="ID_1141606243" MODIFIED="1462789886693" TEXT="Server vs Zombie">
<node CREATED="1462789889435" ID="ID_1730355738" MODIFIED="1462789898869" TEXT="Rule Based Game"/>
</node>
<node CREATED="1462789902031" ID="ID_620423767" MODIFIED="1462789908027" TEXT="Packet Ninja">
<node CREATED="1462789909391" ID="ID_1331892703" MODIFIED="1462789936084" TEXT="Firewall game"/>
</node>
<node CREATED="1462789939171" ID="ID_1155699222" MODIFIED="1462789957989" TEXT="Orwell 1984">
<node CREATED="1462790030688" ID="ID_608348661" MODIFIED="1462790049028" TEXT="Using sci-fi tech to explain concepts"/>
</node>
<node CREATED="1462790060109" ID="ID_478988648" MODIFIED="1462790063642" TEXT="Big Bro">
<node CREATED="1462790064613" ID="ID_354172496" MODIFIED="1462790112060" TEXT="Space repetition quiz game"/>
</node>
</node>
<node CREATED="1462790081255" ID="ID_1806955099" MODIFIED="1462790085733" TEXT="Flow Theory"/>
<node CREATED="1462790202596" ID="ID_1387403221" MODIFIED="1462790221770" TEXT="Key aspects of successful videogame"/>
</node>
</node>
<node CREATED="1462788670368" HGAP="10" ID="ID_955550561" MODIFIED="1462788735874" POSITION="left" TEXT="Conclusions : The end of the Journey " VSHIFT="27"/>
<node CREATED="1462788742627" HGAP="5" ID="ID_313408729" MODIFIED="1462792002582" POSITION="right" TEXT="Abstract : Why so serious " VSHIFT="-87"/>
</node>
</map>
