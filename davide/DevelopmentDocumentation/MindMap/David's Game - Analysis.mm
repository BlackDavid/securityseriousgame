<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1462890725174" ID="ID_242600084" MODIFIED="1462894811039" TEXT="Serious Game">
<node CREATED="1462891125704" HGAP="35" ID="ID_1493252425" MODIFIED="1462894796788" POSITION="right" TEXT="RPG Game" VSHIFT="-121">
<node CREATED="1462891901363" ID="ID_1546368271" MODIFIED="1462918287522" TEXT="Missioni da portare a termine per avanzare nel gioco" VSHIFT="-41">
<icon BUILTIN="button_ok"/>
<node CREATED="1462891206968" ID="ID_1057687901" MODIFIED="1462893418259" TEXT="Rewarding: dopo ogni missione vengo ricompensato">
<icon BUILTIN="button_ok"/>
<node CREATED="1462891802835" ID="ID_1594748496" MODIFIED="1462893324684" TEXT="Punti Esperienza">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1462891817106" ID="ID_139411965" MODIFIED="1462893390796" TEXT="Oro">
<icon BUILTIN="help"/>
</node>
<node CREATED="1462891820103" ID="ID_575568042" MODIFIED="1462893410805" TEXT="Oggetti utili a continuare il gioco">
<icon BUILTIN="help"/>
</node>
<node CREATED="1462893397164" ID="ID_1063924671" MODIFIED="1462893407352" TEXT="Sblocco altre missioni">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1462891918638" ID="ID_1082183619" MODIFIED="1462893697348" TEXT="Decisioni basate su conflitti che influenzano il profilo del giocatore (Uccido un bambino per salvare il mondo?)">
<icon BUILTIN="help"/>
</node>
<node CREATED="1462918214229" ID="ID_1825620718" MODIFIED="1462918280162" TEXT="Incomplete Information Problems: &#xa;alcune azioni sono fatte senza sapere a &#xa;cosa porteranno (supermario che corre dietro al fungo)&#xa;"/>
</node>
<node CREATED="1462891228012" ID="ID_87539583" MODIFIED="1462893684144" TEXT="Bilanciamento difficolt&#xe0; di gioco/interesse giocatore &#xa;(ovvero come mantenere vivo l&apos;interesse nonostante la difficolt&#xe0; di gioco)">
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1462891305418" ID="ID_1130327779" MODIFIED="1462893675023" TEXT="Mondo di gioco stimolante&#xa;Pi&#xf9; e vasto e pi&#xf9; cose ci sono da fare pi&#xf9; l&apos;utente ne &#xe8; immerso ed ha voglia di continuare.&#xa;">
<icon BUILTIN="messagebox_warning"/>
</node>
<node CREATED="1462891312532" ID="ID_775019665" MODIFIED="1462893429306" TEXT="Narrativa">
<icon BUILTIN="button_ok"/>
<node CREATED="1462892024258" ID="ID_1633295710" MODIFIED="1462893380639" TEXT="Interactive storytelling">
<icon BUILTIN="help"/>
</node>
<node CREATED="1462892036458" ID="ID_1895610820" MODIFIED="1462893438057" TEXT="Linear Storytelling">
<icon BUILTIN="help"/>
</node>
</node>
<node CREATED="1462891368276" ID="ID_1185207378" MODIFIED="1462893345498" TEXT="Mental challenge">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node CREATED="1462893051952" HGAP="40" ID="ID_1316139234" MODIFIED="1462894800585" POSITION="left" TEXT="Puzzle Game" VSHIFT="-207">
<node CREATED="1462893081822" ID="ID_1505090911" MODIFIED="1462893662413" TEXT="Stimolanti e facilmente adattabili a vari ambiti &#xa;(storia, geografia, matematica)">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1462893174163" ID="ID_906215981" MODIFIED="1462893707150" TEXT="Una volta trovata la soluzione &#xe8; ripetitivo">
<font NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_cancel"/>
</node>
<node CREATED="1462893611152" ID="ID_75895070" MODIFIED="1462893650631" TEXT="Si sposa bene con una gameplay di tipo grafico">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1462893721489" HGAP="107" ID="ID_1725059961" MODIFIED="1462894804960" POSITION="right" TEXT="Apprendimento" VSHIFT="-13">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1462893860899" ID="ID_563692994" MODIFIED="1462893881837" TEXT="Memorizzazione">
<node CREATED="1462893906768" ID="ID_398499012" MODIFIED="1462893915363" TEXT="Per ripetizione">
<node CREATED="1462893900171" ID="ID_1041718447" MODIFIED="1462894000241" TEXT="Visiva(Scrivo/Rileggo)"/>
<node CREATED="1462893903671" ID="ID_374789356" MODIFIED="1462894014292" TEXT="Uditiva(Leggo e ripeto voce alta)"/>
</node>
<node CREATED="1462893916107" ID="ID_595392769" MODIFIED="1462893923889" TEXT="Per associazione di idee/concetti"/>
<node CREATED="1462893924810" ID="ID_934581280" MODIFIED="1462893948390" TEXT="Tramite Pratica"/>
</node>
<node CREATED="1462893882740" ID="ID_1626814343" MODIFIED="1462893885568" TEXT="Comprensione">
<node CREATED="1462894327823" ID="ID_1233954233" MODIFIED="1462894373830" TEXT="Esercitazione in ambiente controllato"/>
<node CREATED="1462894334217" ID="ID_1484665548" MODIFIED="1462894366484" TEXT="Applicazione in contesti diversi"/>
</node>
<node COLOR="#338800" CREATED="1462894590534" HGAP="12" ID="ID_1456533966" MODIFIED="1462894750306" TEXT="Risultati Migliori si ottengono &#xa;unendo pi&#xf9; stimoli percettivi e fondendoli con le emozioni" VSHIFT="53">
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1462899322973" HGAP="249" ID="ID_709440113" LINK="David&apos;s%20Game-Exploded.mm" MODIFIED="1462918189078" POSITION="left" TEXT="David&apos;s Game" VSHIFT="101"/>
</node>
</map>
