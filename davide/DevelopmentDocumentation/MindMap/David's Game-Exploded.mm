<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1462894817929" HGAP="214" ID="ID_749709224" LINK="DavidsGame.mm" MODIFIED="1462899322911" TEXT="David&apos;s Game" VSHIFT="249">
<icon BUILTIN="idea"/>
<node CREATED="1462895808258" HGAP="61" ID="ID_1783573014" MODIFIED="1462902083558" POSITION="left" VSHIFT="-62">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Puzzle Game a blocchi visuali
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="idea"/>
<node CREATED="1462896787964" HGAP="22" ID="ID_866652411" MODIFIED="1462901187632" TEXT="All&apos;inizio del gioco saranno disponibili alcuni blocchi base" VSHIFT="-66"/>
<node CREATED="1462896378438" HGAP="23" ID="ID_112129205" MODIFIED="1462901189773" VSHIFT="-28">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ogni Blocco rappresenta una funzione di sicurezza
    </p>
    <p>
      Es DES, AES
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1462896280043" HGAP="25" ID="ID_211286218" MODIFIED="1462901193820" VSHIFT="-21">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ogni blocco ha 1 o pi&#249; input e
    </p>
    <p>
      produce 1&#160;o pi&#249; output
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1462896306481" HGAP="56" ID="ID_1546540693" MODIFIED="1462901975113" VSHIFT="2">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ogni blocco pu&#242; essere
    </p>
    <p>
      combinato con altri per
    </p>
    <p>
      realizzare nuove &quot;funzionalit&#224;&quot;
    </p>
    <p>
      Ad esempio 3 blocchi Des in serie
    </p>
    <p>
      realizzano il triplo des
    </p>
  </body>
</html></richcontent>
<node CREATED="1462900505401" ID="ID_929439774" MODIFIED="1462902077626" TEXT="I blocchi possono essere assemblati in libert&#xe0;, lasciando libero il giocatore di sbagliare, &#xa;si otterrano nuove funzionalit&#xe0; solo quando i blocchi assemblati corrisponderanno a &#xa;configurazioni corrette ">
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1462900691315" HGAP="14" ID="ID_1541494212" MODIFIED="1462902091401" TEXT="Alcuni puzzle dovranno essere risolti in un tempo prestabilito &#xa;(stile cowntdown) per mantenere viva l&apos;attenzione del giocatore.&#xa;" VSHIFT="26">
<node CREATED="1462900775760" ID="ID_819360919" MODIFIED="1462900948336">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      I puzzle temporizzati sono quelli di fine livello,
    </p>
    <p>
      questo permette di fissare l'apprendimento aggiungendo
    </p>
    <p>
      un piccolo livello di stress.
    </p>
    <p>
      Si stimola cos&#236; l'uso di blocchi
    </p>
    <p>
      creati dall'utente per risparmiare tempo.
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node CREATED="1462895827282" HGAP="25" ID="ID_1367288653" MODIFIED="1462901299290" POSITION="left" VSHIFT="43">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Supportato da una storia stile Mr. Robot/Matrix
    </p>
    <p>
      Anche ispirata ad esempi raccontati dal prof a lezione
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="idea"/>
<node CREATED="1462897281167" ID="ID_482566059" MODIFIED="1462897667995" VSHIFT="-12">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Idea base di Storia: Il giocatore deve infiltrarsi in
    </p>
    <p>
      un'azienda di sicurezza per danneggiarli
    </p>
  </body>
</html></richcontent>
<node CREATED="1462901306000" HGAP="33" ID="ID_153403666" MODIFIED="1462901440998" TEXT="Per farsi assumere lo studente deve superare una selezione&#xa;basata su test a risposta multipla. che copre gli argomenti del primo&#xa;blocco di slide del professore." VSHIFT="6"/>
</node>
<node CREATED="1462902188272" HGAP="29" ID="ID_1449111858" MODIFIED="1462902364302" TEXT="Le missioni da affrontare verranno assegnate dal capo aziendale &#xa;oppure dall&apos;associazione hacker sovversiva" VSHIFT="70">
<node CREATED="1462897193376" HGAP="17" ID="ID_1595860680" MODIFIED="1462897662698" TEXT="Il giocatore interagisce con un Personaggio Non Giocante&#xa; che lo contatta via telefono&#xa;(simulazione di chiamata) e gli da istruzioni sulla missione" VSHIFT="31">
<node CREATED="1462902136865" ID="ID_1850328244" MODIFIED="1462902176516" TEXT="Il giocatore potr&#xe0; accettare o rifiutare gli incarichi affidatigli dal PNG."/>
</node>
</node>
<node CREATED="1462897131261" ID="ID_601947191" MODIFIED="1462902242866" TEXT="Audio che aiutino lo studente a risolvere i puzzle&#xa;( ipotesi di spezzoni delle audiolezioni per sottolineare alcuni concetti)" VSHIFT="131"/>
</node>
<node CREATED="1462896702526" HGAP="4" ID="ID_1148503483" MODIFIED="1462901477016" POSITION="left" TEXT="Ad ogni puzzle risolto si accumulano punti esperienza" VSHIFT="80">
<icon BUILTIN="idea"/>
<node CREATED="1462896734907" MODIFIED="1462897680178" TEXT="Accumulato un certo numero di punti esperienza nell&apos;uso di un blocco base, &#xa;sar&#xe0; possibile assemblare pi&#xf9; blocchi base e creare una &quot;funzionalit&#xe0; avanzata&quot;&#xa;" VSHIFT="-44"/>
<node CREATED="1462896952123" HGAP="22" ID="ID_1610269461" MODIFIED="1462901161525" TEXT="La quantit&#xe0; di punti esperienza accumulati per ogni &quot;livello completato&quot;&#xa;&#xe8; inversamente proporzionale al tempo impiegato a risolverlo" VSHIFT="-19"/>
<node CREATED="1462900997934" HGAP="19" ID="ID_783351096" MODIFIED="1462901163510" TEXT="I punti esperienza si differenziano in base alla tipologia di puzzle risolto, &#xa;si avranno punti esperienza per algoritmi simmetrici, algoritmi asimmetrici,&#xa;network security, Main In The Middle etc. etc." VSHIFT="12"/>
<node CREATED="1462901086863" HGAP="21" ID="ID_724723071" MODIFIED="1462901173569" TEXT="I punteggi finali di ogni livello potrebbero essere raccolti in una classifica &#xa;per permettere il confronto tra studenti" VSHIFT="39">
<icon BUILTIN="help"/>
</node>
</node>
</node>
</map>
