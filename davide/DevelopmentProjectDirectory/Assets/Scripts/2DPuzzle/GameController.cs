﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.IO;

public class GameController : MonoBehaviour {

    /// <summary>
    /// Main Menu Scene
    /// </summary>
    public Object MainMenuScene;

    //Singleton instance
    public static GameController instance;

    //All Levels Data
    private List<LevelData> allLevelsData = new List<LevelData>();

    /// <summary>
    /// Identify the current playing level index in allLevelsData list
    /// This index is used because the level ordering may differ from the numerical order of the level ID 
    /// and depends on the sorting order of the main menu.
    /// This index is also used to enable the next level when user solve the current level
    /// -1 for main menu
    /// </summary>
    private int currentLevelIndexInLevelsList = -1;
    

    void Awake()
    {
        //Implementing singleton game controller
        if (instance == null)
            instance = this;
        else if (instance != this)
            DestroyObject(gameObject);
        
        DontDestroyOnLoad(gameObject);
    }

    /// <summary>
    /// Method used to add a level to the list of all playable levels
    /// </summary>
    /// <param name="ld">Data of the new level</param>
    public void AddLevel(LevelData ld)
    {
        allLevelsData.Add(ld);
        //Enabling the first level of the list
        if (allLevelsData[0].levelEnabled == false)
        {
            allLevelsData[0].levelEnabled = true;
        }
    }

    /// <summary>
    /// use this method to load new level
    /// </summary>
    /// <param name="levelXmlFileName">Xml file name of the loading level</param>
    /// <param name="scene">scene name of the level</param>
    public void LoadLevel(LevelData currentLevelData, string levelSceneName)
    {
        //Saving the current level ID
        currentLevelIndexInLevelsList = allLevelsData.FindIndex(
          delegate (LevelData l) { return l.levelID == currentLevelData.levelID; }
          );

        Debug.Log("Current level index "+currentLevelIndexInLevelsList);
        //loading the scene;
        SceneManager.LoadScene(levelSceneName);
        
    }

    /// <summary>
    /// Return the current leve data object 
    /// used by the levelManager to retrive the information about the level
    /// </summary>
    /// <returns></returns>
    public LevelData GetCurrentLevelData()
    {
        return allLevelsData[currentLevelIndexInLevelsList];
    }

    /// <summary>
    /// Update the Level Data of the level just played
    /// </summary>
    public void updateLevelData(LevelData ld)
    {
        if (currentLevelIndexInLevelsList != -1)
        {
            allLevelsData[currentLevelIndexInLevelsList] = ld;
            allLevelsData[currentLevelIndexInLevelsList].UpdateLevelSavingsData();
            //If the level is passed enable the next level.
            if (allLevelsData[currentLevelIndexInLevelsList].levelPassed && currentLevelIndexInLevelsList + 1 < allLevelsData.Count)
            {
                allLevelsData[currentLevelIndexInLevelsList + 1].levelEnabled = true;
                allLevelsData[currentLevelIndexInLevelsList + 1].UpdateLevelSavingsData();
            }
        }
        

        

    }

    /// <summary>
    /// method called when the user want to come back to main menu
    /// </summary>
    public void LoadMainMenu()
    {

            currentLevelIndexInLevelsList = -1;
            SceneManager.LoadScene(MainMenuScene.name);

        
    }

    /// <summary>
    /// Method used for developing new levels, save the solutions to json
    /// </summary>
    /// <param name="proposedSolution"></param>
    
    public void SaveSolution( string proposedSolution)
    {
        
        //Json file name
        string savingSolutionFileName = "solution-" + allLevelsData[currentLevelIndexInLevelsList].levelID.ToString() + ".txt";
        //Saving json file path
        string savingSolutionFilePath = Path.Combine(Application.streamingAssetsPath, "solutions/"+savingSolutionFileName);
        //string used to convert levelData to json
        //string dataAsJson = string.Empty;
        //serialization of new level data
        //dataAsJson = JsonUtility.ToJson(proposedSolution);
        if (File.Exists(savingSolutionFilePath))
        {
            File.WriteAllText(savingSolutionFilePath, proposedSolution);
            Debug.Log("Saving complete");
        }
        else
        {
            Debug.Log("Warnign json data file doesn't exist, creating");
            File.CreateText(savingSolutionFilePath).Dispose();
            Debug.Log("Json file created");
            File.WriteAllText(savingSolutionFilePath, proposedSolution);
            Debug.Log("Saving complete");
        }
    }




}





// DEPRECATED
///// <summary>
///// Loading the levels data from json file
///// </summary>
//private void LoadGameDataFromJson()
//{
//    string dataFilePath = Path.Combine(Application.dataPath, gameDataFileName);
//    if (File.Exists(dataFilePath))
//    {
//        //attempt to load the file
//        //read all the text from the file path into a string of json
//        string dataAsJson = File.ReadAllText(dataFilePath);
//        //deserialization
//        GameData loadedData = JsonUtility.FromJson<GameData>(dataAsJson);
//        allLevelsData = loadedData.allLevelsData;
//    }else
//    {
//        //if file doesn't exists the levels data list has to be filled in with information from levels xml files
//        allLevelsData = new List<LevelData>();
//        CreateGameDataJsonFile();
//    }


//}

///// <summary>
//    /// Saving the game data to json file
//    /// </summary>
//private void SaveGameData()
//{
//    //Object used to that has to be serialized
//    GameData gameData = new GameData();
//    gameData.allLevelsData = allLevelsData;
//    string filePath = Path.Combine(Application.streamingAssetsPath, gameDataFileName);
//    if (!File.Exists(filePath))
//    {
//        Debug.Log("Warnign file doesn't exist, creating");
//        File.CreateText(filePath).Dispose();
//        Debug.Log("Json file created");
//    }
//    //serialization of the data
//    string dataAsJson = JsonUtility.ToJson(gameData);
//    File.WriteAllText(filePath, dataAsJson);

//    Debug.Log("Saving complete");
//}

///// <summary>
///// Populate the GameData structure starting from level xml files
///// </summary>
//private void CreateGameDataJsonFile()
//{
//    /* 1 for each file in the directory
//     * 2 read the information and copy in levelData object and add it to the list
//     * 3 save the new information in the json file  
//     */
//    string xmlFilesPath = Path.Combine(Application.dataPath, gameDataXMLDirectory);
//    string[] xmlFiles = Directory.GetFiles(xmlFilesPath,"Level*.xml");
//    foreach (string xmlFile in xmlFiles)
//    {
//         allLevelsData.Add(LoadXMLData(xmlFile));
//    }
//    Debug.Log("All levels data loaded from xml files");
//    //Enabling first level of the game
//    allLevelsData[0].levelEnabled = true;
//    SaveGameData();
//}


///// <summary>
///// Starting from xml file name search for corresponding json data file and load level data information
///// </summary>
///// <param name="xmlFilePath">File name of the XML description file</param>
//LevelSavingsData LoadLevelData(string xmlFileName)
//{
//    LevelSavingsData loadedData;
//    //obtain the json file name starting from xml filename
//    string jsonFileName = xmlFileName.Replace(".xml", ".json");
//    //Json file path
//    string jsonDataFilePath = Path.Combine(Application.dataPath, jsonFileName);
//    //string used to convert levelData to json
//    string dataAsJson = string.Empty;
//    //load level data from json File
//    if (File.Exists(jsonDataFilePath))
//    {
//        //attempt to load the file
//        //read all the text from the file path into a string of json
//        dataAsJson = File.ReadAllText(jsonDataFilePath);
//        //deserialization
//        loadedData = JsonUtility.FromJson<LevelSavingsData>(dataAsJson);
//    }
//    else
//    {
//        //if file doesn't exists the levels data object has to be filled in with information from levels xml files
//        //string xmlFilePath= Path.Combine(gameDataXMLDirectory, xmlFileName);
//        loadedData = LoadXMLData(xmlFileName);
//        //creating json File
//        Debug.Log("Warnign json data file doesn't exist, creating");
//        File.CreateText(jsonDataFilePath).Dispose();
//        Debug.Log("Json file created");
//        //serialization of new level data
//        dataAsJson = JsonUtility.ToJson(loadedData);
//        File.WriteAllText(jsonDataFilePath, dataAsJson);
//        Debug.Log("Saving complete");
//    }
//    //Append to levels data list
//    allLevelsData.Add(loadedData);
//    return loadedData;
//}

///// <summary>
///// reading Xml from Files and return a LevelData object
///// </summary>
//public LevelSavingsData LoadXMLData(string xmlFileName)
//{
//    string jsonFileName = xmlFileName.Replace(".xml", ".json");
//    string xmlFilePath = Path.Combine(gameDataXMLDirectory, xmlFileName);
//    // Xml variables to access the xml data
//    XmlDocument xmlDoc = new XmlDocument();
//    XmlNode xmlNode;
//    //Loading the level data from the xml file
//    xmlDoc.Load(xmlFilePath);
//    //Loading the level title
//    xmlNode = xmlDoc.SelectSingleNode("/level");
//    LevelSavingsData ld = new LevelSavingsData(int.Parse(xmlNode.Attributes["lid"].Value));
//    return ld;
//}





