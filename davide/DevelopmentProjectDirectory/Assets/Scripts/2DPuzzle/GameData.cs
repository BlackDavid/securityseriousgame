﻿using System.Collections.Generic;
/// <summary>
/// Storage class for all the level data to save in a json file
/// </summary>

[System.Serializable]
public class GameData {

    /// <summary>
    /// All the level data is collected here
    /// </summary>
    public List<LevelSavingsData> allLevelsData;
        
	
}
