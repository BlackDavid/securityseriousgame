﻿using UnityEngine;
using System.Collections;
using System;

public class Level00AdditionalLogic : MonoBehaviour, ILevelAdditionalLogic
{


    public bool CheckSolution(LevelData levelData, string proposedSolution)
    {
        /* The first thing to do is to adapt the user solution to the general solution 
         * normalizinfg the key value that is different for each new block in the play scene
         */
        string openString = "[Simmetric-Key]";
        string closeString = "[lenght]";
        ///Triple des has up to 3 key
        string[] keys = new string[3];

        //All the elaboration is made on a copy of the user proposed solution
        string modifiedUserSolution = proposedSolution;

        int openingIndex = 0;
        int closingIndex = 0;

        for (int i = 0; i < 3; i++)
        {
            //Search occurence of key value
            openingIndex = proposedSolution.IndexOf(openString, closingIndex ,StringComparison.OrdinalIgnoreCase);
            closingIndex = proposedSolution.IndexOf(closeString, openingIndex, StringComparison.OrdinalIgnoreCase);


            //If there is no key the proposed solution is wrong
            if (openingIndex == -1 || closingIndex == -1)
            {
                return false;
            }
            int lenghtOfSubString = closingIndex - (openingIndex + openString.Length);
            keys[i] = proposedSolution.Substring((openingIndex + openString.Length), lenghtOfSubString);
        }

        //replace first key value with AAA
        proposedSolution= proposedSolution.Replace(keys[0], "AAA");
        //replace second key with BBB
        proposedSolution = proposedSolution.Replace(keys[1], "BBB");
        //If the third key is equal to first replace with AAA 
        //instead replace with CCC
        if (string.Compare(keys[0], keys[2]) == 0)
        {
            proposedSolution = proposedSolution.Replace(keys[2], "AAA");
        }
        else
        {
            proposedSolution = proposedSolution.Replace(keys[2], "CCC");
        }


        /* TEST THE SOLUTION ELABORATED WITH THE SOLUTION STORED IN THE LEVEL DATA OBJECT */        
        foreach (string sol in levelData.solutions)
        {
            if (string.Compare(sol, proposedSolution) == 0)
                return true;
        }
        return false;
    }
 
}

	
    
