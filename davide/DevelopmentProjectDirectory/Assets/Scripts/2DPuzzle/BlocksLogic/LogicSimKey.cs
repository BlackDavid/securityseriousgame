﻿using UnityEngine;
using System.Collections;
using System;

//In the simmetric key logic the operation mode is the key lenght 
public class LogicSimKey : MonoBehaviour,IBlockLogic {

    private BlockModel model;
    public int keyID;
    
    public GameObject output1;
    
    //Block's Sprite 
    public Sprite spriteDef;
    public Sprite sprite0;
    public Sprite sprite1;
    public Sprite sprite2;
    public Sprite sprite3;


    void Awake()
    {
        model = gameObject.GetComponent<BlockModel>();
        keyID = GetInstanceID();
    }


    public void SetOutput()
    {

        //If all the inputs are linked then the outputs can be elaborated
        if (model.ElaborateInputs())
        {
            string outString = string.Concat("[",model.blockType,"]",
                keyID.ToString(),"[lenght]",model.getCurrentOperation(),"[/lenght]",
                "[/",model.blockType,"]");
            output1.GetComponentInChildren<OutputPlugController>().ChangeOutputData(true,outString);
        }
    }

    //Used to change the block's image
    public void ChangeOperation(int oID)
    {
        switch (oID)
        {
            case 0:
                gameObject.GetComponent<SpriteRenderer>().sprite = sprite0;
                break;
            case 1:
                gameObject.GetComponent<SpriteRenderer>().sprite = sprite1;
                break;
            case 2:
                gameObject.GetComponent<SpriteRenderer>().sprite = sprite2;
                break;
            case 3:
                gameObject.GetComponent<SpriteRenderer>().sprite = sprite3;
                break;
            default:
                gameObject.GetComponent<SpriteRenderer>().sprite = spriteDef;
                break;
        }
    }


}
