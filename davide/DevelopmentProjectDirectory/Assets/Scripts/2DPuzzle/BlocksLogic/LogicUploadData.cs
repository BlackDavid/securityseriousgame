﻿using UnityEngine;
using System.Collections;
using System;

public class LogicUploadData : MonoBehaviour,IBlockLogic {

    private BlockModel model;
    private LevelController levelController;
    //Rename the input and the output name
    public GameObject input1;

    //Block's Sprite 
    public Sprite spriteDef;

    /**DEPRECATED
    /// <summary>
    /// True if all the block Input are linked
    /// </summary>
    private bool allInputLinked = false;
    */

    /// <summary>
    /// Output String for the level manager
    /// </summary>
    private string outString = string.Empty;


    void Awake()
    {

        model = gameObject.GetComponent<BlockModel>();
        levelController=GameObject.FindGameObjectWithTag("LevelController").GetComponent<LevelController>();
        if (levelController == null)
        {
            throw new Exception("No Level manager Found!");
        }
    }


    public void SetOutput()
    {
        //If all the inputs are linked then the outputs can be elaborated
        if (model.ElaborateInputs())
        {
            //Tell the level manager that the solutions is right
            levelController.CheckSolution( input1.GetComponentInChildren<InputSocketController>().inMessage);
            //string test = "Downloaded_Input_Data<Des><op>Encryption</op><key><Simmetric-Key><key>-27440</key><lenght>64bit</lenght></Simmetric-Key></key></Des><Des><op>Decryption</op><key><Simmetric-Key><key>-27380</key><lenght>64bit</lenght></Simmetric-Key></key></Des><Des><op>Encryption</op><key><Simmetric-Key><key>-27320</key><lenght>64bit</lenght></Simmetric-Key></key></Des>";
            //levelController.CheckSolution(test);
        }
    }
        
    //Used to change the block's image
    public void ChangeOperation(int oID)
    {
        switch (oID)
        {
            /*
            case 0:
                gameObject.GetComponent<SpriteRenderer>().sprite = sprite0;
                break;
            case 1:
                gameObject.GetComponent<SpriteRenderer>().sprite = sprite1;
                break;
            case 2:
                gameObject.GetComponent<SpriteRenderer>().sprite = sprite2;
                break;
            case 3:
                gameObject.GetComponent<SpriteRenderer>().sprite = sprite3;
                break;
                */
            default:
                gameObject.GetComponent<SpriteRenderer>().sprite = spriteDef;
                break;
        }
    }
}
