﻿using UnityEngine;
using System.Collections;

public class LogicDes : MonoBehaviour,IBlockLogic {

    private BlockModel model;
    //Rename the input and the output name
    public GameObject input1;
    public GameObject key;
    public GameObject output1;

    //Block's Sprite 
    public Sprite sprite1;
    public Sprite sprite2;

    /// <summary>
    /// True if all the block Input are linked
    /// </summary>
    //private bool allInputLinked = false;

    void Awake()
    {
        model = gameObject.GetComponent<BlockModel>();
    }
    void Update()
    {
        /*
        if (input1.GetComponentInChildren<InputSocketController>().linkState && key.GetComponentInChildren<InputSocketController>().linkState)
            allInputLinked = true;
        else
            allInputLinked = false;
            */
    }

    public void SetOutput()
    {

        //If all the inputs are linked then the outputs can be elaborated
        if (model.ElaborateInputs())
        {

            string outString = string.Concat(input1.GetComponentInChildren<InputSocketController>().inMessage,
                "[", model.blockType,"]", "[op]",model.getCurrentOperation(),"[/op]",
                "[key]",key.GetComponentInChildren<InputSocketController>().inMessage,"[/key]",
                "[/",model.blockType,"]");
            //Debug.Log("Stringa: " + outString);
            output1.GetComponentInChildren<OutputPlugController>().ChangeOutputData(true,outString);
        }
        else
        {
            //return empty string
            output1.GetComponentInChildren<OutputPlugController>().ChangeOutputData(true, string.Empty);
        }
    }



    //Used to change the block's image
    public void ChangeOperation(int oID)
    {
        //Debug.Log("Logic method called oID: "+oID.ToString());
        switch (oID)
        {
            case 0:
                gameObject.GetComponent<SpriteRenderer>().sprite = sprite1;
                break;
            case 1:
                gameObject.GetComponent<SpriteRenderer>().sprite = sprite2;
                break;
            default:
                gameObject.GetComponent<SpriteRenderer>().sprite = sprite1;
                break;
        }
    }
}
