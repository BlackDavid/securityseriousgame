﻿using UnityEngine;
using System.Collections;
using System;

public class LogicDownloadData : MonoBehaviour,IBlockLogic {

    private BlockModel model;
    private LevelController levelController;

    //Rename the input and the output name
    public GameObject output1;

    //Block's Sprite 
    public Sprite spriteDef;

    /**DEPRECATED
    /// <summary>
    /// True if all the block Input are linked
    /// </summary>
    private bool allInputLinked = false;
    */

    void Awake()
    {
        model = gameObject.GetComponent<BlockModel>();
        levelController = GameObject.FindGameObjectWithTag("LevelController").GetComponent<LevelController>();
    }
    void Update()
    {
    }

    public void SetOutput()
    {
        output1.GetComponentInChildren<OutputPlugController>().ChangeOutputData(true, "Downloaded_Input_Data");
        
    }

    public void ChangeOperation(int oID)
    {
        //Not implemented for the Input Data Block
        
    }


}
