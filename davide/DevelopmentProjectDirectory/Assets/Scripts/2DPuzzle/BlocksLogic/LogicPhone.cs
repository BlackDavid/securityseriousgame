﻿using UnityEngine;
using System.Collections;

public class LogicPhone : MonoBehaviour,IBlockLogic {

    private BlockModel model;
    private LevelController levelController;
    //Rename the input and the output name
    public GameObject input1;

    //String used to submit the phone input to the level controller
    private string uploadString=string.Empty;

    //Block's Sprite 
    public Sprite spriteDef;


    /*Not needed for now
     * public Sprite sprite0;
     * public Sprite sprite1;
     * public Sprite sprite2;
     * public Sprite sprite3;
     */



    void Awake()
    {
        model = gameObject.GetComponent<BlockModel>();
        levelController = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelController>();
    }
    void Update()
    {
    }

    public void SetOutput()
    {
        //If all the inputs are linked then the outputs can be elaborated
        if (model.ElaborateInputs())
        {
            uploadString= input1.GetComponentInChildren<InputSocketController>().inMessage +
            "<" + model.blockType + ">" +
            "<op>" + model.getCurrentOperation() + "</op>" +
            "</" + model.blockType + ">";
            levelController.CheckPhoneInput(uploadString);
        }
        else
        {
            //Send the empty string
            levelController.CheckPhoneInput(string.Empty);
            
        }
    }

    //Used to change the block's image
    public void ChangeOperation(int oID)
    {
        switch (oID)
        {
            /*
            case 0:
                gameObject.GetComponent<SpriteRenderer>().sprite = sprite0;
                break;
            case 1:
                gameObject.GetComponent<SpriteRenderer>().sprite = sprite1;
                break;
            case 2:
                gameObject.GetComponent<SpriteRenderer>().sprite = sprite2;
                break;
            case 3:
                gameObject.GetComponent<SpriteRenderer>().sprite = sprite3;
                break;
                */
            default:
                gameObject.GetComponent<SpriteRenderer>().sprite = spriteDef;
                break;
        }
    }

    public void Test()
    {
        Debug.Log("Write here your Loading test message");
    }
}
