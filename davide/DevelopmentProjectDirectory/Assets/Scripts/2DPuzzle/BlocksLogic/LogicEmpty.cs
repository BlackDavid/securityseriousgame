﻿using UnityEngine;
using System.Collections;

public class LogicEmpty : MonoBehaviour,IBlockLogic {

    private BlockModel model;
    //Rename the input and the output name
    public GameObject input1;
    public GameObject input2;
    public GameObject output1;

    //Block's Sprite 
    public Sprite spriteDef;
    public Sprite sprite0;
    public Sprite sprite1;
    public Sprite sprite2;
    public Sprite sprite3;



    void Awake()
    {
        model = gameObject.GetComponent<BlockModel>();
    }
    void Update()
    {
    }

    public void SetOutput()
    {

        //If all the inputs are linked then the outputs can be elaborated
        if (model.ElaborateInputs())
        {
            output1.GetComponentInChildren<OutputPlugController>().ChangeOutputData(
            true,
            input1.GetComponentInChildren<InputSocketController>().inMessage +
            "<"+model.blockType+">" +
            "<op>" + model.getCurrentOperation()+ "</op>" + 
            input2.GetComponentInChildren<InputSocketController>().inMessage +
            "</" + model.blockType + ">");
        }
        else
        {
            //return empty string
            output1.GetComponentInChildren<OutputPlugController>().ChangeOutputData(false, string.Empty);
        }
    }

    //Used to change the block's image
    //Used to change the block's image
    public void ChangeOperation(int oID)
    {
        switch (oID)
        {
            case 0:
                gameObject.GetComponent<SpriteRenderer>().sprite = sprite0;
                break;
            case 1:
                gameObject.GetComponent<SpriteRenderer>().sprite = sprite1;
                break;
            case 2:
                gameObject.GetComponent<SpriteRenderer>().sprite = sprite2;
                break;
            case 3:
                gameObject.GetComponent<SpriteRenderer>().sprite = sprite3;
                break;
            default:
                gameObject.GetComponent<SpriteRenderer>().sprite = spriteDef;
                break;
        }
    }

    public void Test()
    {
        Debug.Log("Write here your Loading test message");
    }
}
