﻿using UnityEngine;
using System.Collections;

public class LogicWireScan : MonoBehaviour,IBlockLogic {

    private BlockModel model;
    //Rename the input and the output name
    public GameObject input1;
    public GameObject output1;
    public GameObject vampired;

    //Block's Sprite 
    public Sprite spriteDef;
    /*
     * public Sprite sprite0;
     * public Sprite sprite1;
     * public Sprite sprite2;
     * public Sprite sprite3;
    */



    void Awake()
    {
        model = gameObject.GetComponent<BlockModel>();
    }
    void Update()
    {
    }

    public void SetOutput()
    {

        //If all the inputs are linked then the outputs can be elaborated
        if (model.ElaborateInputs())
        {
            //Replicate the input on the 2 output
            output1.GetComponentInChildren<OutputPlugController>().ChangeOutputData(
            true,
            input1.GetComponentInChildren<InputSocketController>().inMessage);
            vampired.GetComponentInChildren<OutputPlugController>().ChangeOutputData(
            true,
            input1.GetComponentInChildren<InputSocketController>().inMessage);
        }
        else
        {
            //return empty string
            output1.GetComponentInChildren<OutputPlugController>().ChangeOutputData(false, string.Empty);
            vampired.GetComponentInChildren<OutputPlugController>().ChangeOutputData(false, string.Empty);
        }
    }

    //Used to change the block's image
    //Used to change the block's image
    public void ChangeOperation(int oID)
    {
        switch (oID)
        {
            /*
            case 0:
                gameObject.GetComponent<SpriteRenderer>().sprite = sprite0;
                break;
            case 1:
                gameObject.GetComponent<SpriteRenderer>().sprite = sprite1;
                break;
            case 2:
                gameObject.GetComponent<SpriteRenderer>().sprite = sprite2;
                break;
            case 3:
                gameObject.GetComponent<SpriteRenderer>().sprite = sprite3;
                break;
                */
            default:
                gameObject.GetComponent<SpriteRenderer>().sprite = spriteDef;
                break;
        }
    }

    public void Test()
    {
        Debug.Log("Write here your Loading test message");
    }
}
