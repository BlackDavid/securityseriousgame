﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class LibraryItem
{
    /// <summary>
    /// Xml file that contain the block data
    /// </summary>
    public TextAsset xmlFile;
}


public class BlockLibraryScrollList : MonoBehaviour {

    public List<LibraryItem> blockList;
    public Transform contentPanel;
    public GameObject SampleLibraryItemPrefab;
    
	
	void Awake () {
        AddItems();
	}

    private void AddItems()
    {
        for(int i=0;i< blockList.Count; i++)
        {
            GameObject item = (GameObject)Instantiate(SampleLibraryItemPrefab, contentPanel);
            item.GetComponentInChildren<LibraryItemController>().Setup(blockList[i].xmlFile);
        }
    }
	

}
