﻿using UnityEngine;
using UnityEngine.UI;
using System.Xml;

public class LibraryItemController : MonoBehaviour {

    public Image itemImage;
    public Text itemLabel;

    /// <summary>
    /// Xml file that contain the block data
    /// </summary>
    private TextAsset xmlFile;

    /// <summary>
    /// Xml variables to access the xml data
    /// </summary>
    XmlDocument xmlDoc = new XmlDocument();
    XmlNode xmlNode;
    XmlNodeList elemList;

    Sprite defaultSprite = new Sprite();
    string blockName=string.Empty;
    string blockPrefabName = string.Empty;

    /// <summary>
    /// Method used to setup the block item
    /// </summary>
    /// <param name="xml"></param>
    public void Setup(TextAsset xml)
    {
        xmlFile = xml;
        LoadXMLDataForGui();
        itemLabel.text = blockName;
        itemImage.sprite = defaultSprite;
    }
	
    /// <summary>
    /// This method load the block data from the xml
    /// </summary>
    private void LoadXMLDataForGui()
    {
        //Loading the block data from the xml file
        xmlDoc.LoadXml(xmlFile.text);

        //Loading the block type
        xmlNode = xmlDoc.SelectSingleNode("/block/type");
        blockName = xmlNode.InnerText.ToString().Trim();

        //loading the block default image
        elemList = xmlDoc.GetElementsByTagName("image");
        defaultSprite = Resources.Load<Sprite>("Sprites/BlockSprites/"+elemList[0].InnerText.ToString().Trim());

        //loading the block prefab name
        xmlNode = xmlDoc.SelectSingleNode("/block/prefab");
        blockPrefabName = xmlNode.InnerText.ToString().Trim();
    }

    /// <summary>
    /// Instantiate block in the game area
    /// </summary>
    public void InstantiateBlock()
    {
        GameObject block = (GameObject)Instantiate(Resources.Load("Prefabs/Blocks/"+blockPrefabName));
    }
}
