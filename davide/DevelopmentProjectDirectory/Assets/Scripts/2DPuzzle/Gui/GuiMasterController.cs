﻿using UnityEngine;
using UnityEngine.UI;
using System;


public class GuiMasterController : MonoBehaviour {

    #region Gui Variables

    LevelController levelController;
    
    /// <summary>
    /// Block Option Panel
    /// </summary>
    public GameObject blockGuiPanel;
    private Dropdown blockOperations;
    private Text blockType;
    private Text blockDescription;
    public Text timeLeft;

    /// <summary>
    /// Panel that has to be shown when the level is complete or time is over
    /// </summary>
    public GameObject gameOverGuiPanel;

    /// <summary>
    /// Initial level shown panel
    /// </summary>
    public GameObject levelStartGuiPanel;
    

    #endregion

    //Remember the last output slot that was selected
    private GameObject lastSelectedOutSlot=null;

    //Remember the last object that request a gui 
    private GameObject lastGuiRequesterObject = null;

    void Start()
    {
        levelController = GameObject.FindGameObjectWithTag("LevelController").GetComponent<LevelController>();
        InitializeBlockGui();
        InitializeStartLevelPanel();

        
    }






    #region other Methods

    /// <summary>
    /// Method used to populate the initial panel that show the level objectives and description
    /// </summary>
    private void InitializeStartLevelPanel()
    {
        Text[] text = levelStartGuiPanel.GetComponentsInChildren<Text>();
        //LevelData ld = levelController.getLevelData();
        text[0].text = levelController.getLevelData().levelTitle;
        text[1].text = levelController.getLevelData().levelDescription;
        gameOverGuiPanel.active = false;
        levelStartGuiPanel.active = true;
        
        

    }

    public void StartLevel()
    {
        levelStartGuiPanel.active = false;
        levelController.StartLevel();
        
    }

    /// <summary>
    /// This method load the Game Over panel and personalize the text 
    /// </summary>
    /// <param name="levelPassed">True if the user has succesfully passed the level</param>
    public void LoadGameOverPanel(bool levelPassed)
    {
        gameOverGuiPanel.SetActive(true);
        Text title = GameObject.FindGameObjectWithTag("GuiGameOverTitleText").GetComponent<Text>();
        Text info = GameObject.FindGameObjectWithTag("GuiGameOverInfoText").GetComponent<Text>();
        Text details = GameObject.FindGameObjectWithTag("GuiGameOverDetailsText").GetComponent<Text>();
        if (levelPassed)
        {
            title.text = "Test complete";
            info.text = "Your solution is correct";
            details.text = "";
        }else{
            title.text = "Game Over";
            info.text = "You fail the test";
            details.text = "time over you should be more fast!";
        }

    }

    /// <summary>
    /// Update the time remaining text display
    /// </summary>
    public void UpdateTimeLeftDisplay(float time)
    {
        TimeSpan ts =  TimeSpan.FromSeconds(time);
        if (ts.Seconds < 10){
            timeLeft.text = "Time Left: " + ts.Minutes+ ":0" + ts.Seconds;
        }else{
            timeLeft.text = "Time Left: " + ts.Minutes + ":" + ts.Seconds;
        }
        
    }

    /// <summary>
    /// Function used to initialize the block propriety panel 
    /// </summary>
    private void InitializeBlockGui()
    {
        //blockGuiPanel = GameObject.FindGameObjectWithTag("BlockGuiProprietyPanel");
        blockOperations = blockGuiPanel.GetComponentInChildren<Dropdown>();
        Text[] guiTexts = blockGuiPanel.GetComponentsInChildren<Text>();
        foreach(Text t in guiTexts)
        {
            if(t.tag.Equals("BlockGuiTypeText"))
                blockType = t;
            if (t.tag.Equals("BlockGuiDescriptionText"))
                blockDescription = t;                           
        }
        
        if (blockOperations != null)
        {
            blockOperations.onValueChanged.AddListener(delegate {
                BlockOperationsDropdownUpdate();
            });
        }
        blockGuiPanel.SetActive(false);
    }

    /// <summary>
    /// Define the behaviour of the block when the user ask for the block propriety panel
    /// </summary>
    public void OnBlockGuiRequested(GameObject source, BlockGuiEventArgs args)
    {
        lastGuiRequesterObject = args.block;

        //Update the Gui value
        blockDescription.text = args.description;
        blockType.text = args.type;
        //Clear and repopulate the dropdown options
        blockOperations.ClearOptions();
        foreach (string op in args.operations)
        {
            blockOperations.options.Add(new Dropdown.OptionData() { text = op });
        }
        blockOperations.value = args.currentOperationId;
        blockOperations.RefreshShownValue();
        //Activate the panel
        blockGuiPanel.gameObject.SetActive(true);
    }

    /// <summary>
    /// Close the Option Gui
    /// </summary>
    public void CloseGui()
    {
        blockGuiPanel.gameObject.SetActive(false);
        lastGuiRequesterObject = null;

    }

    /// <summary>
    /// Event handler for changing the output
    /// </summary>
    public void BlockOperationsDropdownUpdate()
    {
        //Saving the user choice
        lastGuiRequesterObject.GetComponent<BlockController>().ChangeOperation(blockOperations.value);
    }

    /// <summary>
    /// Block 
    /// </summary>
    public void DestroyBlock()
    {
        lastGuiRequesterObject.GetComponent<BlockController>().DestroyBlock();
        CloseGui();
    }

    /// <summary>
    /// Function used to add a gameobject to the Master Controller 
    /// </summary>
    /// <param name="obj">Object passed</param>
    public void Add(GameObject obj){
        //Checking the object type and subscribe the corresponding event
        switch (obj.tag)
        {
            case GameConstants.outputPlugTag:
                obj.GetComponent<OutputPlugController>().OutPlugShortPressed += OnOutputPlugShortPress;
                obj.GetComponent<OutputPlugController>().OutPlugLongPressed += OnOutputPlugLongPress;
                break;
            case GameConstants.inputSocketTag:
                obj.GetComponent<InputSocketController>().InSocketShortPressed += OnInputSocketShortPress;
                break;
            case GameConstants.blockTag:
                obj.GetComponent<BlockController>().BlockGuiRequested += OnBlockGuiRequested;
                break;
            default:
                Debug.LogError("MasterController.Add error: Component not recognized");
                Debug.LogError("Not recognized object:" + obj.name.ToString() + "with tag: "+obj.tag.ToString());
                break;
        }
    }

    /// <summary>
    /// Define the behaviour when the user tap an Output Plug
    /// </summary>
    public void OnOutputPlugShortPress(GameObject source, EventArgs e)
    {
        //Handling the selection and connection state 
        
        //If the output Plug is NOT connected then it can be selected or deselected
        if (!source.GetComponent<OutputPlugController>().linkState)
        {
            //No plug was previusly selected
            if (lastSelectedOutSlot == null)
            {
                //Set the status of selection 
                source.GetComponent<OutputPlugController>().UpdateSelectionState(true);
                lastSelectedOutSlot = source;
                //Debug.Log("Plug selezionato: " + source.ToString());
            }
            else
            //there was previusly selected plug
            {
                //if the previusly selected plug is clicked twice has to be deselect
                if (lastSelectedOutSlot == source)
                {
                    lastSelectedOutSlot.GetComponent<OutputPlugController>().UpdateSelectionState(false);
                    lastSelectedOutSlot = null;
                    //Debug.Log("Deseleziono Plug");
                }else
                //if the user select a new plug the previous has to be deselected and the new have to be select
                {
                    lastSelectedOutSlot.GetComponent<OutputPlugController>().UpdateSelectionState(false);
                    //Debug.Log("Plug DEselezionato "+ lastSelectedOutSlot.ToString() + "e selezionato un nuovo " + source.ToString());
                    source.GetComponent<OutputPlugController>().UpdateSelectionState(true);
                    lastSelectedOutSlot = source;
                }
            }
        }
    }

    /// <summary>
    /// Define the behaviour when the user press on an Input Socket
    /// </summary>
    public void OnInputSocketShortPress(GameObject source,EventArgs e)
    {
        //If was selected an output Plug then can be created a new Plug->Socket connection 
        if (lastSelectedOutSlot != null)
        {
            lastSelectedOutSlot.GetComponent<OutputPlugController>().ConnectionRequest(source);
            lastSelectedOutSlot.GetComponent<OutputPlugController>().UpdateSelectionState(false);
            lastSelectedOutSlot = null;
        }
    }

    /// <summary>
    /// Define the behaviour when the user perform a long press on an Output Plug
    /// </summary>
    public void OnOutputPlugLongPress(GameObject source, EventArgs e)
    {
        //If the plug is Connected start the disconnection request
        if (source.GetComponent<OutputPlugController>().linkState)
        {
            source.GetComponent<OutputPlugController>().DisconnectionRequest();
        }
    }

    /// <summary>
    /// Function used to return to main menu
    /// </summary>
    public void ReturnToMainMenu()
    {
        levelController.ReturnToMainMenu();
    }



    #endregion
}
