﻿using UnityEngine;
using System;

/// <summary>
/// This Class allow the user to distinguish between long and short selection (click or click and hold)
/// There are 2 event to handle the different event: ShortPressFired and LongPressFired.
/// ShortPressFired is thrwn when the user press and immediatly release the input button o finger (click or tap) 
/// LongPressFired is thrown when the user press and hold the input button or finget (click/tap and hold)
/// </summary>
public class PressAndHold : MonoBehaviour {
   

    //Counter for the pressing time
    private float pressTime = 0f;
    //identify if the user is pressing 
    private bool pressing = false;
    //Identify if user is performing a long press
    private bool longPressing = false;


    /*Short press and Long Press events*/
    public event GameDelegates.ShortPressEventHandler ShortPressFired;
    public event GameDelegates.LongPressEventHandler LongPressFired;

    
    // Update is called once per frame
    void Update()
    {
        /*If the user is interacting but the long press event was not rised 
          the press time have to be counted
          with this control the long press event is rised only once*/
        if (pressing && !longPressing)
        {
            if (pressTime < GameConstants.longPressTimeDelay)
            {
                pressTime += Time.deltaTime;
            }
            else
            {
                //throw the long press event
                OnLongPress();
            }
        }
    }


    void OnMouseDown()
    {
        pressing = true;
        longPressing = false;
    }


    void OnMouseUp()
    {
        if (pressTime < GameConstants.longPressTimeDelay)
        {
            //throw the short press event
            OnShortPress();
        }
        //Resetting the press status
        pressing = false;
        longPressing = false;
        pressTime = 0f;
    }

    //EventPublishers
    protected virtual void OnShortPress()
    {
        if (ShortPressFired != null)
        {
            ShortPressFired(gameObject, EventArgs.Empty);
        }
    }


    protected virtual void OnLongPress()
    {
        longPressing = true;
        if (LongPressFired != null)
        {
            LongPressFired(gameObject, EventArgs.Empty);
        }
    }
}
