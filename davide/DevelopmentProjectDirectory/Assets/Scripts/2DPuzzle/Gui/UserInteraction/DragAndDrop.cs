﻿using UnityEngine;


/// <summary>
// This class allow to handle the drag and dtop of the graphical element
/// </summary>
public class DragAndDrop : MonoBehaviour {

    //Current position of the user finger or mouse pointer
    private Vector2 userInputPosition;
    //Offset between the sprite center and the user touch or mouse pointer
    private Vector2 inputOffset;


    /// <summary>
    /// Return the coordinates of the user input position
    /// </summary>
    Vector2 CurrentUserInputPosition()
    {
        return Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    void OnMouseDown()
    {
        //Calculate the offset between the center of the sprite and the user input coordinates
        Vector2 inputPosition = CurrentUserInputPosition();
        inputOffset = (Vector2)transform.position - inputPosition;
    }

    void OnMouseDrag()
    {
        Vector2 inputPosition = CurrentUserInputPosition();
        transform.position = inputPosition + inputOffset;
    }
}
