﻿using UnityEngine;
using System;


/// <summary>
/// This class allow to handle the double Press event on an object
/// </summary>
public class DoublePress : MonoBehaviour
{

    //Double press event
    public event GameDelegates.DoublePressEventHandler DoublePressFired;

    //Delay between two presses that allows to distinguish double Press event
    float delay = GameConstants.doublePressTimeDelay;
    //The time of the last press event
    float lastPressTime;
    //Identify the first press of the sequence
    bool firstPress = true;

    //EventPublisher
    protected virtual void OnDoublePress()
    {
        if (DoublePressFired != null)
        {
            DoublePressFired(gameObject, EventArgs.Empty);
        }
    }

    // Update is called once per frame
    void OnMouseDown()
    {
        if (firstPress) // first press no previous presses
        {
            firstPress = false;
            lastPressTime = Time.time; // save the current time
        }
        else
        {
            if ((Time.time - lastPressTime) < delay)
            {
                OnDoublePress();
                //Found double press now reset
                firstPress = true;
            }else
            //If the delay between two presses it's too long the current press coud be the first of new double press
            {
                firstPress = false;
                lastPressTime = Time.time;
            }
        }
    }
}



