﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class MainMenuItem
{
    /// <summary>
    /// Xml file that contain the Level data
    /// </summary>
    public TextAsset xmlLevelDataFile;
    //public string levelSceneName;
    public Object levelScene;
}


public class MainMenuScrollListController : MonoBehaviour {

    private GameController gameController;

    public List<MainMenuItem> levelList;
    public Transform contentPanel;
    public GameObject SampleMainMenuItemPrefab;
    
	
	void Awake () {
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        AddItems();
	}

    private void AddItems()
    {
        for(int i=0;i< levelList.Count; i++)
        {
            GameObject item = (GameObject)Instantiate(SampleMainMenuItemPrefab, contentPanel);

            item.GetComponentInChildren<MainMenuItemController>().Setup(
                levelList[i].xmlLevelDataFile,
                levelList[i].levelScene.name
                );            
        }
    }
	

}
