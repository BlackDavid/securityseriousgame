﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Xml;
using System;

public class MainMenuItemController : MonoBehaviour {
    /*
     * 0- ogni livello deve avere il nome della scena associata altrimenti non funziona - OK
     * 1- Apre XML e ne estrae i seguenti dati: - OK
     * 1.1- Nome del livello - OK
     * 1.2- Nomi delle sprite da mostrare in caso di superato o no - OK
     * 1.3- ID univoco del livello - OK
     * 2- cerca se esiste il file json corrispondente - OK
     * 2.1- se il file non esiste viene creato e popolato di default - OK
     * 2.2- se il file esiste estra i seguenti dati - OK
     * 2.2.1- Se il livello è abilitato - OK
     * 2.2.2- il time record del livello - OK
     * 2.2.3- il totale del tempo speso sul livello - OK
     * 3- Notificare al game controller che questo livelo è disponibile e deve essere aggiunto alla lista dei livelli giocabili
     * 4- quando l'utente clicca sul livello chiede al game manager di avviarlo - OK
     */

    private GameController gameController; 

    public Image itemImage;
    public Text itemLabelName;
    public Text itemLabelEnabled;
    public Text itemLabelTimeRecord;
    public Text itemLabelTimeSpent;



    /// <summary>
    /// Xml file that contain the block data
    /// </summary>
    private TextAsset xmlFile;

    /// <summary>
    /// The unity scene associated with this level
    /// </summary>
    private string levelSceneName;

    //Level Data
    LevelData levelData;
    
    void Awake()
    {
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

    /// <summary>
    /// Method used to setup the Main Menu Level Item
    /// </summary>
    /// <param name="xml"></param>
    public void Setup(TextAsset xml,string sceneName)
    {
        levelSceneName = sceneName;
        levelData = new LevelData(xml.name+".xml");
        itemLabelName.text = "Level Name:\n"+levelData.levelTitle;
        itemLabelTimeSpent.text = "Time Record:\n" + levelData.totalTimeSpent.ToString();
        itemLabelTimeRecord.text = "Total Time Spent On Level:\n" + levelData.bestPassTime.ToString();
        if (levelData.levelEnabled){
            itemLabelEnabled.text = "This Level is:\nEnabled";
            itemImage.sprite = Resources.Load<Sprite>("Sprites/LevelSprites/" + levelData.passImageName);
        }
        else{
            itemLabelEnabled.text = "This Level is:\nDisabled";
            itemImage.sprite = Resources.Load<Sprite>("Sprites/LevelSprites/" + levelData.notPassImageName);
        }

        //Notify to the game controller that this level has been added to the list of playable levels!
        gameController.AddLevel(levelData);
    }
	
    /// <summary>
    /// Ask the game controller to load the level
    /// </summary>
    public void StartLevel()
    {
        //If the level is enabled it could be played
        if (levelData.levelEnabled)
        {
            gameController.LoadLevel(levelData, levelSceneName);
        }
        
    }
}
