﻿using UnityEngine;
using System;
using System.Collections;
using System.Xml;
using System.Text.RegularExpressions;

public class LevelController : MonoBehaviour {



    #region attributes
    /// <summary>
    /// Level data
    /// </summary>
    private LevelData _levelData;
    
    


    /// <summary>
    /// Game Controller
    /// </summary>
    private GameController gameController;

    /// <summary>
    /// Master controller of the level
    /// </summary>
    private GuiMasterController guiMasterController;

    /// <summary>
    /// Level additional logit to check solution proposed
    /// </summary>
    private ILevelAdditionalLogic levelAdditionalLogic;
    
    //Time left to solve the puzzle
    private float timeLeft;

    /// <summary>
    /// True if the proposed solution is correct
    /// </summary>
    private bool userSolutionIsCorrect=false;
    
    /// <summary>
    /// If true the game will save the solution in a file without check if it is correct
    /// </summary>
    public bool saveSolutionMode = false;
    
    /// <summary>
    /// Used to start the level afther the intro panel 
    /// </summary>
    private bool levelStart = false;

    #endregion

    #region Awake-Start-Update

    void Awake()
    {

        //Find game Controller
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        //Finding the gui master controller 
        guiMasterController = GameObject.FindGameObjectWithTag("GuiMasterController").GetComponent<GuiMasterController>();
        //Loading Level Data
        _levelData = gameController.GetCurrentLevelData();
        //initiating the countdown timer and converting in seconds
        timeLeft = _levelData.time*60;

        levelAdditionalLogic = gameObject.GetComponent<ILevelAdditionalLogic>();
        if (levelAdditionalLogic == null)
        {
            throw new Exception("Error! Need to include the Additional Level logic file class");
        }
    }

	void Update () {        
        if (!userSolutionIsCorrect && levelStart)
        {
            //Updating the time left
            if (timeLeft > 0){
                timeLeft -= Time.deltaTime;
                guiMasterController.UpdateTimeLeftDisplay(timeLeft);
            }else{
                GameOver(false);
            }
        }
	}

    #endregion

    #region other Methods

    /// <summary>
    /// This method is used to check the use solutions
    /// </summary>
    /// <param name="result">Output string </param>
    public void CheckSolution(string proposedSolution)
    {
        if (saveSolutionMode){
            userSolutionIsCorrect = true;
            //Save the solutions to json file for later editing
            gameController.SaveSolution(proposedSolution);
            GameOver(true);
        }
        else
        {
            if (levelAdditionalLogic.CheckSolution(_levelData,proposedSolution))
            {
                //Block the countdown
                userSolutionIsCorrect = true;
                GameOver(true);
            }
        }
    }

    /// <summary>
    /// This method is used to check the phone input
    /// </summary>
    /// <param name="phoneInput">Input string of the phone block</param>
    public void CheckPhoneInput(string phoneInput)
    {

    }

    /// <summary>
    /// Method called when the level is complete or the time is over
    /// </summary>
    /// <param name="levelPassed">Set true if the user answer correctly, otherwise pass false</param>
    private void GameOver(bool levelPassed)
    {
        float timeSpent = (_levelData.time * 60) - timeLeft;
        //Time spent to solve the level
        _levelData.totalTimeSpent += timeSpent;
        if (levelPassed)
        {
            _levelData.levelPassed = true;
            if (_levelData.bestPassTime == -1 || _levelData.bestPassTime > timeSpent)
            {
                _levelData.bestPassTime = timeSpent;
            }
            
        }                               
        //Updating level data
        gameController.updateLevelData(_levelData);
        //Call gameOver Gui from Master Controller
        guiMasterController.LoadGameOverPanel(levelPassed);
    }
    #endregion

    /// <summary>
    /// Used to start time counting of the level
    /// called when the user click on the start level button
    /// of the intro panel
    /// </summary>
    public void StartLevel()
    {
        levelStart = true;
    }

    /// <summary>
    /// Used to return lo main menu
    /// </summary>
    public void ReturnToMainMenu()
    {
        gameController.LoadMainMenu();
    }

    /// <summary>
    /// Used to pass game data
    /// </summary>
    public LevelData getLevelData()
    {
        return gameController.GetCurrentLevelData();
    }

}




///// <summary>
///// Xml file that contain the level data
///// </summary>
//public TextAsset xmlFile;

///// <summary>
///// Xml variables to access the xml data
///// </summary>
//XmlDocument xmlDoc = new XmlDocument();
//XmlNode xmlNode;
//XmlNodeList elemList;


////Level ID
//int _levelID = -1;
///// <summary>
///// Level ID
///// </summary>
//public int levelID
//{
//    get { return _levelID; }
//}


//// The title of the level
//string _levelTitle = string.Empty;
///// <summary>
///// The title of the level 
///// </summary>
//public string levelTitle
//{
//    get { return _levelTitle; }
//}

//// The description of the level 
//string _levelDescription = string.Empty;
///// <summary>
///// The description of the level 
///// </summary>
//public string levelDescription
//{
//    get { return _levelDescription; }
//}

//// The time limit to solve the level    
//float _time = 0.0f;
///// <summary>
///// Time left for the game over
///// </summary>
//private float timeLeft;

//// The successful message 
//string _congratMessage = string.Empty;
///// <summary>
///// The successful message 
///// </summary>
//public string congratMessage
//{
//    get { return _congratMessage; }
//}

//// The list of the level solution
//ArrayList _solutions = new ArrayList();

///// <summary>
///// The number of level solutions
///// </summary>
//public int solutionsNumber
//{
//    get { return _solutions.Count; }
//}

///// <summary>
///// Load Level Xml Data 
///// </summary>
//public void LoadXMLData()
//{
//    //Loading the level data from the xml file
//    xmlDoc.LoadXml(xmlFile.text);

//    //Loading Level ID
//    xmlNode = xmlDoc.SelectSingleNode("/level");
//    _levelID = int.Parse(xmlNode.Attributes["lid"].Value);

//    //Loading the level title
//    xmlNode = xmlDoc.SelectSingleNode("/level/title");
//    _levelTitle = xmlNode.InnerText.ToString();

//    //Loading the level description
//    xmlNode = xmlDoc.SelectSingleNode("/level/description");
//    _levelDescription = xmlNode.InnerText.ToString();

//    //Loading the congrat message
//    xmlNode = xmlDoc.SelectSingleNode("/level/congrat");
//    _congratMessage = xmlNode.InnerText.ToString();

//    //Loading the time limit
//    xmlNode = xmlDoc.SelectSingleNode("/level/time");
//    _time = Convert.ToSingle(xmlNode.InnerText.ToString());

//    //Loading the operation list
//    elemList = xmlDoc.GetElementsByTagName("solution");
//    for (int i = 0; i < elemList.Count; i++)
//    {
//        _solutions.Add(elemList[i].InnerText.ToString());
//    }
//}