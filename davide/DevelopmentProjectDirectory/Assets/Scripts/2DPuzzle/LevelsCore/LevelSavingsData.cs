﻿
/// <summary>
/// This class contain the level information
/// </summary>
[System.Serializable]
public class LevelSavingsData{
    /// <summary>
    /// Unique ID of the level
    /// </summary>
    public int levelID;

    /// <summary>
    /// Identify if the level is enabled (for example because the previous level is not completed)
    /// </summary>
    public bool levelEnabled=false;


    /// <summary>
    /// Identify if the user has correctly solved the level
    /// </summary>
    public bool levelPassed=false;

    /// <summary>
    /// Identify the best time in solving the level 
    /// </summary>
    public float bestPassTime=-1f;

    /// <summary>
    /// Identify the total amount of seconds passed on this level including the failure
    /// </summary>
    public float totalTimeSpent=0f;

    /// <summary>
    /// Identify the total number of failure 
    /// </summary>
    public int failureNumber=-1;



    public LevelSavingsData(int levelID)
    {
        this.levelID = levelID;
    }

}
