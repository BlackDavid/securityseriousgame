﻿using UnityEngine;
using System.Collections;

public interface ILevelAdditionalLogic
{

    /// <summary>
    /// This method check additional solutions
    /// </summary>
    bool CheckSolution(LevelData levelData, string proposedSolution);


}
