﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System;
using System.IO;

/// <summary>
/// This class contain all Level details and saving data loaded from xml and json file
/// </summary>
public class LevelData {

    private int _levelID;
    /// <summary>
    /// Level unique ID
    /// </summary>
    public int levelID { get { return _levelID; } }

    private string _levelTitle = string.Empty;
    public string levelTitle { get { return _levelTitle; } }

    private string _levelDescription = string.Empty;
    /// <summary>
    /// Level Desctiption
    /// </summary>
    public string levelDescription { get { return _levelDescription; } }

    private float _time;
    /// <summary>
    /// Total time in minutes to solve the puzzle
    /// </summary>
    public float time { get { return _time; } }

    private string _congrats = string.Empty;
    /// <summary>
    /// Congratulation message shown when user solve the puzzle
    /// </summary>
    public string congrats { get { return _congrats; } }

    private string _passImageName = string.Empty;
    /// <summary>
    /// Image to show in main menu when level has been solved
    /// </summary>
    public string passImageName { get { return _passImageName; } }

    private string _notPassImageName = string.Empty;
    /// <summary>
    /// Image to show in main menu when level hasn't been solved
    /// </summary>
    public string notPassImageName { get { return _notPassImageName; } }

    /// <summary>
    /// List of th esolutions for the level
    /// </summary>
    public List<string> solutions = new List<string>();

    /// <summary>
    /// Saving data to update each time user play this level.
    /// This information has to be stored in a json file and loaded / updated each time the user plays the game
    /// </summary>
    private LevelSavingsData levelSavingData;

    /// <summary>
    /// Identify if the level is enable or not
    /// </summary>
    public bool levelEnabled {
        get { return levelSavingData.levelEnabled; }
        set { levelSavingData.levelEnabled = value; }
    }

    /// <summary>
    /// Identify if the user has correctly solved the level
    /// </summary>
    public bool levelPassed
    {
        get { return levelSavingData.levelPassed; }
        set { levelSavingData.levelPassed = value; }
    }

    /// <summary>
    /// Identify the best time in solving the level 
    /// </summary>
    public float bestPassTime
    {
        get { return levelSavingData.bestPassTime; }
        set { levelSavingData.bestPassTime = value; }
    }

    /// <summary>
    /// Identify the total amount of seconds passed on this level including the failure
    /// </summary>
    public float totalTimeSpent
    {
        get { return levelSavingData.totalTimeSpent; }
        set { levelSavingData.totalTimeSpent = value; }
    }

    /// <summary>
    /// Identify the total number of failure 
    /// </summary>
    public int failureNumber
    {
        get { return levelSavingData.failureNumber; }
        set { levelSavingData.failureNumber = value; }
    }

    //Relative path of the XML FILES
    private string levelDataXMLDirectory = Path.Combine(Application.dataPath,"Resources/Xml/Levels/");
    
    // Xml file that contain the block data
    private string levelXmlFileName;

    //complete path of the xml file
    private string levelXmlFilePath;

    //File name of the json saving data
    private string savingDataJsonFileName;

    //json saving data file path
    private string savingDataJsonFilePath;
  
    public LevelData(string xmlFileName)
    {
        //Load level information form xml file
        levelXmlFileName = xmlFileName;
        levelXmlFilePath = Path.Combine(levelDataXMLDirectory, levelXmlFileName);
        LoadXmlLevelData();
        //Load level savings data from json file
        savingDataJsonFileName = levelXmlFileName.Replace(".xml", ".json");
        savingDataJsonFilePath = Path.Combine(Application.streamingAssetsPath, savingDataJsonFileName);
        LoadLevelSavingsData();
    }

    /// <summary>
    /// Load Level Xml Data 
    /// </summary>
    private void LoadXmlLevelData()
    {
        // Xml variables to access the xml data
        XmlDocument xmlDoc = new XmlDocument();
        XmlNode xmlNode;
        XmlNodeList elemList;

        //Loading the level data from the xml file
        xmlDoc.Load(levelXmlFilePath);

        //Loading Level ID
        xmlNode = xmlDoc.SelectSingleNode("/level");
        _levelID = int.Parse(xmlNode.Attributes["lid"].Value.Trim());

        //Loading the level title
        xmlNode = xmlDoc.SelectSingleNode("/level/title");
        _levelTitle = xmlNode.InnerText.ToString().Trim();

        //Loading the level description
        xmlNode = xmlDoc.SelectSingleNode("/level/description");
        _levelDescription = xmlNode.InnerText.ToString().Trim();

        //Loading the congrat message
        xmlNode = xmlDoc.SelectSingleNode("/level/congrat");
        _congrats = xmlNode.InnerText.ToString().Trim();

        //Loading the time limit
        xmlNode = xmlDoc.SelectSingleNode("/level/time");
        _time = Convert.ToSingle(xmlNode.InnerText.ToString().Trim());

        //Loading the operation list
        elemList = xmlDoc.GetElementsByTagName("solution");
        for (int i = 0; i < elemList.Count; i++)
        {
            solutions.Add(elemList[i].InnerText.ToString().Trim());
        }

        //Loading the pass and not pass images
        elemList = xmlDoc.GetElementsByTagName("image");
        _notPassImageName= elemList[0].InnerText.ToString().Trim();
        _passImageName = elemList[1].InnerText.ToString().Trim();
    }


    /// <summary>
    /// load level savings data from json file
    /// </summary>
    /// <returns>LevelSavingsData object </returns>
    private void LoadLevelSavingsData()
    {
        //string used to convert levelData to json
        string dataAsJson = string.Empty;

        //LOAD THE JSON DATA FILE IF EXIST otherwise create newone
        if (File.Exists(savingDataJsonFilePath))
        {
            //attempt to load the file
            //read all the text from the file path into a string of json
            dataAsJson = File.ReadAllText(savingDataJsonFilePath);
            //deserialization
            levelSavingData= JsonUtility.FromJson<LevelSavingsData>(dataAsJson);
        }
        else
        {
            //if file doesn't exists the levels data object has to be filled in with information from levels xml files
            //creating json File
            Debug.Log("Warnign json data file doesn't exist, creating");
            File.CreateText(savingDataJsonFilePath).Dispose();
            Debug.Log("Json file created");
            levelSavingData = new LevelSavingsData(_levelID);
            //serialization of new level data
            dataAsJson = JsonUtility.ToJson(levelSavingData);
            File.WriteAllText(savingDataJsonFilePath, dataAsJson);
            Debug.Log("Saving complete");
        }
    }

    /// <summary>
    /// Update the level saving data to json file
    /// </summary>
    public void UpdateLevelSavingsData()
    {
        //string used to convert levelData to json
        string dataAsJson = string.Empty;
        //serialization of new level data
        dataAsJson = JsonUtility.ToJson(levelSavingData);
        if (File.Exists(savingDataJsonFilePath))
        {
            File.WriteAllText(savingDataJsonFilePath, dataAsJson);
            Debug.Log("Saving complete");
        }
        else
        {
            Debug.Log("Warnign json data file doesn't exist, creating");
            File.CreateText(savingDataJsonFilePath).Dispose();
            Debug.Log("Json file created");
            File.WriteAllText(savingDataJsonFilePath, dataAsJson);
            Debug.Log("Saving complete");
        }
    }
}
