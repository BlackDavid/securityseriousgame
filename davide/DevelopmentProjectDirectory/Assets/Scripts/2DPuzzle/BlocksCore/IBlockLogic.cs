﻿using UnityEngine;
using System.Collections;

public interface IBlockLogic
{

    /// <summary>
    /// This method set the output value of the block
    /// </summary>
    void SetOutput();

    /// <summary>
    /// Change the Current block operation
    /// </summary>
    /// <param name="oID">Operation id as specified in the xml file</param>
    void ChangeOperation(int oID);
    
    



}
