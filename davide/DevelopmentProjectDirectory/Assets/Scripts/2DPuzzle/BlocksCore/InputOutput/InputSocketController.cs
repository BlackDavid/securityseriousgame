﻿using UnityEngine;
using System;

public class InputSocketController : MonoBehaviour {

    #region Properties

    private InputSocketModel model;
    private InputSocketView view;

    //Input Message
    private string _inMessage = string.Empty;
    public string inMessage
    {
        get { return _inMessage; }
    }

    /// <summary>
    /// The linkstate, propriety created to isolate the view and the model
    /// </summary>
    public bool linkState
    {
        get { return model.linkState; }
    }

    /// <summary>
    /// The component that handle the short and long press
    /// </summary>
    private PressAndHold pressAndHoldComponent;

    /// <summary>
    /// Event rised when the data of on this input change
    /// </summary>
    public event GameDelegates.DataChangedEventHandler InputDataChanged;

    /// <summary>
    /// Event rised when the user select an Input Socket
    /// </summary>
    public event GameDelegates.ShortPressEventHandler InSocketShortPressed;

    #endregion

    #region Awake-Start-Update
    void Awake()
    {
        /*Settg the layer and the tag of the object*/
        gameObject.layer = LayerMask.NameToLayer("InputSockets");
        gameObject.tag = GameConstants.inputSocketTag;

        //Initialization of the view and model components
        model = gameObject.AddComponent<InputSocketModel>();
        view = gameObject.AddComponent<InputSocketView>();

        //GameComponent that handle the press and long press event
        pressAndHoldComponent = gameObject.GetComponent<PressAndHold>();

        //Subscribe the event of the InputSocket
        pressAndHoldComponent.ShortPressFired += OnComponentShortPressEventFired;

        

    }
    #endregion

    #region event publisher and subscriber
    /// <summary>
    /// Event Subscriber for the PressAndHoldComponent event
    /// </summary>
    public void OnComponentShortPressEventFired(GameObject source, EventArgs e)
    {
        OnInSocketShortPressed();
    }

    /// <summary>
    /// Event Publisher of the Input Socket game object for the short press user action
    /// </summary>
    private void OnInSocketShortPressed()
    {
        if (InSocketShortPressed != null)
        {
            InSocketShortPressed(gameObject, EventArgs.Empty);
        }
    }


    /// <summary>
    /// Called when the input data change
    /// </summary>
    protected virtual void OnInputDataChanged()
    {
        if (InputDataChanged != null)
        {
            InputDataChanged(gameObject, EventArgs.Empty);
        }
    }

    /// <summary>
    /// Event handler for the output plug
    /// </summary>
    public void OnLinkedOutputPlugDataChanged(GameObject source, EventArgs e)
    {
        //Rise event for the connected block
        OnInputDataChanged();
    }

    #endregion

    #region OtherMethods

    /*Called from the OutputPlug when is released over the input socket, 
     * if the socket is not linked to other plug return true 
     * and set Socket as plugged 
     * otherwise return false.*/
    public bool ConnectionRequest(GameObject requester)
    {   //If the Socket is free and the the connection request come from an OutputPlug
        if (!model.linkState && requester.layer==LayerMask.NameToLayer("OutputPlugs"))
        {
            model.connectedPlug = requester.GetComponent<OutputPlugController>();
            model.linkState = true;
            
            //The Input Socket has to subscribe the event of changing data from the output plug.
            requester.GetComponent<OutputPlugController>().DataChanged += OnLinkedOutputPlugDataChanged;
            return true;
        }
        return false;
    }
    
    //Called when The Output Plug is dragged out from the Input Socket 
    public bool DisconnectionRequest(GameObject requester)
    {
        //If the socket is connected and the requester is the connected Plug
        if (model.linkState && requester.GetComponent<OutputPlugController>() == model.connectedPlug)
        {
            model.connectedPlug = null;
            model.linkState = false;
            Debug.Log(gameObject.name.ToString() + " and " + requester.name.ToString() +" disconnected");
            //Delete the subscription to the event
            requester.GetComponent<OutputPlugController>().DataChanged -= OnLinkedOutputPlugDataChanged;
            return true;
        }
        return false;
    }

    //Method called to force the disconnection from connected plug
    public void ForcePlugDisconnection()
    {
        //If the socket is connected, call the disconnection method of the connected plug
        if (model.linkState)
        {
            model.connectedPlug.DisconnectionRequest();
        }
    }

    /// <summary>
    /// Method return the Linked Output Plug state and the additional message with reference variable "msg" 
    /// </summary>
    /// <returns>The state of the linked OutputPlug </returns>
    public bool CheckInputData()
    {
        //If the Input Socket is linked to another block return the Output Plug Data
        if (model.linkState)
        {
            /*DEPRECATED
            _type = model.connectedPlug.outputType;
            */
            _inMessage = model.connectedPlug.outputMessage;
            //Debug.Log("Connected Plug: "+model.connectedPlug.gameObject.name.ToString());

            return model.connectedPlug.outputState;            
        }
        else
        {
            //If the input socket isn't linked than return false, empty message string and empty message type
            /*DEPRECATED
            _type = GameData.type.Empty;
            */
            _inMessage = string.Empty;
            return false;
        }
    }

    #endregion
}
