﻿using UnityEngine;
using System.Collections;

/// <summary>
/// The model of the MVC architecture for the InputSocekt Object
/// </summary>
public class InputSocketModel : MonoBehaviour {




    /*Type of data that this input socket accept*/
    private BlockEnum.type _inType = BlockEnum.type.Empty;
    /// <summary>
    /// Type of data that this output slot use
    /// </summary>
    public BlockEnum.type inType
    {
        get { return _inType; }
        set { _inType = value; }
    }
    
    /*State of the link between this input socket plug and some output plug */
    private bool _linkState = false;
    /// <summary>
    /// State of the link between this input socket plug and some output plug
    /// True if the link is Up false if the link is down
    /// </summary>
    public bool linkState
    {
        get { return _linkState; }
        set { _linkState = value; }
    }

    /*The output plug this socket is connected to*/
    private OutputPlugController _connectedPlug = null;
    /// <summary>
    /// The output plug this socket is connected to
    /// </summary>
    public OutputPlugController connectedPlug
    {
        get { return _connectedPlug; }
        set { _connectedPlug = value; }
    }
}
