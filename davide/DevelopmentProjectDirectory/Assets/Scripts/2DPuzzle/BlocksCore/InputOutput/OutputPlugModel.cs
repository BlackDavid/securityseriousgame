﻿using UnityEngine;
using System;

/// <summary>
/// The model of the MVC architecture for the OutputPlug Object
/// </summary>
public class OutputPlugModel : MonoBehaviour {
    
    /*The output state*/
    private bool _outState = false;
    /// <summary>
    /// Output State
    /// </summary>
    public bool outState
    {
        get { return _outState; }
        set { _outState = value; }
    }

    /*Type of data that this output slot use*/
    private BlockEnum.type _outType = BlockEnum.type.Empty;
    /// <summary>
    /// Type of data that this output slot use
    /// </summary>
    public BlockEnum.type outType
    {
        get { return _outType; }
        set { _outType = value; }
    }

    /*The output message - can also contain data for the next block*/
    public string _outMessage = string.Empty;
    /// <summary>
    /// The output message - can also contain data for the next block
    /// </summary>
    public string outMessage
    {
        get { return _outMessage; }
        set { _outMessage = value; }

    }

    /*State of the link between this output plug and some input socket*/
    private bool _linkState = false;
    /// <summary>
    /// State of the link between this output plug and some input socket
    /// True if the link is Up false if the link is down
    /// </summary>
    public bool linkState
    {
        get { return _linkState; }
        set { _linkState = value; }
    }

    /*The input socket the plug is connected to*/
    private InputSocketController _connectedSocket = null;
    /// <summary>
    /// The input socket the plug is connected to
    /// </summary>
    public InputSocketController connectedSocket
    {
        get { return _connectedSocket; }
        set { _connectedSocket = value; }
    }


}
