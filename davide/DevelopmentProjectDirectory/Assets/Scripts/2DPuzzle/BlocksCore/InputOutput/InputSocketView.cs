﻿using UnityEngine;
using System.Collections;
using System;

public class InputSocketView : MonoBehaviour {

    #region Properties

    private InputSocketController controller;
    
    #endregion

    #region Awake-Start-Update

    // Use this for initialization
    void Awake()
    {
        controller = gameObject.GetComponent<InputSocketController>();
    }
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    #endregion

    #region OtherMethods

    #endregion


}
