﻿using UnityEngine;


public class OutputPlugView : MonoBehaviour {

    //Selection Animator
    private Animator plugSelectionAnimator;

    //Line renderer
    private LineRenderer line;

    //Starting point of the OutputPlug Line 
    private Vector2 outPlugParentPosition;
    //Ending point of the OutputPlug Line
    private Vector2 outPlugCurrentPosition;
    
    //Controller
    private OutputPlugController controller;

    void Awake()
    {
        controller = gameObject.GetComponent<OutputPlugController>();
        //get the animator component
        plugSelectionAnimator = gameObject.GetComponent<Animator>();
    }

    void Start()
    {
        /*Setting the layer and the tag of the object*/
        gameObject.layer = LayerMask.NameToLayer("OutputPlugs");
        gameObject.tag = "OutputPlug";

        //Initialization of the connection line
        line = gameObject.GetComponentInParent<LineRenderer>();

        //Saving the starting position of the Line Point
        outPlugParentPosition = transform.parent.position;
        outPlugCurrentPosition = transform.position;
        line.SetPosition(0, outPlugParentPosition);
        line.SetPosition(1, outPlugCurrentPosition);

        line.sortingLayerName = "InputOutput"; //Remember it works only if the material is sprite-renderer!    
    }

    void Update()
    {
        UpdateOutputPlugPosition();
        

    }

    private void UpdateOutputPlugPosition()
    {
        //Update Plug Position and connection line coordinates
        outPlugParentPosition = transform.parent.position;
        line.SetPosition(0, outPlugParentPosition);

        //Update the current plug position 
        //If the plug is not connected it have to follow the parent transformation
        if (!controller.linkState)
        {   
            outPlugCurrentPosition = outPlugParentPosition;
        }//if the plug is linked have to follow the linked block transformation
        else
        {
            outPlugCurrentPosition = controller.connectedSocket.gameObject.transform.position;
        }

        line.SetPosition(1, outPlugCurrentPosition);
        transform.position = outPlugCurrentPosition;
    }

    //Update the animation state
    public void UpdateStateAnimation()
    {
        //Start Selection Animation
        plugSelectionAnimator.SetBool("Selected", controller.selected);
    }







}
