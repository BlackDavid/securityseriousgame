﻿using UnityEngine;
using System.Collections;
using System;

public class OutputPlugController : MonoBehaviour
{

    #region Properties

    private OutputPlugModel model;
    private OutputPlugView view;

    /// <summary>
    /// Selection State
    /// </summary>
    private bool _selected = false;
    public bool selected
    {
        get { return _selected; }
    }
    

    /// <summary>
    /// The linkstate, propriety created to isolate the view and the model
    /// </summary>
    public bool linkState
    {
        get { return model.linkState; }
    }

   /// <summary>
   /// The output state
   /// </summary>
    public bool outputState
    {
        get { return model.outState; }
    }

    /* DEPRECATED
    /// <summary>
    /// This attribute has to be setted in unity environment
    /// </summary>
    public GameData.type outputPlugType = GameData.type.Empty;

    /// <summary>
    /// The type of this output
    /// </summary>
    public GameData.type outputType
    {
        get { return model.outType; }
    }
    */

    /// <summary>
    /// Additional data output string
    /// </summary>
    public string outputMessage
    {
        get { return model.outMessage; }
    }

    /// <summary>
    /// The connected socket
    /// </summary>
    public InputSocketController connectedSocket
    {
        get { return model.connectedSocket; }
    }

    /// <summary>
    /// The component that handle the short and long press
    /// </summary>
    private PressAndHold pressAndHoldComponent;

    /// <summary>
    /// Event rised when the data of the output plug change
    /// </summary>
    public event GameDelegates.DataChangedEventHandler DataChanged;

    /// <summary>
    /// Event rised when the user select an Output Slot
    /// </summary>
    public event GameDelegates.ShortPressEventHandler OutPlugShortPressed;

    /// <summary>
    /// Event rised when the user does a long press on the OutputSlot
    /// </summary>
    public event GameDelegates.LongPressEventHandler OutPlugLongPressed;

    #endregion

    #region Akawe-Start-Update
    void Awake()
    {
        

        //Setting the object Tag
        gameObject.tag = GameConstants.outputPlugTag;

        //Initialization of the view and model components
        model = gameObject.AddComponent<OutputPlugModel>();
        view = gameObject.AddComponent<OutputPlugView>();
        
        //GameComponent that handle the press and long press event
        pressAndHoldComponent = gameObject.GetComponent<PressAndHold>();

        //Registering the event of the output
        pressAndHoldComponent.ShortPressFired += OnComponentShortPressEventFired;
        pressAndHoldComponent.LongPressFired += OnComponentLongPressEventFired;

        
        /* DEPRECATED
        if (outputPlugType == GameData.type.Empty)
        {
            throw new Exception("The output type have to be set for this output Plug: " + gameObject.name.ToString());
        }
        else
        {
            model.outType = outputPlugType;
        }
        */
    }

    #endregion

    #region event Publisher and Subscriber

    /// <summary>
    /// Event Publisher
    /// </summary>
    protected virtual void OnDataChanged()
    {
        if (DataChanged != null)
        {
            DataChanged(gameObject,EventArgs.Empty);
        }
    }

    /// <summary>
    /// Event Publisher
    /// </summary>
    protected virtual void OnOutPlugShortPressed()
    {
        if (OutPlugShortPressed != null)
        {
            OutPlugShortPressed(gameObject, EventArgs.Empty);
        }
    }

    /// <summary>
    /// Output Slot Long Press Event Publisher
    /// </summary>
    protected virtual void OnOutPlugLongPressed()
    {
        if (OutPlugLongPressed != null)
        {
            OutPlugLongPressed(gameObject, EventArgs.Empty);
        }
    }

    //Event Subscriber for the PressAndHoldComponent event
    public void OnComponentShortPressEventFired(GameObject source, EventArgs e)
    {
        OnOutPlugShortPressed();
    }
    
    //Event Subscriber for the PressAndHoldComponent event
    public void OnComponentLongPressEventFired(GameObject source, EventArgs e)
    {
        OnOutPlugLongPressed();
    }

    #endregion

    #region OtherMethods

    /// <summary>
    /// Update the state of selection of the plug
    /// </summary>
    /// <param name="newState">The new state</param>
    public void UpdateSelectionState(bool newState)
    {
        _selected = newState;
        view.UpdateStateAnimation();
    }

    /// <summary>
    /// This method initiate the connection sequence to the input socket
    /// </summary>
    /// <param name="other">The other object that is cliked after the the Output Plug</param>
    public void ConnectionRequest(GameObject other)
    {
        //If the collider belongs to an input socket and it's not plugged in with another block the link can be done
        if (other.gameObject.layer == LayerMask.NameToLayer("InputSockets"))
        {
            //If the collider belogns to different Blocks that the original (can't connect an input of the same block)
            //if connection request is accepted save the current triggered socket as linked
            if (other.gameObject.GetComponent<InputSocketController>().ConnectionRequest(gameObject))
            {
                model.connectedSocket = other.gameObject.GetComponent<InputSocketController>();
                model.linkState = true;
            }
            else
            {
                model.connectedSocket = null;
                model.linkState = false;
            }
        }
        else
        {
            model.connectedSocket = null;
            model.linkState = false;
        }
        //Debug.Log("Stato connessione plug: " + model.linkState.ToString());
    }

    /// <summary>
    /// This method Initiate the disconnection sequence from the input socket
    /// </summary>
    public void DisconnectionRequest()
    {
        if(model.linkState && model.connectedSocket.DisconnectionRequest(gameObject))
        {
            model.linkState = false;
            model.connectedSocket = null;
        }
    }
    
    /// <summary>
    /// Method used to change output data
    /// </summary>
    public void ChangeOutputData(bool outState,string msg)
    {
        //Update the model
        model.outState = outState;
        model.outMessage= msg;
        //Rise event to notify the changind of the data
        OnDataChanged();
    }


    #endregion
}
    

