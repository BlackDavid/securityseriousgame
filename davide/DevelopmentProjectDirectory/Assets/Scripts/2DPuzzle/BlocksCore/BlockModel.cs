﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;


/// <summary>
/// Model of the Blocks
/// </summary>
public class BlockModel : MonoBehaviour{

    #region attributes
    /// <summary>
    /// The type of the block (Des, AES, Plain text, etc)
    /// </summary>
    string _blockType = string.Empty;

    /// <summary>
    /// The type of the block (Des, AES, Plain text, etc)
    /// </summary>
    public string blockType {
        get{ return _blockType; }
    }

    string _blockDescription = string.Empty;
    /// <summary>
    /// The description of the block 
    /// </summary>
    public string blockDescription
    {
        get { return _blockDescription; }
    }

    /// <summary>
    /// Array that contain the list of the input
    /// </summary>
    public List<GameObject>inputs = new List<GameObject>();
    /// <summary>
    /// Number of the input of the block
    /// </summary>
    public int inputNumber
    {
        get {return inputs.Count ; }
    }

    /// <summary>
    /// Array that contain the list of the output
    /// </summary>
    public List<GameObject>outputs = new List<GameObject>();
    /// <summary>
    /// Number of the output of the block
    /// </summary>
    public int outputNumber
    {
        get { return outputs.Count; }
    }


    /// <summary>
    /// The list of the operation the block can perform (Encryption, Decription, etc)
    /// </summary>
    ArrayList _operations=new ArrayList();

    /// <summary>
    /// The number of operation the block can perform
    /// </summary>
    public int operationsNumber
    {
        get { return _operations.Count; }
    }
    
    /// <summary>
    /// The operation identifier that the block is performing
    /// </summary>
    private int _currentOperationId = 0;
    public int currentOperationId
    {
        get { return _currentOperationId; }
    }
    
    /// <summary>
    /// The list of the block hints
    /// </summary>
    ArrayList _hints = new ArrayList();

    /// <summary>
    /// The number of the hints for this block
    /// </summary>
    public int hintsNumber
    {
        get { return _hints.Count; }
    }

    /// <summary>
    /// The list of block jokes
    /// </summary>
    ArrayList _joke = new ArrayList();

    /// <summary>
    /// Number of the jokes for this block
    /// </summary>
    public int jokesNumber
    {
        get { return _joke.Count; }
    }

    /// <summary>
    /// Xml file that contain the block data
    /// </summary>
    public TextAsset xmlFile;
    
    /// <summary>
    /// Xml variables to access the xml data
    /// </summary>
    XmlDocument xmlDoc = new XmlDocument();
    XmlNode xmlNode;
    XmlNodeList elemList;

    #endregion
    #region awake-start-update
    void Awake()
    {
        LoadXMLData();
    }
    #endregion

    #region other method
    /// <summary>
    /// This method load the block data from the xml
    /// </summary>
    public void LoadXMLData()
    {
        //Loading the block data from the xml file
        xmlDoc.LoadXml(xmlFile.text);

        //Loading the block type
        xmlNode = xmlDoc.SelectSingleNode("/block/type");
        _blockType = xmlNode.InnerText.ToString().Trim();

        //Loading the block descriptino
        xmlNode = xmlDoc.SelectSingleNode("/block/description");
        _blockDescription = xmlNode.InnerText.ToString().Trim();

        //Loading the operation list
        elemList = xmlDoc.GetElementsByTagName("operation");
        for (int i = 0; i < elemList.Count; i++)
        {
            _operations.Add(elemList[i].InnerText.ToString().Trim());
        }

        //loading the block hints
        elemList = xmlDoc.GetElementsByTagName("hint");
        for (int i = 0; i < elemList.Count; i++)
        {
            _hints.Add(elemList[i].InnerText.ToString().Trim());
        }

        //loading the block jockes
        elemList = xmlDoc.GetElementsByTagName("joke");
        for (int i = 0; i < elemList.Count; i++)
        {
            _joke.Add(elemList[i].InnerText.ToString().Trim());
        }
    }

    /// <summary>
    /// Add the input to the inputs list of the block
    /// </summary>
    /// <param name="obj">The game object that represent the input</param>
    public void AddInputSocket(GameObject obj)
    {
        inputs.Add(obj);
    }

    /// <summary>
    /// Add the Output to the outputs list of the block
    /// </summary>
    /// <param name="obj">The game object that represent the input</param>
    public void AddOutputPlug(GameObject obj)
    {
        outputs.Add(obj);
    }


    /// <summary>
    ///  Check if the inputs are correct, if all inputs are true return true
    ///  If the block has no input always return true
    /// </summary>
    public bool ElaborateInputs()
    {
        bool inputValue = false;
        if (inputNumber != 0)
        {
            
            int i = 0;
            do
            {
                inputValue = inputs[i].GetComponentInChildren<InputSocketController>().CheckInputData();
                //Debug.log("Iteration numeber: "+i);
                //Debug.Log("Input Value: " + inputValue);
                i++;
            } while (inputValue && i < inputs.Count);
        }
        else
        {
            inputValue=true;
        }
        //Debug.Log("InputValue: " + inputValue + " "+gameObject.name.ToString());
        return inputValue;

    }

    /// <summary>
    /// Return the operation with id=oid
    /// </summary>
    /// <param name="oid">Operation id as specified in the xml file</param>
    /// <returns>The operation string</returns>
    public string getOperation(int oid)
    {
        /*If the oid is contained between 1 and the number 
         * of the operations, return the operation, otherwise return empty string
         */
        if (oid >= 0 && oid <= this._operations.Count)
        {
            return _operations[oid].ToString();
        }
        return string.Empty;
    }

    /// <summary>
    /// Return the current operation
    /// </summary>
    /// <returns>String that identify the block current operation </returns>
    public string getCurrentOperation()
    {
        return getOperation(_currentOperationId);
    }

    /// <summary>
    /// Set the current operation of the block 
    /// </summary>
    /// <param name="oid">operation id as specified in the xml file</param>
    /// <returns>If the operation exist return bool otherwise return false</returns>
    public bool setOperation(int oid)
    {
        /*If the oid is contained between 1 and the number 
         * of the operations, return the operation, otherwise return empty string
         */
        if (oid > 0 && oid <= this._operations.Count)
        {
            _currentOperationId = oid;
            return true;
        }
        //the default operation is the first
        _currentOperationId = 0;
        return false;
    }

    /// <summary>
    /// Return the Hint with id=hid
    /// </summary>
    /// <param name="hid">Hint Id as specified in the xml file</param>
    /// <returns></returns>
    public string getHint(int hid)
    {
        /*If the hid is contained between 1 and the number 
         * of the hint, return the hint, otherwise return empty string
         */
        if (hid  > 0 && hid <= this._hints.Count)
        {
            return _hints[hid - 1].ToString();
        }
        return string.Empty;
    }
    
    /// <summary>
    /// Return the joke with jid=jid
    /// </summary>
    /// <param name="jid">Joke Id as specified in the xml file</param>
    /// <returns></returns>
    public string getJoke(int jid)
    {
        if (jid > 0 && jid <= this._joke.Count)
        {
            return _joke[jid - 1].ToString();
        }
        return string.Empty;
    }

    #endregion
}
