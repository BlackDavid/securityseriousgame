﻿using UnityEngine;
using System;

public class BlockController : MonoBehaviour {


    //Block general variable
    private LevelController levelController;
    private BlockModel model;
    private BlockView view;
    private GuiMasterController masterController;

    //Double press component, it handle the double press event fire
    private DoublePress doublePressComponent;

    //Block event
    public event GameDelegates.BlockGuiRequestEventHandler BlockGuiRequested;

    //Block Logic
    private IBlockLogic logic;


    void Awake()
    {   
        /*Setting the layer and the tag of the object*/
        gameObject.layer = LayerMask.NameToLayer("Blocks");
        gameObject.tag = GameConstants.blockTag;
        InitController();
    }

    void Update()
    {
        logic.SetOutput();
    }

    /// <summary>
    /// This method load the main block components and link these to the rest of the game environment
    /// </summary>
    public void InitController()
    {
        

        //Loading the general block component
        model = gameObject.GetComponent<BlockModel>();      
        view = gameObject.GetComponent<BlockView>();
        

        masterController = GameObject.FindGameObjectWithTag("GuiMasterController").GetComponent<GuiMasterController>();
        levelController = GameObject.FindGameObjectWithTag("LevelController").GetComponent<LevelController>();

        doublePressComponent = gameObject.GetComponent<DoublePress>();
        doublePressComponent.DoublePressFired += OnComponentDoublePress;

        //Loading the block logic
        logic = gameObject.GetComponent<IBlockLogic>();
        if (logic == null)
        {
            throw new Exception("You must Add block logic script!");
        }
        

        //Adding the inputs and the outputs component to the model inputs list and into the MasterController
        InputSocketController[] inSockControllers = gameObject.GetComponentsInChildren<InputSocketController>();
        foreach(InputSocketController inSock in inSockControllers)
        {
            //Register the input into the model and into the masterController 
            model.AddInputSocket(inSock.gameObject);
            masterController.Add(inSock.gameObject);
        }
        

        OutputPlugController[] outPlugControllers = gameObject.GetComponentsInChildren<OutputPlugController>();
        foreach(OutputPlugController outPlug in outPlugControllers)
        {
            //Register the output Plug into the model and into the masterController
            model.AddOutputPlug(outPlug.gameObject);
            masterController.Add(outPlug.gameObject);
        }
        //Register the block into the masterController
        masterController.Add(gameObject);
    }

    /**DEPRECATED The controller invoke the logic.setOutput() method
    /// <summary>
    /// This method set the output value of the block
    /// </summary>
    public void SetOutput()
    {
        if (model.ElaborateInputs())
        {
            logic.SetOutput();
        }
    }
    */

    /// <summary>
    /// Double press event subscriber for the "internal event"
    /// </summary>
    public void OnComponentDoublePress(GameObject source, EventArgs e)
    {
        //Debug.Log("Block double pressed");
        OnBlockGuiRequested();
    }
            
    /// <summary>
    /// Event Riser, this event has to be seen from other blocks and master controller
    /// </summary>
    public void OnBlockGuiRequested()
    {
        if (BlockGuiRequested != null)
        {
            //Collecting block data that has to be passed with the event
            BlockGuiEventArgs args = new BlockGuiEventArgs();
            args.block = gameObject;
            args.type = model.blockType;
            args.description = model.blockDescription;
            
            for (int oID = 0; oID < model.operationsNumber; oID++)
            {
                args.operations.Add(model.getOperation(oID));
            }
            args.currentOperationId = model.currentOperationId;
            BlockGuiRequested(gameObject, args);
        }
    }

    /// <summary>
    /// Change the Current block operation
    /// </summary>
    /// <param name="oID">Operation id as specified in the xml file</param>
    public void ChangeOperation(int oID)
    {
        //Debug.Log("Operation Changed");
        model.setOperation(oID);
        gameObject.GetComponent<IBlockLogic>().ChangeOperation(oID);
    }

    /// <summary>
    /// Delete the block from the play area
    /// </summary>
    public void DestroyBlock()
    {


        //Disconnect all socket
        foreach (GameObject inSock in model.inputs)
        {
            inSock.GetComponentInChildren<InputSocketController>().ForcePlugDisconnection();
        }
        foreach (GameObject outPlug in model.outputs)
        {
            outPlug.GetComponentInChildren<OutputPlugController>().DisconnectionRequest();
        }

        Destroy(gameObject);
    }
}
