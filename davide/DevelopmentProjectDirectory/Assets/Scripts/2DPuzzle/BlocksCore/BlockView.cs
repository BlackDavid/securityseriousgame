﻿using UnityEngine;
using System;

public class BlockView : MonoBehaviour {

    #region Proprieties

    private IBlockLogic controller;

    
    #endregion

    #region Awake-Start-Update
    void Awake()
    {
        //Loading the BlockController
        controller = gameObject.GetComponent<IBlockLogic>();
    }


    #endregion

    #region OtherMethods


    //Flip the block along X axis
    public void FlipX(bool f)
    {
        gameObject.GetComponent<SpriteRenderer>().flipX = f;
    }

    //Flip the block along Y axis
    public void FlipY(bool f)
    {
        gameObject.GetComponent<SpriteRenderer>().flipY = f;
    }



    #endregion



}
