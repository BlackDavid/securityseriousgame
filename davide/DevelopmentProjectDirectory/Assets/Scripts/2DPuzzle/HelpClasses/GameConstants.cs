﻿using UnityEngine;
using System;

public class GameConstants{
    //Vectors used as comparision
    public static Vector3 nullVector3 = new Vector3(-1000, -1000, -1000);
    public static Vector2 nullVector2 = new Vector2(-1000, -1000);
    /// <summary>
    /// It's the time delay that identify the user double click
    /// </summary>
    public static float doublePressTimeDelay = 0.5f;
    
    /// <summary>
    /// It's the time delay that identify a long press
    /// </summary>
    public static float longPressTimeDelay = 0.5f;

    public const string outputPlugTag = "OutputPlug";
    public const string inputSocketTag = "InputSocket";
    public const string blockTag = "Block";

}
