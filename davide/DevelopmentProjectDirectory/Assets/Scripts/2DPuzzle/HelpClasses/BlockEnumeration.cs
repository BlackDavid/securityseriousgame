﻿using UnityEngine;
using System.Collections;

public class BlockEnum : MonoBehaviour {

    /*This enum contains the type of information that blocks can exchange and their color association*/
    public enum type { Empty, CleanMessage, CypherMessage, Key, Salt };

    /*this enum contains the type of operation that a block can do*/
    public enum operation { Empty, Encryption, Decryption}

    /*Return the visual color associated with a particular data type*/
    public static Color TypeToColor(type dataType)
    {
        switch (dataType)
        {
            case type.Empty:
                return Color.black;
            case type.CleanMessage:
                return Color.blue;
            case type.CypherMessage:
                return Color.red;
            case type.Key:
                return Color.yellow;
            case type.Salt:
                return Color.white;
            default:
                return Color.clear;
        }
    }

}
