﻿using UnityEngine;
using System;
using System.Collections;

/// <summary>
/// Represent the arguments that has to be passed when user press twice on a block to show the gui
/// </summary>
public class BlockGuiEventArgs : EventArgs {

    /// <summary>
    /// the block that request the gui 
    /// </summary>
    public GameObject block = null;
    /// <summary>
    /// Block Type
    /// </summary>
    public string type = string.Empty;
    /// <summary>
    /// The Block Description
    /// </summary>
    public string description=string.Empty;
    /// <summary>
    /// Operations the block can perform
    /// </summary>
    public ArrayList operations = new ArrayList();
    /// <summary>
    /// the current block operation id
    /// </summary>
    public int currentOperationId = 0;


}
