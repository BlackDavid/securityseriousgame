﻿using UnityEngine;
using System;

public class GameDelegates : MonoBehaviour {
    /*Represent the delegate for the data changed events, raised by the IO components */
    public delegate void DataChangedEventHandler(GameObject source, EventArgs args);

    /*Double Press Delegates for the component*/
    public delegate void DoublePressEventHandler(GameObject source, EventArgs args);

    /*Short Press input Delegates for GUI interaction (click/tap)*/
    public delegate void ShortPressEventHandler(GameObject source, EventArgs args);

    /*Long press input Delegates for GUI interaction (click/tap holding)*/
    public delegate void LongPressEventHandler(GameObject source, EventArgs args);

    /*Load Block Gui Delegates */
    public delegate void BlockGuiRequestEventHandler(GameObject source, BlockGuiEventArgs args);

    





}
